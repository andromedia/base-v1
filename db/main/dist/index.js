var __create = Object.create;
var __defProp = Object.defineProperty;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __commonJS = (callback, module2) => () => {
  if (!module2) {
    module2 = { exports: {} };
    callback(module2.exports, module2);
  }
  return module2.exports;
};
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __exportStar = (target, module2, desc) => {
  __markAsModule(target);
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  if (module2 && module2.__esModule)
    return module2;
  return __exportStar(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", { value: module2, enumerable: true }), module2);
};

// ../../db/main/config.js
var require_config = __commonJS((exports2, module2) => {
  module2.exports = {
    client: "postgresql",
    connection: {
      host: "db.plansys.co",
      port: "5432",
      database: "pelindo",
      user: "postgres",
      password: "andromedia123oke"
    },
    pool: {
      min: 2,
      max: 10
    }
  };
});

// ../../db/main/src/index.ts
__export(exports, {
  dbconfig: () => dbconfig20,
  knex: () => knex2,
  master_category: () => master_category_default,
  master_menu: () => master_menu_default,
  master_port: () => master_port_default,
  master_province: () => master_province_default,
  master_question: () => master_question_default,
  master_setting: () => master_setting_default,
  p_migration: () => p_migration_default,
  p_role: () => p_role_default,
  p_user: () => p_user_default,
  p_user_role: () => p_user_role_default,
  page_content: () => page_content_default,
  page_content_media: () => page_content_media_default,
  page_intro: () => page_intro_default,
  page_intro_item: () => page_intro_item_default,
  page_section: () => page_section_default,
  page_section_item: () => page_section_item_default,
  page_section_item_content: () => page_section_item_content_default,
  page_static: () => page_static_default,
  page_static_item: () => page_static_item_default
});
var knex = __toModule(require("knex"));
var objection20 = __toModule(require("objection"));

// ../../db/main/src/generated/master_setting.ts
var objection = __toModule(require("objection"));
var dbconfig = require_config();
var master_setting = class extends objection.Model {
  static get tableName() {
    return "master_setting";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {};
  }
};
master_setting.db = {
  name: "main",
  type: dbconfig.client
};
master_setting.relations = {};
master_setting.columns = {
  id: { name: "id", type: "number" },
  key: { name: "key", type: "string" },
  value: { name: "value", type: "string" }
};
var master_setting_default = {
  definition: () => master_setting,
  update: async (fields) => {
    return await master_setting.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_setting.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_setting.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_setting.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_setting.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_setting.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_port.ts
var objection3 = __toModule(require("objection"));

// ../../db/main/src/generated/master_province.ts
var objection2 = __toModule(require("objection"));
var dbconfig2 = require_config();
var master_province = class extends objection2.Model {
  static get tableName() {
    return "master_province";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      master_port: {
        relation: objection2.Model.HasManyRelation,
        modelClass: master_port2,
        join: {
          from: "master_province.id",
          to: "master_port.id_province"
        }
      }
    };
  }
};
master_province.db = {
  name: "main",
  type: dbconfig2.client
};
master_province.relations = {
  master_port: {
    relation: "Model.HasManyRelation",
    table: "master_port",
    join: {
      from: "master_province.id",
      to: "master_port.id_province"
    }
  }
};
master_province.columns = {
  id: { name: "id", type: "number" },
  name_en: { name: "name_en", type: "string" },
  name_id: { name: "name_id", type: "string" },
  metadata: { name: "metadata", type: "string" },
  content_en: { name: "content_en", type: "string" },
  content_id: { name: "content_id", type: "string" },
  unit: { name: "unit", type: "string" }
};
var master_province_default = {
  definition: () => master_province,
  update: async (fields) => {
    return await master_province.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_province.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_province.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_province.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_province.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection2.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_province.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation2(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation2 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation2(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_port.ts
var dbconfig3 = require_config();
var master_port2 = class extends objection3.Model {
  static get tableName() {
    return "master_port";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      master_province: {
        relation: objection3.Model.BelongsToOneRelation,
        modelClass: master_province,
        join: {
          from: "master_port.id_province",
          to: "master_province.id"
        }
      }
    };
  }
};
master_port2.db = {
  name: "main",
  type: dbconfig3.client
};
master_port2.relations = {
  master_province: {
    relation: "Model.BelongsToOneRelation",
    table: "master_province",
    join: {
      from: "master_port.id_province",
      to: "master_province.id"
    }
  }
};
master_port2.columns = {
  id: { name: "id", type: "number" },
  id_province: { name: "id_province", type: "number" },
  name_en: { name: "name_en", type: "string" },
  name_id: { name: "name_id", type: "string" },
  metadata: { name: "metadata", type: "string" },
  sequence: { name: "sequence", type: "number" },
  link: { name: "link", type: "string" }
};
var master_port_default = {
  definition: () => master_port2,
  update: async (fields) => {
    return await master_port2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_port2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_port2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_port2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_port2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection3.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_port2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation3(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation3 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation3(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_menu.ts
var objection4 = __toModule(require("objection"));
var dbconfig4 = require_config();
var master_menu = class extends objection4.Model {
  static get tableName() {
    return "master_menu";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {};
  }
};
master_menu.db = {
  name: "main",
  type: dbconfig4.client
};
master_menu.relations = {};
master_menu.columns = {
  id: { name: "id", type: "number" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" },
  menu_type: { name: "menu_type", type: "string" },
  sequence: { name: "sequence", type: "number" },
  render_type: { name: "render_type", type: "string" },
  id_parent: { name: "id_parent", type: "number" },
  html: { name: "html", type: "string" },
  link: { name: "link", type: "string" }
};
var master_menu_default = {
  definition: () => master_menu,
  update: async (fields) => {
    return await master_menu.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_menu.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_menu.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_menu.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_menu.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection4.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_menu.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation4(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation4 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation4(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_category.ts
var objection10 = __toModule(require("objection"));

// ../../db/main/src/generated/page_content.ts
var objection9 = __toModule(require("objection"));

// ../../db/main/src/generated/page_content_media.ts
var objection5 = __toModule(require("objection"));
var dbconfig5 = require_config();
var page_content_media = class extends objection5.Model {
  static get tableName() {
    return "page_content_media";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_content: {
        relation: objection5.Model.BelongsToOneRelation,
        modelClass: page_content2,
        join: {
          from: "page_content_media.id_page_content",
          to: "page_content.id"
        }
      }
    };
  }
};
page_content_media.db = {
  name: "main",
  type: dbconfig5.client
};
page_content_media.relations = {
  page_content: {
    relation: "Model.BelongsToOneRelation",
    table: "page_content",
    join: {
      from: "page_content_media.id_page_content",
      to: "page_content.id"
    }
  }
};
page_content_media.columns = {
  id: { name: "id", type: "number" },
  id_page_content: { name: "id_page_content", type: "number" },
  media_mime: { name: "media_mime", type: "string" },
  media_url: { name: "media_url", type: "string" },
  sequence: { name: "sequence", type: "number" }
};
var page_content_media_default = {
  definition: () => page_content_media,
  update: async (fields) => {
    return await page_content_media.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_content_media.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_content_media.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_content_media.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_content_media.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection5.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_content_media.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation5(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation5 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation5(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_section_item_content.ts
var objection8 = __toModule(require("objection"));

// ../../db/main/src/generated/page_section_item.ts
var objection7 = __toModule(require("objection"));

// ../../db/main/src/generated/page_section.ts
var objection6 = __toModule(require("objection"));
var dbconfig6 = require_config();
var page_section = class extends objection6.Model {
  static get tableName() {
    return "page_section";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      master_category: {
        relation: objection6.Model.BelongsToOneRelation,
        modelClass: master_category2,
        join: {
          from: "page_section.id_category",
          to: "master_category.id"
        }
      },
      page_section_item: {
        relation: objection6.Model.HasManyRelation,
        modelClass: page_section_item2,
        join: {
          from: "page_section.id",
          to: "page_section_item.id_section"
        }
      }
    };
  }
};
page_section.db = {
  name: "main",
  type: dbconfig6.client
};
page_section.relations = {
  master_category: {
    relation: "Model.BelongsToOneRelation",
    table: "master_category",
    join: {
      from: "page_section.id_category",
      to: "master_category.id"
    }
  },
  page_section_item: {
    relation: "Model.HasManyRelation",
    table: "page_section_item",
    join: {
      from: "page_section.id",
      to: "page_section_item.id_section"
    }
  }
};
page_section.columns = {
  id: { name: "id", type: "number" },
  id_category: { name: "id_category", type: "number" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" },
  slug: { name: "slug", type: "string" },
  content_en: { name: "content_en", type: "string" },
  content_id: { name: "content_id", type: "string" }
};
var page_section_default = {
  definition: () => page_section,
  update: async (fields) => {
    return await page_section.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_section.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_section.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_section.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_section.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection6.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_section.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation6(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation6 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation6(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_section_item.ts
var dbconfig7 = require_config();
var page_section_item2 = class extends objection7.Model {
  static get tableName() {
    return "page_section_item";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_section: {
        relation: objection7.Model.BelongsToOneRelation,
        modelClass: page_section,
        join: {
          from: "page_section_item.id_section",
          to: "page_section.id"
        }
      },
      page_section_item_content: {
        relation: objection7.Model.HasManyRelation,
        modelClass: page_section_item_content2,
        join: {
          from: "page_section_item.id",
          to: "page_section_item_content.id_page_section_item"
        }
      }
    };
  }
};
page_section_item2.db = {
  name: "main",
  type: dbconfig7.client
};
page_section_item2.relations = {
  page_section: {
    relation: "Model.BelongsToOneRelation",
    table: "page_section",
    join: {
      from: "page_section_item.id_section",
      to: "page_section.id"
    }
  },
  page_section_item_content: {
    relation: "Model.HasManyRelation",
    table: "page_section_item_content",
    join: {
      from: "page_section_item.id",
      to: "page_section_item_content.id_page_section_item"
    }
  }
};
page_section_item2.columns = {
  id: { name: "id", type: "number" },
  id_section: { name: "id_section", type: "number" },
  content_en: { name: "content_en", type: "string" },
  content_id: { name: "content_id", type: "string" },
  sequence: { name: "sequence", type: "number" },
  section_title_en: { name: "section_title_en", type: "string" },
  section_title_id: { name: "section_title_id", type: "string" }
};
var page_section_item_default = {
  definition: () => page_section_item2,
  update: async (fields) => {
    return await page_section_item2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_section_item2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_section_item2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_section_item2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_section_item2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection7.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_section_item2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation7(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation7 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation7(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_section_item_content.ts
var dbconfig8 = require_config();
var page_section_item_content2 = class extends objection8.Model {
  static get tableName() {
    return "page_section_item_content";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_content: {
        relation: objection8.Model.BelongsToOneRelation,
        modelClass: page_content2,
        join: {
          from: "page_section_item_content.id_page_content",
          to: "page_content.id"
        }
      },
      page_section_item: {
        relation: objection8.Model.BelongsToOneRelation,
        modelClass: page_section_item2,
        join: {
          from: "page_section_item_content.id_page_section_item",
          to: "page_section_item.id"
        }
      }
    };
  }
};
page_section_item_content2.db = {
  name: "main",
  type: dbconfig8.client
};
page_section_item_content2.relations = {
  page_content: {
    relation: "Model.BelongsToOneRelation",
    table: "page_content",
    join: {
      from: "page_section_item_content.id_page_content",
      to: "page_content.id"
    }
  },
  page_section_item: {
    relation: "Model.BelongsToOneRelation",
    table: "page_section_item",
    join: {
      from: "page_section_item_content.id_page_section_item",
      to: "page_section_item.id"
    }
  }
};
page_section_item_content2.columns = {
  id: { name: "id", type: "number" },
  id_page_section_item: { name: "id_page_section_item", type: "number" },
  id_page_content: { name: "id_page_content", type: "number" },
  groupname_en: { name: "groupname_en", type: "string" },
  groupname_id: { name: "groupname_id", type: "string" }
};
var page_section_item_content_default = {
  definition: () => page_section_item_content2,
  update: async (fields) => {
    return await page_section_item_content2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_section_item_content2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_section_item_content2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_section_item_content2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_section_item_content2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection8.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_section_item_content2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation8(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation8 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation8(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_content.ts
var dbconfig9 = require_config();
var page_content2 = class extends objection9.Model {
  static get tableName() {
    return "page_content";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      master_category: {
        relation: objection9.Model.BelongsToOneRelation,
        modelClass: master_category2,
        join: {
          from: "page_content.id_category",
          to: "master_category.id"
        }
      },
      page_content_media: {
        relation: objection9.Model.HasManyRelation,
        modelClass: page_content_media,
        join: {
          from: "page_content.id",
          to: "page_content_media.id_page_content"
        }
      },
      page_section_item_content: {
        relation: objection9.Model.HasManyRelation,
        modelClass: page_section_item_content2,
        join: {
          from: "page_content.id",
          to: "page_section_item_content.id_page_content"
        }
      }
    };
  }
};
page_content2.db = {
  name: "main",
  type: dbconfig9.client
};
page_content2.relations = {
  master_category: {
    relation: "Model.BelongsToOneRelation",
    table: "master_category",
    join: {
      from: "page_content.id_category",
      to: "master_category.id"
    }
  },
  page_content_media: {
    relation: "Model.HasManyRelation",
    table: "page_content_media",
    join: {
      from: "page_content.id",
      to: "page_content_media.id_page_content"
    }
  },
  page_section_item_content: {
    relation: "Model.HasManyRelation",
    table: "page_section_item_content",
    join: {
      from: "page_content.id",
      to: "page_section_item_content.id_page_content"
    }
  }
};
page_content2.columns = {
  id: { name: "id", type: "number" },
  id_category: { name: "id_category", type: "number" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" },
  subtitle_en: { name: "subtitle_en", type: "string" },
  subtitle_id: { name: "subtitle_id", type: "string" },
  content_en: { name: "content_en", type: "string" },
  content_id: { name: "content_id", type: "string" },
  status: { name: "status", type: "string" },
  created_date: { name: "created_date", type: "Date" },
  published_date: { name: "published_date", type: "Date" },
  slug: { name: "slug", type: "string" }
};
var page_content_default = {
  definition: () => page_content2,
  update: async (fields) => {
    return await page_content2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_content2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_content2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_content2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_content2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection9.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_content2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation9(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation9 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation9(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_category.ts
var dbconfig10 = require_config();
var master_category2 = class extends objection10.Model {
  static get tableName() {
    return "master_category";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_content: {
        relation: objection10.Model.HasManyRelation,
        modelClass: page_content2,
        join: {
          from: "master_category.id",
          to: "page_content.id_category"
        }
      },
      page_section: {
        relation: objection10.Model.HasManyRelation,
        modelClass: page_section,
        join: {
          from: "master_category.id",
          to: "page_section.id_category"
        }
      }
    };
  }
};
master_category2.db = {
  name: "main",
  type: dbconfig10.client
};
master_category2.relations = {
  page_content: {
    relation: "Model.HasManyRelation",
    table: "page_content",
    join: {
      from: "master_category.id",
      to: "page_content.id_category"
    }
  },
  page_section: {
    relation: "Model.HasManyRelation",
    table: "page_section",
    join: {
      from: "master_category.id",
      to: "page_section.id_category"
    }
  }
};
master_category2.columns = {
  id: { name: "id", type: "number" },
  name_en: { name: "name_en", type: "string" },
  name_id: { name: "name_id", type: "string" }
};
var master_category_default = {
  definition: () => master_category2,
  update: async (fields) => {
    return await master_category2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_category2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_category2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_category2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_category2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection10.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_category2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation10(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation10 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation10(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_static.ts
var objection12 = __toModule(require("objection"));

// ../../db/main/src/generated/page_static_item.ts
var objection11 = __toModule(require("objection"));
var dbconfig11 = require_config();
var page_static_item = class extends objection11.Model {
  static get tableName() {
    return "page_static_item";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_static: {
        relation: objection11.Model.BelongsToOneRelation,
        modelClass: page_static2,
        join: {
          from: "page_static_item.page_static_id",
          to: "page_static.id"
        }
      }
    };
  }
};
page_static_item.db = {
  name: "main",
  type: dbconfig11.client
};
page_static_item.relations = {
  page_static: {
    relation: "Model.BelongsToOneRelation",
    table: "page_static",
    join: {
      from: "page_static_item.page_static_id",
      to: "page_static.id"
    }
  }
};
page_static_item.columns = {
  id: { name: "id", type: "number" },
  content_en: { name: "content_en", type: "string" },
  content_id: { name: "content_id", type: "string" },
  sequence: { name: "sequence", type: "number" },
  page_static_id: { name: "page_static_id", type: "number" }
};
var page_static_item_default = {
  definition: () => page_static_item,
  update: async (fields) => {
    return await page_static_item.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_static_item.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_static_item.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_static_item.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_static_item.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection11.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_static_item.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation11(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation11 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation11(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_static.ts
var dbconfig12 = require_config();
var page_static2 = class extends objection12.Model {
  static get tableName() {
    return "page_static";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_static_item: {
        relation: objection12.Model.HasManyRelation,
        modelClass: page_static_item,
        join: {
          from: "page_static.id",
          to: "page_static_item.page_static_id"
        }
      }
    };
  }
};
page_static2.db = {
  name: "main",
  type: dbconfig12.client
};
page_static2.relations = {
  page_static_item: {
    relation: "Model.HasManyRelation",
    table: "page_static_item",
    join: {
      from: "page_static.id",
      to: "page_static_item.page_static_id"
    }
  }
};
page_static2.columns = {
  id: { name: "id", type: "number" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" }
};
var page_static_default = {
  definition: () => page_static2,
  update: async (fields) => {
    return await page_static2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_static2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_static2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_static2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_static2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection12.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_static2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation12(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation12 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation12(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/master_question.ts
var objection13 = __toModule(require("objection"));
var dbconfig13 = require_config();
var master_question = class extends objection13.Model {
  static get tableName() {
    return "master_question";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {};
  }
};
master_question.db = {
  name: "main",
  type: dbconfig13.client
};
master_question.relations = {};
master_question.columns = {
  id: { name: "id", type: "number" },
  answer_en: { name: "answer_en", type: "string" },
  answer_id: { name: "answer_id", type: "string" },
  id_parent: { name: "id_parent", type: "number" },
  sequence: { name: "sequence", type: "number" }
};
var master_question_default = {
  definition: () => master_question,
  update: async (fields) => {
    return await master_question.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await master_question.query().delete().where(where);
  },
  insert: async (fields) => {
    return await master_question.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(master_question.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = master_question.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection13.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = master_question.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation13(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation13 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation13(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/p_migration.ts
var objection14 = __toModule(require("objection"));
var dbconfig14 = require_config();
var p_migration = class extends objection14.Model {
  static get tableName() {
    return "p_migration";
  }
  static get idColumn() {
    return "version";
  }
  static get relationMappings() {
    return {};
  }
};
p_migration.db = {
  name: "main",
  type: dbconfig14.client
};
p_migration.relations = {};
p_migration.columns = {
  version: { name: "version", type: "string" },
  apply_time: { name: "apply_time", type: "number" }
};
var p_migration_default = {
  definition: () => p_migration,
  update: async (fields) => {
    return await p_migration.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await p_migration.query().delete().where(where);
  },
  insert: async (fields) => {
    return await p_migration.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(p_migration.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = p_migration.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection14.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = p_migration.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation14(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation14 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation14(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_intro.ts
var objection16 = __toModule(require("objection"));

// ../../db/main/src/generated/page_intro_item.ts
var objection15 = __toModule(require("objection"));
var dbconfig15 = require_config();
var page_intro_item = class extends objection15.Model {
  static get tableName() {
    return "page_intro_item";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_intro: {
        relation: objection15.Model.BelongsToOneRelation,
        modelClass: page_intro2,
        join: {
          from: "page_intro_item.id_intro",
          to: "page_intro.id"
        }
      }
    };
  }
};
page_intro_item.db = {
  name: "main",
  type: dbconfig15.client
};
page_intro_item.relations = {
  page_intro: {
    relation: "Model.BelongsToOneRelation",
    table: "page_intro",
    join: {
      from: "page_intro_item.id_intro",
      to: "page_intro.id"
    }
  }
};
page_intro_item.columns = {
  id: { name: "id", type: "number" },
  id_intro: { name: "id_intro", type: "number" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" },
  banner_mime: { name: "banner_mime", type: "string" },
  banner_url: { name: "banner_url", type: "string" },
  link: { name: "link", type: "string" }
};
var page_intro_item_default = {
  definition: () => page_intro_item,
  update: async (fields) => {
    return await page_intro_item.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_intro_item.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_intro_item.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_intro_item.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_intro_item.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection15.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_intro_item.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation15(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation15 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation15(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/page_intro.ts
var dbconfig16 = require_config();
var page_intro2 = class extends objection16.Model {
  static get tableName() {
    return "page_intro";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      page_intro_item: {
        relation: objection16.Model.HasManyRelation,
        modelClass: page_intro_item,
        join: {
          from: "page_intro.id",
          to: "page_intro_item.id_intro"
        }
      }
    };
  }
};
page_intro2.db = {
  name: "main",
  type: dbconfig16.client
};
page_intro2.relations = {
  page_intro_item: {
    relation: "Model.HasManyRelation",
    table: "page_intro_item",
    join: {
      from: "page_intro.id",
      to: "page_intro_item.id_intro"
    }
  }
};
page_intro2.columns = {
  id: { name: "id", type: "number" },
  banner_mime: { name: "banner_mime", type: "string" },
  banner_url: { name: "banner_url", type: "string" },
  description_en: { name: "description_en", type: "string" },
  description_id: { name: "description_id", type: "string" },
  slug: { name: "slug", type: "string" },
  title_en: { name: "title_en", type: "string" },
  title_id: { name: "title_id", type: "string" }
};
var page_intro_default = {
  definition: () => page_intro2,
  update: async (fields) => {
    return await page_intro2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await page_intro2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await page_intro2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(page_intro2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = page_intro2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection16.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = page_intro2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation16(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation16 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation16(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/p_user_role.ts
var objection19 = __toModule(require("objection"));

// ../../db/main/src/generated/p_role.ts
var objection17 = __toModule(require("objection"));
var dbconfig17 = require_config();
var p_role = class extends objection17.Model {
  static get tableName() {
    return "p_role";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      p_user_role: {
        relation: objection17.Model.HasManyRelation,
        modelClass: p_user_role2,
        join: {
          from: "p_role.id",
          to: "p_user_role.role_id"
        }
      }
    };
  }
};
p_role.db = {
  name: "main",
  type: dbconfig17.client
};
p_role.relations = {
  p_user_role: {
    relation: "Model.HasManyRelation",
    table: "p_user_role",
    join: {
      from: "p_role.id",
      to: "p_user_role.role_id"
    }
  }
};
p_role.columns = {
  id: { name: "id", type: "number" },
  role_name: { name: "role_name", type: "string" },
  role_description: { name: "role_description", type: "string" },
  menu_path: { name: "menu_path", type: "string" },
  home_url: { name: "home_url", type: "string" },
  repo_path: { name: "repo_path", type: "string" }
};
var p_role_default = {
  definition: () => p_role,
  update: async (fields) => {
    return await p_role.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await p_role.query().delete().where(where);
  },
  insert: async (fields) => {
    return await p_role.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(p_role.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = p_role.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection17.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = p_role.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation17(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation17 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation17(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/p_user.ts
var objection18 = __toModule(require("objection"));
var dbconfig18 = require_config();
var p_user = class extends objection18.Model {
  static get tableName() {
    return "p_user";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      p_user_role: {
        relation: objection18.Model.HasManyRelation,
        modelClass: p_user_role2,
        join: {
          from: "p_user.id",
          to: "p_user_role.user_id"
        }
      }
    };
  }
};
p_user.db = {
  name: "main",
  type: dbconfig18.client
};
p_user.relations = {
  p_user_role: {
    relation: "Model.HasManyRelation",
    table: "p_user_role",
    join: {
      from: "p_user.id",
      to: "p_user_role.user_id"
    }
  }
};
p_user.columns = {
  id: { name: "id", type: "number" },
  email: { name: "email", type: "string" },
  username: { name: "username", type: "string" },
  password: { name: "password", type: "string" },
  last_login: { name: "last_login", type: "Date" },
  is_deleted: { name: "is_deleted", type: "boolean" },
  json: { name: "json", type: "string" }
};
var p_user_default = {
  definition: () => p_user,
  update: async (fields) => {
    return await p_user.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await p_user.query().delete().where(where);
  },
  insert: async (fields) => {
    return await p_user.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(p_user.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = p_user.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection18.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = p_user.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          if (!Array.isArray(e)) {
            e = [e];
          }

          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation18(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation18 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation18(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/generated/p_user_role.ts
var dbconfig19 = require_config();
var p_user_role2 = class extends objection19.Model {
  static get tableName() {
    return "p_user_role";
  }
  static get idColumn() {
    return "id";
  }
  static get relationMappings() {
    return {
      p_role: {
        relation: objection19.Model.BelongsToOneRelation,
        modelClass: p_role,
        join: {
          from: "p_user_role.role_id",
          to: "p_role.id"
        }
      },
      p_user: {
        relation: objection19.Model.BelongsToOneRelation,
        modelClass: p_user,
        join: {
          from: "p_user_role.user_id",
          to: "p_user.id"
        }
      }
    };
  }
};
p_user_role2.db = {
  name: "main",
  type: dbconfig19.client
};
p_user_role2.relations = {
  p_role: {
    relation: "Model.BelongsToOneRelation",
    table: "p_role",
    join: {
      from: "p_user_role.role_id",
      to: "p_role.id"
    }
  },
  p_user: {
    relation: "Model.BelongsToOneRelation",
    table: "p_user",
    join: {
      from: "p_user_role.user_id",
      to: "p_user.id"
    }
  }
};
p_user_role2.columns = {
  id: { name: "id", type: "number" },
  user_id: { name: "user_id", type: "number" },
  role_id: { name: "role_id", type: "number" },
  is_default_role: { name: "is_default_role", type: "string" }
};
var p_user_role_default = {
  definition: () => p_user_role2,
  update: async (fields) => {
    return await p_user_role2.query().update(fields.data).where(fields.where);
  },
  delete: async (where) => {
    return await p_user_role2.query().delete().where(where);
  },
  insert: async (fields) => {
    return await p_user_role2.query().insertGraph(fields);
  },
  query: async (props) => {
    const defaultCols = Object.keys(p_user_role2.columns);
    const colInput = props && props.select ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where;
    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select)[col] === "string") {
          colTables.push("raw:" + col);
        } else {
          colTables.push(col);
        }
      } else {
        colRels.push(col);
      }
    }
    const query = p_user_role2.query();
    if (props?.limit) {
      query.limit(props?.limit);
    }
    if (props?.order) {
      query.orderBy(props?.order);
    }
    if (props?.group) {
      query.groupByRaw(props?.group);
    }
    if (props?.offset) {
      query.offset(props?.offset);
    }
    if (colTables.length > 0) {
      query.select(...colTables.map((e) => {
        if (e.indexOf("raw:") === 0) {
          return objection19.Model.knex().raw((props?.select)[e.substr(4)]);
        } else {
          const col = p_user_role2.columns[e];
          if (col.name !== e) {
            return { [e]: col.name };
          }
          return col.name;
        }
      }));
    } else {
      query.select("");
    }
    if (where) {
      if (typeof where === "function") {
        query.where(where);
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === "or" ? "or" : "and";
          if (andor === "and") {
            if (e[0] === "between") {
              query.whereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.whereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.whereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1]);
              } else {
                query.andWhere(k, e[0]);
              }
            }
          } else if (andor === "or") {
            if (e[0] === "between") {
              query.orWhereBetween(k, [e[1], e[2]]);
            } else if (e[0] === "in") {
              query.orWhereIn(k, e[1]);
            } else if (e[0] === "notin") {
              query.orWhereNotIn(k, e[1]);
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1]);
              } else {
                query.orWhere(k, e[0]);
              }
            }
          }
        }
      }
    }
    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation19(query, i, props.select[i]);
      }
    }
    return await query;
  },
  _types: (_where) => {
  }
};
var buildRelation19 = (query, relname, fkcols) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder) {
      if (typeof fkcols === "object" && fkcols.select) {
        let fkfields = [];
        let fkrels = [];
        const fkall = Object.keys(builder.modelClass().columns);
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i);
          } else {
            fkrels.push(i);
          }
        }
        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else {
          builder.select("");
        }
        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation19(builder, ffk, fkcols.select[ffk]);
            builder.withGraphFetched(ffk);
          }
        }
      }
    }
  });
};

// ../../db/main/src/index.ts
var dbconfig20 = require_config();
var knex2 = knex.default(dbconfig20);
objection20.Model.knex(knex2);
//# sourceMappingURL=index.js.map
