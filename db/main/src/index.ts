import Knex from 'knex'
  import { Model } from 'objection'
  export const dbconfig = require('../config.js')
  export const knex = Knex(dbconfig)
  Model.knex(knex)

// export generated model

export { default as master_setting } from './generated/master_setting'
export { default as master_port } from './generated/master_port'
export { default as master_menu } from './generated/master_menu'
export { default as master_category } from './generated/master_category'
export { default as page_static } from './generated/page_static'
export { default as master_question } from './generated/master_question'
export { default as p_migration } from './generated/p_migration'
export { default as page_intro } from './generated/page_intro'
export { default as master_province } from './generated/master_province'
export { default as page_intro_item } from './generated/page_intro_item'
export { default as p_user_role } from './generated/p_user_role'
export { default as page_content } from './generated/page_content'
export { default as page_section } from './generated/page_section'
export { default as page_static_item } from './generated/page_static_item'
export { default as p_user } from './generated/p_user'
export { default as p_role } from './generated/p_role'
export { default as page_content_media } from './generated/page_content_media'
export { default as page_section_item } from './generated/page_section_item'
export { default as page_section_item_content } from './generated/page_section_item_content'