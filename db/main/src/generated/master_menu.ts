import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')



export class master_menu extends Model {
  static get tableName() {
    return 'master_menu';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    title_en : { name: "title_en", type: "string" },
    title_id : { name: "title_id", type: "string" },
    menu_type : { name: "menu_type", type: "string" },
    sequence : { name: "sequence", type: "number" },
    render_type : { name: "render_type", type: "string" },
    id_parent : { name: "id_parent", type: "number" },
    html : { name: "html", type: "string" },
    link : { name: "link", type: "string" },
  };
}

export interface master_menu_select {
  id?: boolean | string
	title_en?: boolean | string
	title_id?: boolean | string
	menu_type?: boolean | string
	sequence?: boolean | string
	render_type?: boolean | string
	id_parent?: boolean | string
	html?: boolean | string
	link?: boolean | string
	
}

export interface master_menu_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	title_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	title_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	menu_type?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	sequence?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	render_type?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	id_parent?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	html?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	link?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface master_menu_insert  {
  id?: number
	title_en: string
	title_id: string
	menu_type: string
	sequence: number
	render_type: string
	id_parent?: number
	html?: string
	link?: string
}

export interface master_menu_update  {
  data: {
    id?: number
	title_en?: string
	title_id?: string
	menu_type?: string
	sequence?: number
	render_type?: string
	id_parent?: number
	html?: string
	link?: string
  },
  where: master_menu_where
}

export interface master_menu_options {
  select?: master_menu_select
  where?: master_menu_where | (() => any)
  limit?: number
  group?: ('id' | 'title_en' | 'title_id' | 'menu_type' | 'sequence' | 'render_type' | 'id_parent' | 'html' | 'link')[]
  order?: ('id' | 'title_en' | 'title_id' | 'menu_type' | 'sequence' | 'render_type' | 'id_parent' | 'html' | 'link' | {
    column: 'id' | 'title_en' | 'title_id' | 'menu_type' | 'sequence' | 'render_type' | 'id_parent' | 'html' | 'link';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => master_menu,
  update: async (fields: master_menu_update) => {
    return await master_menu.query()
      .update(fields.data as PartialModelObject<master_menu>)
      .where(fields.where as any);
  }, 
  delete: async (where: master_menu_where) => {
    return await master_menu.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: master_menu_insert) => {
    return await master_menu.query().insertGraph(fields as PartialModelGraph<master_menu_insert>);
  }, 
  query: async (props?: master_menu_options) => {
    const defaultCols = Object.keys(master_menu.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = master_menu.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (master_menu.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}