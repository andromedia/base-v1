import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { page_section, page_section_options } from "./page_section"
import  { page_section_item_content, page_section_item_content_options } from "./page_section_item_content"

export class page_section_item extends Model {
  static get tableName() {
    return 'page_section_item';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      page_section:{
          relation: Model.BelongsToOneRelation,
          modelClass: page_section,
          join: {
            from: "page_section_item.id_section",
            to: "page_section.id",
          }
        },
	page_section_item_content:{
          relation: Model.HasManyRelation,
          modelClass: page_section_item_content,
          join: {
            from: "page_section_item.id",
            to: "page_section_item_content.id_page_section_item",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    page_section:{
          relation: "Model.BelongsToOneRelation",
          table: "page_section",
          join: {
            from: "page_section_item.id_section",
            to: "page_section.id",
          }
        },
	page_section_item_content:{
          relation: "Model.HasManyRelation",
          table: "page_section_item_content",
          join: {
            from: "page_section_item.id",
            to: "page_section_item_content.id_page_section_item",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    id_section : { name: "id_section", type: "number" },
    content_en : { name: "content_en", type: "string" },
    content_id : { name: "content_id", type: "string" },
    sequence : { name: "sequence", type: "number" },
    section_title_en : { name: "section_title_en", type: "string" },
    section_title_id : { name: "section_title_id", type: "string" },
  };
}

export interface page_section_item_select {
  id?: boolean | string
	id_section?: boolean | string
	content_en?: boolean | string
	content_id?: boolean | string
	sequence?: boolean | string
	section_title_en?: boolean | string
	section_title_id?: boolean | string
	page_section?: boolean | page_section_options
	page_section_item_content?: boolean | page_section_item_content_options
}

export interface page_section_item_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_section?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	content_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	content_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	sequence?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	section_title_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	section_title_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface page_section_item_insert  {
  id?: number
	id_section: number
	content_en?: string
	content_id?: string
	sequence: number
	section_title_en?: string
	section_title_id?: string
}

export interface page_section_item_update  {
  data: {
    id?: number
	id_section?: number
	content_en?: string
	content_id?: string
	sequence?: number
	section_title_en?: string
	section_title_id?: string
  },
  where: page_section_item_where
}

export interface page_section_item_options {
  select?: page_section_item_select
  where?: page_section_item_where | (() => any)
  limit?: number
  group?: ('id' | 'id_section' | 'content_en' | 'content_id' | 'sequence' | 'section_title_en' | 'section_title_id')[]
  order?: ('id' | 'id_section' | 'content_en' | 'content_id' | 'sequence' | 'section_title_en' | 'section_title_id' | {
    column: 'id' | 'id_section' | 'content_en' | 'content_id' | 'sequence' | 'section_title_en' | 'section_title_id';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => page_section_item,
  update: async (fields: page_section_item_update) => {
    return await page_section_item.query()
      .update(fields.data as PartialModelObject<page_section_item>)
      .where(fields.where as any);
  }, 
  delete: async (where: page_section_item_where) => {
    return await page_section_item.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: page_section_item_insert) => {
    return await page_section_item.query().insertGraph(fields as PartialModelGraph<page_section_item_insert>);
  }, 
  query: async (props?: page_section_item_options) => {
    const defaultCols = Object.keys(page_section_item.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = page_section_item.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (page_section_item.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}