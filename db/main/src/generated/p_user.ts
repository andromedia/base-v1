import { Model, PartialModelGraph, PartialModelObject } from 'objection'
const dbconfig = require('../../config.js')

import { p_user_role, p_user_role_options } from './p_user_role'

export class p_user extends Model {
  static get tableName() {
    return 'p_user'
  }

  static get idColumn() {
    return 'id'
  }

  static get relationMappings() {
    return {
      p_user_role: {
        relation: Model.HasManyRelation,
        modelClass: p_user_role,
        join: {
          from: 'p_user.id',
          to: 'p_user_role.user_id',
        },
      },
    }
  }

  static db = {
    name: 'main',
    type: dbconfig.client,
  }

  static relations = {
    p_user_role: {
      relation: 'Model.HasManyRelation',
      table: 'p_user_role',
      join: {
        from: 'p_user.id',
        to: 'p_user_role.user_id',
      },
    },
  }

  static columns = {
    id: { name: 'id', type: 'number' },
    email: { name: 'email', type: 'string' },
    username: { name: 'username', type: 'string' },
    password: { name: 'password', type: 'string' },
    last_login: { name: 'last_login', type: 'Date' },
    is_deleted: { name: 'is_deleted', type: 'boolean' },
    json: { name: 'json', type: 'string' },
  }
}

export interface p_user_select {
  id?: boolean | string
  email?: boolean | string
  username?: boolean | string
  password?: boolean | string
  last_login?: boolean | string
  is_deleted?: boolean | string
  json?: boolean | string
  p_user_role?: boolean | p_user_role_options
}

export interface p_user_where {
  _and?: any
  _or?: any
  id?:
    | number
    | ['in' | 'notin', number[]]
    | [string, number]
    | [string, number, 'or' | 'and']
    | ['between', number, number]
    | ['between', number, number, 'or' | 'and']
  email?:
    | string
    | ['in' | 'notin', string[]]
    | [string, string]
    | [string, string, 'or' | 'and']
    | ['between', string, string]
    | ['between', string, string, 'or' | 'and']
  username?:
    | string
    | ['in' | 'notin', string[]]
    | [string, string]
    | [string, string, 'or' | 'and']
    | ['between', string, string]
    | ['between', string, string, 'or' | 'and']
  password?:
    | string
    | ['in' | 'notin', string[]]
    | [string, string]
    | [string, string, 'or' | 'and']
    | ['between', string, string]
    | ['between', string, string, 'or' | 'and']
  last_login?:
    | Date
    | ['in' | 'notin', Date[]]
    | [string, Date]
    | [string, Date, 'or' | 'and']
    | ['between', Date, Date]
    | ['between', Date, Date, 'or' | 'and']
  is_deleted?:
    | boolean
    | ['in' | 'notin', boolean[]]
    | [string, boolean]
    | [string, boolean, 'or' | 'and']
    | ['between', boolean, boolean]
    | ['between', boolean, boolean, 'or' | 'and']
  json?:
    | string
    | ['in' | 'notin', string[]]
    | [string, string]
    | [string, string, 'or' | 'and']
    | ['between', string, string]
    | ['between', string, string, 'or' | 'and']
}

export interface p_user_insert {
  id?: number
  email: string
  username: string
  password: string
  last_login?: Date
  is_deleted?: boolean
  json?: string
}

export interface p_user_update {
  data: {
    id?: number
    email?: string
    username?: string
    password?: string
    last_login?: Date
    is_deleted?: boolean
    json?: string
  }
  where: p_user_where
}

export interface p_user_options {
  select?: p_user_select
  where?: p_user_where | (() => any)
  limit?: number
  group?: (
    | 'id'
    | 'email'
    | 'username'
    | 'password'
    | 'last_login'
    | 'is_deleted'
    | 'json'
  )[]
  order?: (
    | 'id'
    | 'email'
    | 'username'
    | 'password'
    | 'last_login'
    | 'is_deleted'
    | 'json'
    | {
        column:
          | 'id'
          | 'email'
          | 'username'
          | 'password'
          | 'last_login'
          | 'is_deleted'
          | 'json'
        order: 'asc' | 'desc' | 'ASC' | 'DESC'
      }
  )[]
  offset?: number
}

export default {
  definition: () => p_user,
  update: async (fields: p_user_update) => {
    return await p_user
      .query()
      .update(fields.data as PartialModelObject<p_user>)
      .where(fields.where as any)
  },
  delete: async (where: p_user_where) => {
    return await p_user
      .query()
      .delete()
      .where(where as any)
  },
  insert: async (fields: p_user_insert) => {
    return await p_user
      .query()
      .insertGraph(fields as PartialModelGraph<p_user_insert>)
  },
  query: async (props?: p_user_options) => {
    const defaultCols = Object.keys(p_user.columns)
    const colInput =
      props && props.select ? Object.keys(props.select) : defaultCols
    const colTables = []
    const colRels = []
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col)
        } else {
          colTables.push(col)
        }
      } else {
        colRels.push(col)
      }
    }

    const query = p_user.query()

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (p_user.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else {
      // we have to do this to select no columns
      query.select('')
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          console.log(k, e)
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else { 
              if (e.length > 1) { 
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query
  },
  _types: (_where: p_user_where | (() => any)) => {},
}

const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields)
        } else {
          // select no columns
          builder.select('')
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}
