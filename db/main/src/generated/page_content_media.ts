import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { page_content, page_content_options } from "./page_content"

export class page_content_media extends Model {
  static get tableName() {
    return 'page_content_media';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      page_content:{
          relation: Model.BelongsToOneRelation,
          modelClass: page_content,
          join: {
            from: "page_content_media.id_page_content",
            to: "page_content.id",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    page_content:{
          relation: "Model.BelongsToOneRelation",
          table: "page_content",
          join: {
            from: "page_content_media.id_page_content",
            to: "page_content.id",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    id_page_content : { name: "id_page_content", type: "number" },
    media_mime : { name: "media_mime", type: "string" },
    media_url : { name: "media_url", type: "string" },
    sequence : { name: "sequence", type: "number" },
  };
}

export interface page_content_media_select {
  id?: boolean | string
	id_page_content?: boolean | string
	media_mime?: boolean | string
	media_url?: boolean | string
	sequence?: boolean | string
	page_content?: boolean | page_content_options
}

export interface page_content_media_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_page_content?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	media_mime?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	media_url?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	sequence?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
}

export interface page_content_media_insert  {
  id?: number
	id_page_content: number
	media_mime: string
	media_url: string
	sequence: number
}

export interface page_content_media_update  {
  data: {
    id?: number
	id_page_content?: number
	media_mime?: string
	media_url?: string
	sequence?: number
  },
  where: page_content_media_where
}

export interface page_content_media_options {
  select?: page_content_media_select
  where?: page_content_media_where | (() => any)
  limit?: number
  group?: ('id' | 'id_page_content' | 'media_mime' | 'media_url' | 'sequence')[]
  order?: ('id' | 'id_page_content' | 'media_mime' | 'media_url' | 'sequence' | {
    column: 'id' | 'id_page_content' | 'media_mime' | 'media_url' | 'sequence';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => page_content_media,
  update: async (fields: page_content_media_update) => {
    return await page_content_media.query()
      .update(fields.data as PartialModelObject<page_content_media>)
      .where(fields.where as any);
  }, 
  delete: async (where: page_content_media_where) => {
    return await page_content_media.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: page_content_media_insert) => {
    return await page_content_media.query().insertGraph(fields as PartialModelGraph<page_content_media_insert>);
  }, 
  query: async (props?: page_content_media_options) => {
    const defaultCols = Object.keys(page_content_media.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = page_content_media.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (page_content_media.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}