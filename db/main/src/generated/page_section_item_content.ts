import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { page_content, page_content_options } from "./page_content"
import  { page_section_item, page_section_item_options } from "./page_section_item"

export class page_section_item_content extends Model {
  static get tableName() {
    return 'page_section_item_content';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      page_content:{
          relation: Model.BelongsToOneRelation,
          modelClass: page_content,
          join: {
            from: "page_section_item_content.id_page_content",
            to: "page_content.id",
          }
        },
	page_section_item:{
          relation: Model.BelongsToOneRelation,
          modelClass: page_section_item,
          join: {
            from: "page_section_item_content.id_page_section_item",
            to: "page_section_item.id",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    page_content:{
          relation: "Model.BelongsToOneRelation",
          table: "page_content",
          join: {
            from: "page_section_item_content.id_page_content",
            to: "page_content.id",
          }
        },
	page_section_item:{
          relation: "Model.BelongsToOneRelation",
          table: "page_section_item",
          join: {
            from: "page_section_item_content.id_page_section_item",
            to: "page_section_item.id",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    id_page_section_item : { name: "id_page_section_item", type: "number" },
    id_page_content : { name: "id_page_content", type: "number" },
    groupname_en : { name: "groupname_en", type: "string" },
    groupname_id : { name: "groupname_id", type: "string" },
  };
}

export interface page_section_item_content_select {
  id?: boolean | string
	id_page_section_item?: boolean | string
	id_page_content?: boolean | string
	groupname_en?: boolean | string
	groupname_id?: boolean | string
	page_content?: boolean | page_content_options
	page_section_item?: boolean | page_section_item_options
}

export interface page_section_item_content_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_page_section_item?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_page_content?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	groupname_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	groupname_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface page_section_item_content_insert  {
  id?: number
	id_page_section_item: number
	id_page_content: number
	groupname_en?: string
	groupname_id?: string
}

export interface page_section_item_content_update  {
  data: {
    id?: number
	id_page_section_item?: number
	id_page_content?: number
	groupname_en?: string
	groupname_id?: string
  },
  where: page_section_item_content_where
}

export interface page_section_item_content_options {
  select?: page_section_item_content_select
  where?: page_section_item_content_where | (() => any)
  limit?: number
  group?: ('id' | 'id_page_section_item' | 'id_page_content' | 'groupname_en' | 'groupname_id')[]
  order?: ('id' | 'id_page_section_item' | 'id_page_content' | 'groupname_en' | 'groupname_id' | {
    column: 'id' | 'id_page_section_item' | 'id_page_content' | 'groupname_en' | 'groupname_id';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => page_section_item_content,
  update: async (fields: page_section_item_content_update) => {
    return await page_section_item_content.query()
      .update(fields.data as PartialModelObject<page_section_item_content>)
      .where(fields.where as any);
  }, 
  delete: async (where: page_section_item_content_where) => {
    return await page_section_item_content.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: page_section_item_content_insert) => {
    return await page_section_item_content.query().insertGraph(fields as PartialModelGraph<page_section_item_content_insert>);
  }, 
  query: async (props?: page_section_item_content_options) => {
    const defaultCols = Object.keys(page_section_item_content.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = page_section_item_content.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (page_section_item_content.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}