import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { master_province, master_province_options } from "./master_province"

export class master_port extends Model {
  static get tableName() {
    return 'master_port';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      master_province:{
          relation: Model.BelongsToOneRelation,
          modelClass: master_province,
          join: {
            from: "master_port.id_province",
            to: "master_province.id",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    master_province:{
          relation: "Model.BelongsToOneRelation",
          table: "master_province",
          join: {
            from: "master_port.id_province",
            to: "master_province.id",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    id_province : { name: "id_province", type: "number" },
    name_en : { name: "name_en", type: "string" },
    name_id : { name: "name_id", type: "string" },
    metadata : { name: "metadata", type: "string" },
    sequence : { name: "sequence", type: "number" },
    link : { name: "link", type: "string" },
  };
}

export interface master_port_select {
  id?: boolean | string
	id_province?: boolean | string
	name_en?: boolean | string
	name_id?: boolean | string
	metadata?: boolean | string
	sequence?: boolean | string
	link?: boolean | string
	master_province?: boolean | master_province_options
}

export interface master_port_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_province?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	name_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	name_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	metadata?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	sequence?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	link?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface master_port_insert  {
  id?: number
	id_province: number
	name_en: string
	name_id: string
	metadata?: string
	sequence: number
	link?: string
}

export interface master_port_update  {
  data: {
    id?: number
	id_province?: number
	name_en?: string
	name_id?: string
	metadata?: string
	sequence?: number
	link?: string
  },
  where: master_port_where
}

export interface master_port_options {
  select?: master_port_select
  where?: master_port_where | (() => any)
  limit?: number
  group?: ('id' | 'id_province' | 'name_en' | 'name_id' | 'metadata' | 'sequence' | 'link')[]
  order?: ('id' | 'id_province' | 'name_en' | 'name_id' | 'metadata' | 'sequence' | 'link' | {
    column: 'id' | 'id_province' | 'name_en' | 'name_id' | 'metadata' | 'sequence' | 'link';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => master_port,
  update: async (fields: master_port_update) => {
    return await master_port.query()
      .update(fields.data as PartialModelObject<master_port>)
      .where(fields.where as any);
  }, 
  delete: async (where: master_port_where) => {
    return await master_port.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: master_port_insert) => {
    return await master_port.query().insertGraph(fields as PartialModelGraph<master_port_insert>);
  }, 
  query: async (props?: master_port_options) => {
    const defaultCols = Object.keys(master_port.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = master_port.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (master_port.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}