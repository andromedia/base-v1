import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { master_category, master_category_options } from "./master_category"
import  { page_content_media, page_content_media_options } from "./page_content_media"
import  { page_section_item_content, page_section_item_content_options } from "./page_section_item_content"

export class page_content extends Model {
  static get tableName() {
    return 'page_content';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      master_category:{
          relation: Model.BelongsToOneRelation,
          modelClass: master_category,
          join: {
            from: "page_content.id_category",
            to: "master_category.id",
          }
        },
	page_content_media:{
          relation: Model.HasManyRelation,
          modelClass: page_content_media,
          join: {
            from: "page_content.id",
            to: "page_content_media.id_page_content",
          }
        },
	page_section_item_content:{
          relation: Model.HasManyRelation,
          modelClass: page_section_item_content,
          join: {
            from: "page_content.id",
            to: "page_section_item_content.id_page_content",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    master_category:{
          relation: "Model.BelongsToOneRelation",
          table: "master_category",
          join: {
            from: "page_content.id_category",
            to: "master_category.id",
          }
        },
	page_content_media:{
          relation: "Model.HasManyRelation",
          table: "page_content_media",
          join: {
            from: "page_content.id",
            to: "page_content_media.id_page_content",
          }
        },
	page_section_item_content:{
          relation: "Model.HasManyRelation",
          table: "page_section_item_content",
          join: {
            from: "page_content.id",
            to: "page_section_item_content.id_page_content",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    id_category : { name: "id_category", type: "number" },
    title_en : { name: "title_en", type: "string" },
    title_id : { name: "title_id", type: "string" },
    subtitle_en : { name: "subtitle_en", type: "string" },
    subtitle_id : { name: "subtitle_id", type: "string" },
    content_en : { name: "content_en", type: "string" },
    content_id : { name: "content_id", type: "string" },
    status : { name: "status", type: "string" },
    created_date : { name: "created_date", type: "Date" },
    published_date : { name: "published_date", type: "Date" },
    slug : { name: "slug", type: "string" },
  };
}

export interface page_content_select {
  id?: boolean | string
	id_category?: boolean | string
	title_en?: boolean | string
	title_id?: boolean | string
	subtitle_en?: boolean | string
	subtitle_id?: boolean | string
	content_en?: boolean | string
	content_id?: boolean | string
	status?: boolean | string
	created_date?: boolean | string
	published_date?: boolean | string
	slug?: boolean | string
	master_category?: boolean | master_category_options
	page_content_media?: boolean | page_content_media_options
	page_section_item_content?: boolean | page_section_item_content_options
}

export interface page_content_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	id_category?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	title_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	title_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	subtitle_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	subtitle_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	content_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	content_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	status?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	created_date?: Date | ['in' | 'notin', Date[]] | [string, Date] | [string, Date, 'or' | 'and']  | ['between', Date, Date] | ['between', Date, Date, 'or' | 'and']
	published_date?: Date | ['in' | 'notin', Date[]] | [string, Date] | [string, Date, 'or' | 'and']  | ['between', Date, Date] | ['between', Date, Date, 'or' | 'and']
	slug?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface page_content_insert  {
  id?: number
	id_category?: number
	title_en: string
	title_id: string
	subtitle_en?: string
	subtitle_id?: string
	content_en?: string
	content_id?: string
	status: string
	created_date: Date
	published_date?: Date
	slug?: string
}

export interface page_content_update  {
  data: {
    id?: number
	id_category?: number
	title_en?: string
	title_id?: string
	subtitle_en?: string
	subtitle_id?: string
	content_en?: string
	content_id?: string
	status?: string
	created_date?: Date
	published_date?: Date
	slug?: string
  },
  where: page_content_where
}

export interface page_content_options {
  select?: page_content_select
  where?: page_content_where | (() => any)
  limit?: number
  group?: ('id' | 'id_category' | 'title_en' | 'title_id' | 'subtitle_en' | 'subtitle_id' | 'content_en' | 'content_id' | 'status' | 'created_date' | 'published_date' | 'slug')[]
  order?: ('id' | 'id_category' | 'title_en' | 'title_id' | 'subtitle_en' | 'subtitle_id' | 'content_en' | 'content_id' | 'status' | 'created_date' | 'published_date' | 'slug' | {
    column: 'id' | 'id_category' | 'title_en' | 'title_id' | 'subtitle_en' | 'subtitle_id' | 'content_en' | 'content_id' | 'status' | 'created_date' | 'published_date' | 'slug';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => page_content,
  update: async (fields: page_content_update) => {
    return await page_content.query()
      .update(fields.data as PartialModelObject<page_content>)
      .where(fields.where as any);
  }, 
  delete: async (where: page_content_where) => {
    return await page_content.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: page_content_insert) => {
    return await page_content.query().insertGraph(fields as PartialModelGraph<page_content_insert>);
  }, 
  query: async (props?: page_content_options) => {
    const defaultCols = Object.keys(page_content.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = page_content.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (page_content.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}