import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { p_user_role, p_user_role_options } from "./p_user_role"

export class p_role extends Model {
  static get tableName() {
    return 'p_role';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      p_user_role:{
          relation: Model.HasManyRelation,
          modelClass: p_user_role,
          join: {
            from: "p_role.id",
            to: "p_user_role.role_id",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    p_user_role:{
          relation: "Model.HasManyRelation",
          table: "p_user_role",
          join: {
            from: "p_role.id",
            to: "p_user_role.role_id",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    role_name : { name: "role_name", type: "string" },
    role_description : { name: "role_description", type: "string" },
    menu_path : { name: "menu_path", type: "string" },
    home_url : { name: "home_url", type: "string" },
    repo_path : { name: "repo_path", type: "string" },
  };
}

export interface p_role_select {
  id?: boolean | string
	role_name?: boolean | string
	role_description?: boolean | string
	menu_path?: boolean | string
	home_url?: boolean | string
	repo_path?: boolean | string
	p_user_role?: boolean | p_user_role_options
}

export interface p_role_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	role_name?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	role_description?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	menu_path?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	home_url?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	repo_path?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface p_role_insert  {
  id?: number
	role_name: string
	role_description: string
	menu_path?: string
	home_url?: string
	repo_path?: string
}

export interface p_role_update  {
  data: {
    id?: number
	role_name?: string
	role_description?: string
	menu_path?: string
	home_url?: string
	repo_path?: string
  },
  where: p_role_where
}

export interface p_role_options {
  select?: p_role_select
  where?: p_role_where | (() => any)
  limit?: number
  group?: ('id' | 'role_name' | 'role_description' | 'menu_path' | 'home_url' | 'repo_path')[]
  order?: ('id' | 'role_name' | 'role_description' | 'menu_path' | 'home_url' | 'repo_path' | {
    column: 'id' | 'role_name' | 'role_description' | 'menu_path' | 'home_url' | 'repo_path';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => p_role,
  update: async (fields: p_role_update) => {
    return await p_role.query()
      .update(fields.data as PartialModelObject<p_role>)
      .where(fields.where as any);
  }, 
  delete: async (where: p_role_where) => {
    return await p_role.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: p_role_insert) => {
    return await p_role.query().insertGraph(fields as PartialModelGraph<p_role_insert>);
  }, 
  query: async (props?: p_role_options) => {
    const defaultCols = Object.keys(p_role.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = p_role.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (p_role.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}