import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

import  { p_role, p_role_options } from "./p_role"
import  { p_user, p_user_options } from "./p_user"

export class p_user_role extends Model {
  static get tableName() {
    return 'p_user_role';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      p_role:{
          relation: Model.BelongsToOneRelation,
          modelClass: p_role,
          join: {
            from: "p_user_role.role_id",
            to: "p_role.id",
          }
        },
	p_user:{
          relation: Model.BelongsToOneRelation,
          modelClass: p_user,
          join: {
            from: "p_user_role.user_id",
            to: "p_user.id",
          }
        }
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    p_role:{
          relation: "Model.BelongsToOneRelation",
          table: "p_role",
          join: {
            from: "p_user_role.role_id",
            to: "p_role.id",
          }
        },
	p_user:{
          relation: "Model.BelongsToOneRelation",
          table: "p_user",
          join: {
            from: "p_user_role.user_id",
            to: "p_user.id",
          }
        }
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    user_id : { name: "user_id", type: "number" },
    role_id : { name: "role_id", type: "number" },
    is_default_role : { name: "is_default_role", type: "string" },
  };
}

export interface p_user_role_select {
  id?: boolean | string
	user_id?: boolean | string
	role_id?: boolean | string
	is_default_role?: boolean | string
	p_role?: boolean | p_role_options
	p_user?: boolean | p_user_options
}

export interface p_user_role_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	user_id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	role_id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	is_default_role?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
}

export interface p_user_role_insert  {
  id?: number
	user_id: number
	role_id: number
	is_default_role?: string
}

export interface p_user_role_update  {
  data: {
    id?: number
	user_id?: number
	role_id?: number
	is_default_role?: string
  },
  where: p_user_role_where
}

export interface p_user_role_options {
  select?: p_user_role_select
  where?: p_user_role_where | (() => any)
  limit?: number
  group?: ('id' | 'user_id' | 'role_id' | 'is_default_role')[]
  order?: ('id' | 'user_id' | 'role_id' | 'is_default_role' | {
    column: 'id' | 'user_id' | 'role_id' | 'is_default_role';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => p_user_role,
  update: async (fields: p_user_role_update) => {
    return await p_user_role.query()
      .update(fields.data as PartialModelObject<p_user_role>)
      .where(fields.where as any);
  }, 
  delete: async (where: p_user_role_where) => {
    return await p_user_role.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: p_user_role_insert) => {
    return await p_user_role.query().insertGraph(fields as PartialModelGraph<p_user_role_insert>);
  }, 
  query: async (props?: p_user_role_options) => {
    const defaultCols = Object.keys(p_user_role.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = p_user_role.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (p_user_role.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}