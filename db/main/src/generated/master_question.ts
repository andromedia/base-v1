import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')



export class master_question extends Model {
  static get tableName() {
    return 'master_question';
  }

  static get idColumn() {
    return "id"
  }

  static get relationMappings() {
    return { 
      
    }
  }

  static db = {
    name: "main",
    type: dbconfig.client
  }

  static relations = { 
    
  }
  
  static columns = { 
    id : { name: "id", type: "number" },
    answer_en : { name: "answer_en", type: "string" },
    answer_id : { name: "answer_id", type: "string" },
    id_parent : { name: "id_parent", type: "number" },
    sequence : { name: "sequence", type: "number" },
  };
}

export interface master_question_select {
  id?: boolean | string
	answer_en?: boolean | string
	answer_id?: boolean | string
	id_parent?: boolean | string
	sequence?: boolean | string
	
}

export interface master_question_where {
  _and?: any,
  _or?:any,
  id?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	answer_en?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	answer_id?: string | ['in' | 'notin', string[]] | [string, string] | [string, string, 'or' | 'and']  | ['between', string, string] | ['between', string, string, 'or' | 'and']
	id_parent?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
	sequence?: number | ['in' | 'notin', number[]] | [string, number] | [string, number, 'or' | 'and']  | ['between', number, number] | ['between', number, number, 'or' | 'and']
}

export interface master_question_insert  {
  id?: number
	answer_en?: string
	answer_id?: string
	id_parent?: number
	sequence: number
}

export interface master_question_update  {
  data: {
    id?: number
	answer_en?: string
	answer_id?: string
	id_parent?: number
	sequence?: number
  },
  where: master_question_where
}

export interface master_question_options {
  select?: master_question_select
  where?: master_question_where | (() => any)
  limit?: number
  group?: ('id' | 'answer_en' | 'answer_id' | 'id_parent' | 'sequence')[]
  order?: ('id' | 'answer_en' | 'answer_id' | 'id_parent' | 'sequence' | {
    column: 'id' | 'answer_en' | 'answer_id' | 'id_parent' | 'sequence';
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => master_question,
  update: async (fields: master_question_update) => {
    return await master_question.query()
      .update(fields.data as PartialModelObject<master_question>)
      .where(fields.where as any);
  }, 
  delete: async (where: master_question_where) => {
    return await master_question.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: master_question_insert) => {
    return await master_question.query().insertGraph(fields as PartialModelGraph<master_question_insert>);
  }, 
  query: async (props?: master_question_options) => {
    const defaultCols = Object.keys(master_question.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (defaultCols.indexOf(col) >= 0) {
        if (props?.select && typeof (props?.select as any)[col] === 'string') {
          colTables.push('raw:' + col);
        } else {
          colTables.push(col);
        }

      } else {
        colRels.push(col);
      }
    }

    const query = master_question.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (master_question.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {
          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: p_user_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(`${relname}(selectColumns)`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}