module.exports = {
  purge: {
    enabled: true,
    content: ['./**/*.tsx', './**/*.ts', './**/*.jsx', './**/*.js', './**/*.html']
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
