/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
  routes: [
    { "match": "routes", "src": ".*", "_srcRegex": /.*/ig, "dest": "/index.html" }
  ],
  alias: {
    pages: './src/pages',
    components: './src/components',
  },
  mount: {
    public: '/',
    src: '/dist/',
    pages: '/dist/pages/',
    components: '/dist/components/',
  },
  plugins: [
    '@snowpack/plugin-dotenv',
    '@snowpack/plugin-postcss',
    '@snowpack/plugin-webpack',
    
  ],
  devOptions: {
    /* ... */
    port: 3201,
    output: 'stream',
    open: 'none'
  },
  buildOptions: {
    out: '../.build/client',
    clean: true,
    metaUrlPath: '_snowpack_'
  },
};
