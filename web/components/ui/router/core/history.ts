import { isSSR } from 'components/ui/router'
import { getPathWithoutBasename } from './basename'

export interface RouterHistory {
  push: (path: string) => void
  replace: (path: string) => void 
  stop: () => void
  urlTo: (path: string) => string
}

export const getHashPath = () => {
  if (isSSR) {
    return window.location.hash.substr(1)
  }
  return ''
}
export function createHistory(
  hashMode: boolean,
  basename: string,
  setPath: (path: string) => void
): RouterHistory {
  if (isSSR) {
    return {
      push(path: string) {},
      replace(path: string) {},
      stop() {},
      urlTo(path: string) {
        return ''
      },
    }
  }

  const { location, history } = window
  const onChange = hashMode
    ? () => setPath(getHashPath())
    : () => setPath(getPathWithoutBasename(basename))

  const urlTo = hashMode
    ? (path: string) => `${location.pathname}${location.search}#${path}`
    : (path: string) => `${basename}${path}`

  window.addEventListener('popstate', onChange)
  return {
    push(path: string) {
      const newPath = urlTo(path)
      // Only push if something changed.
      if (newPath !== location.pathname + location.search + location.hash)
        history.pushState({}, '', newPath)
      onChange()
    },
    replace(path: string) {
      history.replaceState({}, '', urlTo(path))
      onChange()
    },
    stop() {
      window.removeEventListener('popstate', onChange)
    },
    urlTo,
  }
}
