import React from 'react'
import ReactDOM from 'react-dom'
import MainRouter, { IPages } from './router'

export const initApp = (pages: IPages) => {
  ReactDOM.render(<MainRouter pages={pages} />, document.getElementById('root'))
}
