import { matchRoute } from 'libs'
import { get } from 'lodash-es'
import React, { Suspense, useEffect } from 'react'
import { isSSR } from 'components/ui/router'
import { Route, RouteParams, Router } from './core'
import { MainLayout } from './layout'

const getLastPath = () => {
  return (window as any).lastPath || ''
}

const setLastPath = (path: string) => {
  return ((window as any).lastPath = path)
}

export const getServerProps = () => {
  return (window as any).serverProps || {}
}

export const setServerProps = (props: any) => {
  ;(window as any).serverProps = props
}

export type IPages = Record<string, { c: any; s: boolean }>
function routeMatcherFactory(pattern: string) {
  return (path: string): RouteParams | null => {
    const result = matchRoute(path, pattern)

    if (result) {
      if (path.split('/').pop()?.indexOf('_') === 0) {
        return null
      }
      return { ...result, ___PATH___: path } as RouteParams
    }
    return null
  }
}

const MainRouter = ({
  pages,
  ssr,
}: {
  pages: IPages
  ssr?: { url: string; props: any }
}) => {
  useEffect(() => {
    ;(window as any).isSSR = false
  }, [])

  return (
    <Router routeMatcherFactory={routeMatcherFactory} ssr={ssr}>
      <MainLayout pages={pages} ssr={ssr} serverProps={getServerProps()}>
        {(renderLayout: () => void) => {
          return Object.keys(pages).map((e) => {
            const page = createPageRoute(
              e,
              pages[e].c,
              pages[e].s,
              renderLayout,
              ssr
            )
            return <Route key={e} path={e} component={page} />
          })
        }}
      </MainLayout>
    </Router>
  )
}

export const createPageRoute = (
  _pattern: string,
  Component: React.FC,
  haveServerMethod: boolean,
  renderLayout: () => void,
  ssr?: { url: string; props: any }
) => {
  const PageRoute = (inputProps: any) => {
    const path = get(inputProps, 'params.___PATH___')

    let finalProps: any = {
      path,
      isLoading: false,
    }

    const lastPath = getLastPath()
    if (haveServerMethod) {
      if (lastPath !== path) {
        if (!isSSR) {
          window.scrollTo(0, 0)
        }

        if (lastPath === '') {
          if (ssr) {
            finalProps = {
              ...get(ssr.props, 'page.props', {}),
              isLoading: false,
              path,
            }
          } else {
            finalProps = {
              ...get(getServerProps(), 'page.props', {}),
              isLoading: false,
              path,
            }
          }
        } else {
          // should render from server
          finalProps = {
            path,
            isLoading: true,
          }
        }
        setLastPath(path)
      } else {
        // this is development live reload
        const serverPath = get(getServerProps(), 'path', '')
        if (!serverPath || serverPath === path) {
          finalProps = {
            ...get(getServerProps(), 'page.props', {}),
            isLoading: false,
            path,
          }
        }
      }
    }

    useEffect(() => {
      if (finalProps.isLoading) {
        ;(async () => {
          const result = await fetch(path, {
            headers: {
              'get-server-props': 'yes',
            },
          })
          const json = await result.json()
          setServerProps({ ...json })
          setLastPath('')
          renderLayout()
        })()
      }
    }, [finalProps.isLoading])

    if (isSSR) {
      return <Component {...finalProps} />
    } else {
      return (
        <Suspense fallback={null}>
          <Component {...finalProps} />
        </Suspense>
      )
    }
  }
  return PageRoute
}

export default MainRouter
