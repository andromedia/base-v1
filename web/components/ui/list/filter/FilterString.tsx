import { TextField } from '@fluentui/react/lib/TextField'
import { action } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import * as React from 'react'
import ItemButton from './ItemButton'

export default observer(({ label, field, value, setValue, submit }: any) => {
  const meta = useLocalObservable(() => ({
    oldval: value,
  }))
  return (
    <ItemButton
      label={label}
      field={field}
      setValue={setValue}
      onClear={() => {
        setValue(null)
        submit()
      }}
      value={value}
    >
      <TextField
        value={meta.oldval}
        onChange={action((e: any) => {
          meta.oldval = e.target.value
        })}
        onKeyDown={action((e: any) => {
          if (e.which === 13) {
            meta.oldval = e.target.value
            setValue(e.target.value)
            submit()
          }
        })}
        styles={{ root: { padding: 10 } }}
      />
    </ItemButton>
  )
})
