import React, { useEffect, useRef } from 'react'
import { IFieldType, PureField, Section } from 'components/ui/form/QueryForm'
import { get } from 'lodash-es'
import { action, runInAction, toJS } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'

interface IJsonEditor {
  value: any
  useMeta?: boolean
  colNum?: number
  useFormTag?: boolean
  alter?: (def: any) => any
  onChange: (value: any, change: { key: string | number; value: any }) => void
}

type IDefField = {
  name: string
  type: IFieldType
  custom?: (IDefField) => any
}

type IDefRecord = Record<any, IDefField>

export const JsonEditor = observer(
  ({ value, alter, onChange, colNum, useFormTag, useMeta }: IJsonEditor) => {
    const meta = useLocalObservable(() => ({
      data: value as Record<number | string, any>,
    }))

    const metaValue = useMeta ? meta.data : value

    let def = generateDef(metaValue)

    if (alter) {
      def = alter(def)
    }

    const original = useRef(toJS(value))

    const renderedDef = renderDef(def, metaValue, {
      colNum,
      setAll: action((value: any) => {
        meta.data = value
      }),
      set: action((key: string | number, value: any) => {
        meta.data[key] = value
        onChange(toJS(meta.data), { key, value })
      }),
      original: original.current,
    })

    if (useFormTag) {
      return <form>{renderedDef}</form>
    }
    return <>{renderedDef}</>
  }
)

const renderDef = (
  def: IDefRecord,
  value: any,
  options: {
    original: any
    colNum?: number
    setAll: (value: any) => void
    set: (key: string | number, value: any) => void
  }
) => {
  const { colNum, set, setAll, original } = options
  const fields = Object.entries(def)

  return (
    <Section colNum={colNum || 2} className="flex-1">
      {fields.map(([k, v]) => {
        let arrayOptions: any = undefined
        if (Array.isArray(value)) {
          arrayOptions = {
            push: action((newval: any) => {
              value.push(newval)
            }),
            remove: action((index: number) => {
              value.splice(index, 1)
            }),
            set: (newval: any) => {
              setAll(newval)
            },
            reset: () => {
              setAll(original)
            },
            emptyItem: createEmptyObject(original[0]),
          }
        } else if (Array.isArray(value[v.name])) {
          arrayOptions = {
            push: action((newval: any) => {
              value[v.name].push(newval)
            }),
            remove: action((index: number) => {
              value[v.name].splice(index, 1)
            }),
            set: (newval: any) => {
              set(k, newval)
            },
            reset: () => {
              set(k, original[v.name])
            },
            emptyItem: createEmptyObject(original[v.name][0]),
          }
        }

        return (
          <React.Fragment key={k}>
            <PureField
              key={k}
              name={v.name}
              value={value[v.name]}
              type={v.type}
              onChange={(newval: any) => {
                set(k, newval)
              }}
              arrayOptions={arrayOptions}
            />
          </React.Fragment> 
        )
      })}
    </Section>
  )
}

const generateDef = (value: any, parent?: any): IDefRecord => {
  const def = {}

  if (typeof value !== 'object') {
    if (Array.isArray(parent)) {
      throw new Error(
        `
[components/ui/field/JsonEditor]

Array element must be an object, current value is ${typeof value}: ${JSON.stringify(
          value,
          null,
          2
        )}.
Please change current value into an object.

Parent Array:
${JSON.stringify(parent, null, 2)}
`
      )
    } else {
      throw new Error(
        `
[components/ui/field/JsonEditor] Value is not an object: 
${JSON.stringify(value, null, 2)}

Parent Value:
${JSON.stringify(parent, null, 2)}
`
      )
    }
  }

  for (let [k, v] of Object.entries(value)) {
    let type: any = typeof v

    if (typeof v === 'object') {
      if (Array.isArray(v)) {
        type = 'array'
      } else if (v instanceof Date) {
        type = 'date'
      } else {
        type = 'json'
      }
    }

    def[k] = {
      name: k,
      type: type,
    }
  }

  return def
}

const createEmptyObject = (value: any) => {
  const result: any = {}

  for (let [k, v] of Object.entries(value)) {
    let val: any = ''
    switch (typeof v) {
      case 'number':
        val = 0
        break
      case 'object':
        if (v instanceof Date) {
          val = new Date()
        } else {
          val = createEmptyObject(val)
        }
        break
    }

    result[k] = val
  }

  return result
}
