import React from 'react'
import { Toggle } from '@fluentui/react/lib/Toggle'

interface IProps {
  style?: any
  label?: string
  defaultChecked?: boolean
  onChange: (event: any, checked?: boolean) => void
  onText?: string
  offText?: string
}

export default ({
  label,
  defaultChecked = true,
  onChange,
  onText = 'Yes',
  offText = 'No',
}: IProps) => {
  return (
    <Toggle
      label={label}
      defaultChecked={defaultChecked}
      onText={onText}
      offText={offText}
      styles={{ pill: 'focus:outline-none' }}
      onChange={(event: React.MouseEvent<HTMLElement>, checked?: boolean) => {
        onChange(event, checked)
      }}
    />
  )
}
