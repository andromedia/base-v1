import { db } from 'libs';
import { runInAction } from 'mobx';
import { observer, useLocalObservable } from 'mobx-react-lite';
import React from 'react';

const Page = (props: any) => {
  const state = useLocalObservable(() => ({
    count: 0,
    data: {},
  }));

  return (
    <div>
      {state.count}
      <pre>{JSON.stringify(state.data, null, 2)}</pre>

      <button
        className="bg-red-300 border-black"
        onClick={async () => {
          const rows = await db.p_user.query();

          runInAction(() => {
            state.count++;
            state.data = rows;
          });
        }}
      >
        asdasd  sadas
      </button>
    </div>
  );
};

export default Page;
