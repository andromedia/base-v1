import type { Server } from 'libs'
import { observer } from 'mobx-react-lite'
import React from 'react'

const Page = (props: any) => {
  const { children } = props
  return (
    <div className="flex flex-col flex-1 h-screen">
      <div className="flex px-2">
        INI HEADER LAYOUT {props.data} {props.isLoading ? 'loading' : ''}
      </div>
      <div className="flex flex-1 ">
        <div className="flex flex-col flex-1 w-full px-2">{children}</div>
      </div>
    </div>
  )
}
export default observer(Page)
