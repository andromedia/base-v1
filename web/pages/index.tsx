import { PrimaryButton } from '@fluentui/react'
import { QueryForm } from 'components/ui/form/QueryForm'
import { QueryList } from 'components/ui/list/QueryList'
import { Link } from 'components/ui/router'
import { set } from 'lodash-es'
import { observer } from 'mobx-react-lite'
import React from 'react'

const Page = (props: any) => {
  return (
    <>
      <Link href="/halo">ke halo</Link> {props.data}
      <QueryForm
        db="main"
        table="p_user"
        where={{
          id: 1,
        }}
        log={true}
        alter={(def) => {
          set(def, 'columns.json.type', 'json')
          return def
        }}
        // layout={{ colNum: 1 }}
        // log={true}
        // data={{ username: 'asd' }}
        // submit={(save: any, data: any) => {
        //   save()
        // }}
      >
        {/* {({ Field, Section }) => {
          return (
            <Section colNum={3} title="Product Description">
              <Field name="email"></Field>
              <Field name="username"></Field>
              <Section colNum={2}>
                <Field name="id"></Field>
                <Field name="last_login"></Field>
              </Section>
              <Field name="password"></Field>
            </Section>
          )
        }} */}
      </QueryForm>
      {/* <QueryList
        db="main"
        table="p_user"
        params={{
          select: {
            username: true,
            password: true,
            id: true,
            email: true,
            p_user_role: true,
          },
        }}
        columns={[
          'password',
          ['id', { width: 30 }],
          ['username', { width: 500 }],
          ['email', { width: 300 }],
          'p_user_role',
          (row) => {
            return {
              key: 'mantab jiwa',
              value: <PrimaryButton>{row.username}</PrimaryButton>,
            }
          },
        ]}
      /> */}
    </>
  )
}

export default observer(Page)
