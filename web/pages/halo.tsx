import { Link } from 'components/ui/router'
import React from 'react'

const Page = (props: any) => {
  return (
    <div>
      Halo {props.data}
      <br />
      <Link href="/mantab/halo">Ini Mantab Halo</Link>
      <br />
      <Link href="/">Ke Index</Link>
    </div>
  )
}
export default Page
