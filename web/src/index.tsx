import { initApp } from 'components/ui/router/init-app'
import pages from './pages'
import { initializeIcons } from '@fluentui/react/lib/Icons'

if (import.meta.env.MODE === 'development') {
  import('./tailwind.css')
} else {
  import('./tailwind-prod.css')
}

if (!(window as any).iconinit) {
  ;(window as any).iconinit = true
  initializeIcons(/* optional base url */)
}

initApp(pages)

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://snowpack.dev/concepts/hot-module-replacement
if (import.meta.hot) {
  import.meta.hot.accept()
}
