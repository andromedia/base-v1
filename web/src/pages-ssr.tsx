
import p_layout from "../pages/_layout";
import pAdmin from "../pages/admin";
import pContoh__id_ from "../pages/contoh.[id]";
import pHalo from "../pages/halo";
import pIndex from "../pages/index";
import pMantab_layout from "../pages/mantab/_layout";
import pMantabA from "../pages/mantab/a";
import pMantabHalo from "../pages/mantab/halo";
import React from 'react';
import { renderToString } from 'react-dom/server';
import MainRouter from 'components/ui/router/router';
import * as router from 'components/ui/router';

const pages = {
  "/_layout": {
  c:p_layout,
  s:false
},
  "/admin": {
  c:pAdmin,
  s:false
},
  "/contoh.[id]": {
  c:pContoh__id_,
  s:false
},
  "/halo": {
  c:pHalo,
  s:true
},
  "/index": {
  c:pIndex,
  s:true
},
  "/mantab/_layout": {
  c:pMantab_layout,
  s:true
},
  "/mantab/a": {
  c:pMantabA,
  s:false
},
  "/mantab/halo": {
  c:pMantabHalo,
  s:true
}
};
export default {
  pages,
  MainRouter,
  React,
  router,
  renderToString
};
