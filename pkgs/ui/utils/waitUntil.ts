export const waitUntil = (f: any): Promise<boolean> => {
  return new Promise((resolve) => {
    const ival = setInterval(() => {
      if (f()) {
        clearInterval(ival)
        resolve(true)
      }
    }, 10)
  })
}
