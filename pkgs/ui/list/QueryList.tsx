import {
  Callout,
  Checkbox,
  DefaultButton,
  DetailsList,
  DetailsListLayoutMode,
  IColumn as FluentColumn,
  SelectionMode,
} from '@fluentui/react'
import { dbAll, waitUntil } from 'libs'
import { get, startCase as startCaseLodash } from 'lodash-es'
import { action, runInAction, toJS } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import { default as React, useEffect, useRef } from 'react'
import NiceValue from '../field/NiceValue'
import FilterString from './filter/FilterString'
import './QueryList.css'

const startCase = (str: string) => {
  const res = startCaseLodash(str)
  return res
    .split(' ')
    .map((e) => {
      if (e.length <= 1) {
        return ''
      }
      return e
    })
    .join(' ')
    .trim()
}

interface IPagination {
  pageSize: number
}

type IColumnDetail = {
  key?: string
  title?: string
  width?: number
}

type IColumn<K = any> =
  | keyof K
  | [keyof K, IColumnDetail]
  | ((
      row: K
    ) => IColumnDetail & {
      value: any
    })

type IAction = {
  title: string
  selection?: boolean
  action: (rows: any, selectedRows?: any[]) => any
}

type IFilter = {
  key: string
  type: string
  default?: string
  operator?: string
}
type Unpromise<T extends Promise<any>> = T extends Promise<infer U> ? U : never

export const QueryList = observer(
  <
    J extends keyof typeof dbAll,
    K extends keyof typeof dbAll[J],
    CLASS = ReturnType<
      typeof dbAll[J][K][Extract<keyof typeof dbAll[J][K], 'definition'>]
    >,
    RELATIONS = CLASS[Extract<keyof CLASS, 'relations'>],
    COLUMNS = CLASS[Extract<keyof CLASS, 'columns'>],
    PARAMS = Parameters<
      typeof dbAll[J][K][Extract<keyof typeof dbAll[J][K], 'query'>]
    >[0],
    SELECT = PARAMS[Extract<keyof PARAMS, 'select'>]
  >(props: {
    db: J
    table: K
    title?: string
    onRowClick?:
      | 'edit-inline'
      | 'edit-popup'
      | 'view-inline'
      | 'view-popup'
      | ((row: any) => any)
    pagination?: IPagination
    actions?: IAction[]
    className?: string
    checkbox?: 'none' | 'single' | 'multiple'
    params?: PARAMS
    columns?: IColumn<
      keyof PARAMS extends never
        ? Record<keyof COLUMNS, any>
        : Record<keyof SELECT, any>
    >[]
    filters?: (string | IFilter)[]
  }) => {
    const { db, table, params, columns, actions, title, filters } = props
    const pickerRef = useRef(null as any)
    const meta = useLocalObservable(() => ({
      list: [],
      params: params as any,
      scroll: {
        left: 0,
        top: 0,
      },
      where: {
        original: undefined as any,
        filterValue: {} as any,
        showPicker: false,
        fieldVisibles: {} as any,
      },
      columns: formatColumns(columns || []),
      def: {
        db: {} as {
          name: string
          type: 'postgresql'
        },
        columns: {},
        rels: {},
      } as any,
      filters: filters,
      sort: (column: string) => {
        const cols = meta.columns.filter((e) => e.fieldName === column)
        if (cols.length > 0) {
          for (let col of cols) {
            const def = meta.def.columns[col.fieldName || '']
            if ((col as any).customRender || !def) return

            meta.columns.forEach((e) => {
              if (e.fieldName !== col.fieldName) {
                delete e.isSorted
                delete e.isSortedDescending
              }
            })
            if (!col.isSorted) {
              col.isSorted = true
            } else {
              if (col.isSortedDescending) {
                delete col.isSorted
                delete col.isSortedDescending
              } else {
                col.isSortedDescending = true
              }
            }

            const mpar: any = meta.params
            if (!col.isSorted) {
              mpar.order = []
            } else {
              mpar.order = [
                {
                  column: col.fieldName,
                  order: col.isSortedDescending ? 'desc' : 'asc',
                },
              ]
            }
          }
        }
        meta.columns = [...meta.columns]
      },
    }))

    const defaultActions: IAction[] = [
      // {
      //   title: 'Create',
      //   action: () => {
      //     alert('Creating')
      //   },
      // },
    ]

    const query = async () => {
      const def = await (dbAll[db][table] as any)['definition']()
      const res = await (dbAll[db][table] as any)['query'](meta.params)
      runInAction(() => {
        if (meta.where.original === undefined) {
          meta.where.original = toJS(get(meta.params, 'where') || {})
        }
        meta.list = res
        meta.def = def
        if (meta.columns.length === 0) {
          meta.columns = formatColumns(generateColumns(meta.list[0]))
        }

        if (!meta.filters || meta.filters.length === 0) {
          const select = get(meta.params, 'select')
          if (select) {
            meta.filters = generateFiltersFromSelect(select, def)
          } else {
            meta.filters = generateFiltersFromColumns(meta.columns, def)
          }
        }
      })
    }

    const submit = () => {
      const formatFilter = (filterValues: object): any => {
        const result: any = {}
        for (let [k, e] of Object.entries(filterValues)) {
          const col = meta.def.columns[k]
          const db = meta.def.db.type
          if (col && col.type) {
            switch (col.type) {
              case 'string':
                if (!!e) {
                  let criteria = `%${e}%`
                  if (e.indexOf('%') >= 0) {
                    criteria = e
                  }
                  result[k] = [db === 'postgresql' ? 'ilike' : 'like', criteria]
                }
                break
            }
          }
        }
        return result
      }

      meta.params.where = {
        ...meta.where.original,
        ...formatFilter(meta.where.filterValue),
      }

      query()
    }

    useEffect(() => {
      ;(async () => {
        await query()

        runInAction(() => {
          if (Object.keys(meta.where.fieldVisibles).length === 0) {
            for (let i = 0; i < 4; i++) {
              if (i < meta.columns.length) {
                const e = meta.columns[i]
                if (e) {
                  meta.where.fieldVisibles[e.fieldName] = true
                }
              }
            }
          }
        })
      })()
    }, [])

    const dref = React.useRef(null)
    useEffect(() => {
      const scroll = meta.scroll
      const isMobile = false
      waitUntil(() => get(dref, 'current._root.current')).then(() => {
        const el = get(dref, 'current._root.current')
        if (el) {
          const grid = isMobile ? el : el.children[0]
          grid.scrollTop = scroll.top
          grid.scrollLeft = scroll.left

          let trycount = 0
          let tryset: any = setInterval(() => {
            grid.scrollTop = scroll.top
            grid.scrollLeft = scroll.left
            trycount++

            if (
              trycount > 100 ||
              (scroll.top === grid.scrollTop && scroll.left === grid.scrollLeft)
            )
              clearInterval(tryset)
          }, 10)
          grid.onscroll = (e: any) => {
            if (tryset) {
              clearInterval(tryset)
              tryset = undefined
            }
            e.target.children[0].style.top = e.target.scrollTop + 'px'
            runInAction(
              () =>
                (meta.scroll = {
                  top: e.target.scrollTop,
                  left: e.target.scrollLeft,
                })
            )
          }
        }
      })
    }, [dref.current])

    let selMode = SelectionMode.none
    if (props.checkbox) {
      switch (props.checkbox) {
        case 'multiple':
          selMode = SelectionMode.multiple
          break
        case 'single':
          selMode = SelectionMode.single
          break
      }
    }

    return (
      <div
        className={
          props.className +
          ' ui-querylist flex flex-auto flex-col items-stretch overflow-hidden'
        }
      >
        <div className="flex flex-row justify-between mb-2">
          <div className="flex text-lg font-semibold">{title}</div>
          <div className="flex flex-row">
            {(actions || defaultActions).map((e, idx) => {
              return (
                <DefaultButton key={idx} onClick={e.action}>
                  {e.title}
                </DefaultButton>
              )
            })}
          </div>
        </div>
        <div className="flex flex-row items-center ui-querylist-filter">
          <div className="picker" ref={pickerRef}>
            <DefaultButton
              onClick={action(() => (meta.where.showPicker = true))}
              iconProps={{ iconName: 'GlobalNavButton' }}
            />
          </div>
          {meta.where.showPicker && (
            <Callout
              onDismiss={action(() => (meta.where.showPicker = false))}
              setInitialFocus={true}
              target={pickerRef.current}
            >
              <div
                style={{
                  padding: 10,
                  display: 'flex',
                  width: '270px',
                  flexWrap: 'wrap',
                  flexDirection: 'row',
                }}
              >
                {(meta.filters || []).map((f, key) => {
                  const filter = filterFromString(f, meta)
                  return (
                    <Checkbox
                      key={filter.key}
                      styles={{
                        root: {
                          marginBottom: 3,
                          marginRight: 3,
                          width: '120px',
                        },
                      }}
                      label={startCase(filter.key)}
                      checked={!!meta.where.fieldVisibles[filter.key]}
                      onChange={action(() => {
                        if (meta.where.fieldVisibles[filter.key]) {
                          meta.where.fieldVisibles[filter.key] = false
                        } else {
                          meta.where.fieldVisibles[filter.key] = true
                        }
                      })}
                    />
                  )
                })}
              </div>
            </Callout>
          )}
          {(meta.filters || []).map((f, index) => {
            const filter = filterFromString(f, meta)
            if (!meta.where.fieldVisibles[filter.key]) return null
            return (
              <Filter
                value={meta.where.filterValue[filter.key]}
                setValue={action((val: any) => {
                  switch (filter.type) {
                    case 'string':
                      meta.where.filterValue[filter.key] = val
                      break
                  }
                })}
                filter={filter}
                key={index}
                submit={submit}
              />
            )
          })}
        </div>

        <div className="flex flex-1">
          <DetailsList
            selectionMode={selMode}
            items={meta.list}
            componentRef={dref}
            onShouldVirtualize={() => true}
            compact={true}
            layoutMode={DetailsListLayoutMode.fixedColumns}
            onColumnHeaderClick={(
              ev?: React.MouseEvent<HTMLElement>,
              fcol?: any
            ) => {
              if (fcol) {
                meta.sort(fcol.fieldName)
                query()
              }
            }}
            columns={meta.columns}
            onRenderItemColumn={renderItem.bind(columns)}
          />
        </div>
      </div>
    )
  }
)

const Filter = observer(
  ({
    filter,
    value,
    setValue,
    submit,
  }: {
    filter: IFilter
    value: any
    setValue: (val: any) => void
    submit: () => void
  }) => {
    if (typeof filter !== 'string') {
      switch (filter.type) {
        case 'string':
          return (
            <FilterString
              field={filter.key}
              setValue={setValue}
              label={startCase(filter.key)}
              value={value}
              submit={submit}
            />
          )
          break
      }
    }

    return null
  }
)

const renderItem = function (
  this: any,
  item: any,
  rowNum?: number,
  column?: FluentColumn
) {
  if (column) {
    const value = item[(column as any).fieldName]
    const col = this[(column as any).idx]

    if (col) {
      if (typeof col === 'function') {
        const result = col(item)
        return <div className="ui-querylist-custom">{result.value}</div>
      }
    }
    if (typeof value === 'object') return <NiceValue value={value} />
    return value
  }
  return ''
}
const filterFromString = (f: string | IFilter, meta: any): IFilter => {
  let filter: any = { key: '', type: '' }
  if (typeof f === 'string') {
    filter.key = f
    const col = meta.def.columns[f]
    if (col) {
      filter.type = col.type
    }

    if (!filter.type) {
      const rel = meta.def.rels[f]
      if (rel) {
        filter.type = 'relation'
        filter.operator = toJS(rel)
      }
    }
  } else {
    filter = f
  }

  return filter
}

const generateFiltersFromSelect = (select: any, def: any): string[] => {
  return Object.keys(select)
    .map((e) => {
      if (def.columns[e]) {
        return e
      }
      return ''
    })
    .filter((e) => !!e)
}

const generateFiltersFromColumns = (columns: any[], def: any): IFilter[] => {
  const result: any = {}

  columns.forEach((e) => {
    if (def.columns[e.fieldName]) {
      result[e.fieldName] = {
        key: e.fieldName,
        type: def.columns[e.fieldName].type,
      }
    }
  })

  return Object.values(result)
}

const generateColumns = (row: Record<string, any>): IColumn[] => {
  return Object.keys(row)
}

const formatColumns = (
  columns: IColumn[]
): (FluentColumn & { fieldName: string })[] => {
  return columns.map((column, idx) => {
    let colName = ''
    let title = ''
    let render: any = null
    let maxWidth = 120

    const applyCol = (col: IColumnDetail) => {
      if (col.key) {
        colName = col.key
      }
      if (col.title) {
        title = col.title
      } else {
        title = startCase(colName)
      }

      if (col.width) {
        maxWidth = col.width
      }
    }

    if (typeof column === 'function') {
      applyCol(column({}))
      render = column
    } else if (typeof column === 'string') {
      colName = column
      title = startCase(colName)
    } else if (Array.isArray(column)) {
      colName = column[0] as any
      if (column[1]) {
        applyCol(column[1])
      }
    }

    return {
      key: idx.toString(),
      idx,
      fieldName: colName,
      name: title,
      isResizable: true,
      minWidth: 50,
      maxWidth: maxWidth,
      customRender: render,
    }
  })
}
