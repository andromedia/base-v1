/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import { DefaultButton, IconButton, Label } from '@fluentui/react'
import { dbAll } from 'libs'
import { get, startCase as startCaseLodash } from 'lodash-es'
import { runInAction, toJS } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import { useEffect, useRef } from 'react'
import DatePicker from '../field/DatePicker'
import { JsonEditor } from '../field/JsonEditor'
import TextField from '../field/TextField'
import Toggle from '../field/Toggle'
import printJSX from './print-jsx'
import React from 'react'

const startCase = (str: string) => {
  const res = startCaseLodash(str)
  return res
    .split(' ')
    .map((e) => {
      if (e.length <= 1) {
        return ''
      }
      return e
    })
    .join(' ')
    .trim()
}
type IColumnDetail = {
  key?: string
  title?: string
  required?: boolean
}

type ISection = {
  colNum?: number
  title?: string
  className?: string
  children?: any
}

type IField = {
  name: string
}

export type IFieldType =
  | 'string'
  | 'json'
  | 'array'
  | 'boolean'
  | 'number'
  | 'date'

export const QueryForm = observer(
  <
    J extends keyof typeof dbAll,
    K extends keyof typeof dbAll[J],
    WHERE = Parameters<
      typeof dbAll[J][K][Extract<keyof typeof dbAll[J][K], '_types'>]
    >[0]
  >(props: {
    db: J
    table: K
    className?: string
    where?: WHERE
    alter?: (def) => any
    title?: string
    log?: boolean
    layout?: {
      colNum?: number
    }
    children?:
      | never[]
      | ((props: {
          Section: (props: ISection) => any
          Field: (props: IField) => any
        }) => any)
    data?: Record<string, any>
    submit?: () => void
  }) => {
    const {
      db,
      table,
      title,
      children,
      log,
      data,
      where,
      alter,
      layout,
    } = props

    const meta = useLocalObservable(() => ({
      def: {
        db: {} as {
          name: string
          type: 'postgresql'
        },
        columns: null,
        rels: {},
      } as any,
      data: data || {},
      Field: null as any,
    }))

    const logged = useRef(false)
    let renderChildren = <div>Loading ...</div>

    if (meta.def.columns !== null && meta.Field !== null) {
      if (typeof children === 'function') {
        renderChildren = children({
          Section,
          Field: meta.Field,
        })
      } else {
        renderChildren = renderDefault(meta, layout)
      }
      if (!logged.current && log) {
        logJSX(renderChildren)
      }
    }

    const init = async () => {
      let def = await (dbAll[db][table] as any)['definition']()
      let fetchData = {}

      if (alter) {
        def = alter(def)
      }

      if (where) {
        const res = await (dbAll[db][table] as any)['query']({
          where,
          limit: 1,
        })
        if (res.length > 0) {
          runInAction(() => {
            fetchData = res[0]
          })
        }
      }
      runInAction(() => {
        meta.def = def
        meta.Field = Field.bind(meta)
        if (Object.keys(fetchData).length > 0) {
          meta.data = fetchData
        }
      })
    }

    useEffect(() => {
      init()
    }, [])

    return (
      <div
        className={
          props.className +
          ' ui-querylist flex flex-auto flex-col items-stretch overflow-hidden'
        }
      >
        <div className="flex flex-row justify-between mb-2">
          <div className="flex text-lg font-semibold">
            {title || startCase((table as any) || '')}
          </div>
          <div className="flex flex-row"></div>
        </div>

        <pre className="text-xs">{JSON.stringify(meta.data, null, 2)}</pre>
        <form>{meta.def.columns === null ? null : renderChildren}</form>
      </div>
    )
  }
)

export const Section = ({
  title,
  colNum = 3,
  children,
  className,
}: ISection) => {
  const width = `${100 / colNum}%`

  let fields = children

  if (!Array.isArray(children)) {
    fields = [children]
  }

  return (
    <>
      {title && <h4 className="font-bold">{title}</h4>}
      <div className={'flex flex-row flex-wrap ' + className}>
        {fields.map((item: any, idx: number) => {
          return (
            <div key={idx} className="" style={{ width }}>
              {item}
            </div>
          )
        })}
      </div>
    </>
  )
}

export const PureField = function (props: {
  name: string
  value: any
  onChange: (value: any) => void
  type: IFieldType
  arrayOptions?: {
    push: (value: any) => void
    remove: (index: number) => void
    set: (value: any[]) => void
    reset: () => void
    emptyItem: any
  }
  originalValue?: any
}) {
  const { name, value, type, onChange, arrayOptions } = props
  let typeField = ''
  const arrayRef = useRef(null as any)

  const changeEvent = {
    date: (date: Date | null | undefined) => {
      onChange(date)
    },
    text: (text: string | undefined) => {
      onChange(text)
    },
    toggle: (event: any, checked?: boolean) => {
      onChange(checked)
    },
    json: (value: any) => {
      onChange(value)
    },
    array: (value: any) => {
      onChange(value)
    },
  }

  const renderField = () => {
    switch (type.toLowerCase()) {
      case 'string':
        typeField = name === 'password' ? 'password' : 'text'
        return (
          <TextField
            label={startCase(name)}
            value={value}
            type={typeField}
            onChange={changeEvent.text}
          />
        )
      case 'json':
        const fromArray = !!arrayOptions
        const remove = get(arrayOptions, 'remove')
        return (
          <div
            className={
              `-ml-2 ` +
              (fromArray
                ? `-mr-2 flex flex-row items-stretch border-b  border-r border-gray-300`
                : 'flex flex-col pb-5')
            }
          >
            <Label
              className={
                fromArray
                  ? `border-r border-l border-gray-300 px-3 w-10 flex items-center justify-center text-lg text-gray-600`
                  : `ml-2 border-b border-gray-300`
              }
            >
              {fromArray ? parseInt(name) + 1 : startCase(name)}
            </Label>
            <div className={`flex-1 flex ` + (fromArray ? 'pb-2' : '')}>
              <JsonEditor
                useFormTag={false}
                useMeta={false}
                value={value}
                colNum={Object.keys(value).length === 1 ? 1 : 2}
                onChange={changeEvent.json}
              />
            </div>
            {fromArray && (
              <div className="flex items-stretch justify-center border-l border-gray-300">
                <IconButton
                  onClick={() => {
                    if (remove) {
                      remove(parseInt(name))
                    }
                  }}
                  className="flex flex-1 h-auto"
                  iconProps={{ iconName: 'Delete' }}
                />
              </div>
            )}
          </div>
        )
      case 'array':
        const reset = get(arrayOptions, 'reset')
        const push = get(arrayOptions, 'push')
        const emptyItem = get(arrayOptions, 'emptyItem')
        return (
          <div className={'flex flex-col'}>
            <div className="flex flex-row items-center justify-between border-b border-gray-300">
              <Label>{startCase(name.toString())}</Label>
              <div className="flex flex-row">
                <div
                  className="flex items-center px-2 text-xs transition-all cursor-pointer hover:bg-gray-200"
                  onClick={() => {
                    if (
                      confirm(
                        `Reset list "${startCase(name)}" to original value ?`
                      ) &&
                      reset
                    )
                      reset()
                  }}
                >
                  Reset
                </div>
                <div
                  className="flex items-center px-3 py-1 ml-2 text-xs text-white transition-all bg-blue-500 rounded-sm cursor-pointer hover:bg-gray-700"
                  onClick={() => {
                    if (push && emptyItem) {
                      push(emptyItem)

                      setTimeout(() => {
                        const arr = arrayRef.current
                        if (arr) {
                          if (arr.scrollIntoViewIfNeeded) {
                            arr.scrollIntoViewIfNeeded()
                          } else if (arr.scrollIntoView) {
                            arr.scrollIntoView()
                          }

                          arr.scrollTo(0, 9999999)
                        }
                      })
                    }
                  }}
                >
                  + New Item
                </div>
              </div>
            </div>
            <div
              className="flex flex-col"
              ref={arrayRef}
              css={css`
                max-height: 500px;
                overflow: auto;
              `}
            >
              <JsonEditor
                value={value}
                onChange={changeEvent.array}
                useMeta={false}
                colNum={1}
                useFormTag={false}
              />
            </div>
          </div>
        )
      case 'boolean':
        return <Toggle label={startCase(name)} onChange={changeEvent.toggle} />
      case 'number':
        typeField = 'number'
        return (
          <TextField
            label={startCase(name)}
            value={value}
            onChange={changeEvent.text}
            type={typeField}
          />
        )
      case 'date':
        return (
          <DatePicker
            label={startCase(name)}
            value={value}
            onSelectDate={changeEvent.date}
          />
        )
      default:
        break
    }
  }

  return <div className="px-2">{renderField()}</div>
}

const Field = function (this: any, { name }: IField) {
  const meta = this

  const type = meta.def.columns[name].type.toLowerCase()
  return (
    <PureField
      name={name}
      value={meta.data[name]}
      type={type}
      onChange={(value: any) => {
        runInAction(() => {
          meta.data[name] = value
        })
      }}
    />
  )
}

const renderDefault = (meta: any, layout) => {
  const columns = Object.keys(meta.def.columns)
  const BoundField = meta.Field
  const colNum = get(layout, 'colNum', 2)
  return (
    <Section colNum={colNum}>
      {columns
        .filter((name) => name.toLowerCase() !== 'id')
        .map((name: any, idx: number) => {
          const type = meta.def.columns[name].type.toLowerCase()

          if (type === 'json') {
            return (
              <BoundField key={idx} name={name}>
                halo sodara
              </BoundField>
            )
          }

          return <BoundField key={idx} name={name} />
        })}
    </Section>
  )
}

const logJSX = (renderChildren: any) => {
  const res = printJSX(renderChildren).replace(/\<bound\s+Field/gi, '<Field')
  console.log(
    `
{({ Field, Section }) => {
  return (
    ${res.split('\n').join('\n    ')}
)
}}`.trim()
  )
}
