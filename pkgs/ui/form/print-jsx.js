const isSSR = window.isSSR

var __defineProperty = Object.defineProperty;
var __hasOwnProperty = Object.prototype.hasOwnProperty;
var __commonJS = (callback, module) => () => {
    if (!module) {
        module = { exports: {} };
        callback(module.exports, module);
    }
    return module.exports;
};
var __markAsModule = (target) => {
    return __defineProperty(target, "__esModule", { value: true });
};
var __exportStar = (target, module) => {
    __markAsModule(target);
    if (typeof module === "object" || typeof module === "function") {
        for (let key in module)
            if (__hasOwnProperty.call(module, key) && !__hasOwnProperty.call(target, key) && key !== "default")
                __defineProperty(target, key, { get: () => module[key], enumerable: true });
    }
    return target;
};
var __toModule = (module) => {
    if (module && module.__esModule)
        return module;
    return __exportStar(__defineProperty({}, "default", { value: module, enumerable: true }), module);
};

// node_modules/object-assign/index.js
var require_object_assign = __commonJS((exports, module) => {
    /*
    object-assign
    (c) Sindre Sorhus
    @license MIT
    */
    "use strict";
    var getOwnPropertySymbols = Object.getOwnPropertySymbols;
    var hasOwnProperty = Object.prototype.hasOwnProperty;
    var propIsEnumerable = Object.prototype.propertyIsEnumerable;
    function toObject(val) {
        if (val === null || val === void 0) {
            throw new TypeError("Object.assign cannot be called with null or undefined");
        }
        return Object(val);
    }
    function shouldUseNative() {
        try {
            if (!Object.assign) {
                return false;
            }
            var test1 = new String("abc");
            test1[5] = "de";
            if (Object.getOwnPropertyNames(test1)[0] === "5") {
                return false;
            }
            var test2 = {};
            for (var i = 0; i < 10; i++) {
                test2["_" + String.fromCharCode(i)] = i;
            }
            var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
                return test2[n];
            });
            if (order2.join("") !== "0123456789") {
                return false;
            }
            var test3 = {};
            "abcdefghijklmnopqrst".split("").forEach(function (letter) {
                test3[letter] = letter;
            });
            if (Object.keys(Object.assign({}, test3)).join("") !== "abcdefghijklmnopqrst") {
                return false;
            }
            return true;
        } catch (err) {
            return false;
        }
    }
    module.exports = shouldUseNative() ? Object.assign : function (target, source) {
        var from;
        var to = toObject(target);
        var symbols;
        for (var s = 1; s < arguments.length; s++) {
            from = Object(arguments[s]);
            for (var key in from) {
                if (hasOwnProperty.call(from, key)) {
                    to[key] = from[key];
                }
            }
            if (getOwnPropertySymbols) {
                symbols = getOwnPropertySymbols(from);
                for (var i = 0; i < symbols.length; i++) {
                    if (propIsEnumerable.call(from, symbols[i])) {
                        to[symbols[i]] = from[symbols[i]];
                    }
                }
            }
        }
        return to;
    };
});

// node_modules/react/cjs/react.production.min.js
var require_react_production_min = __commonJS((exports) => {
    /** @license React v16.13.1
     * react.production.min.js
     *
     * Copyright (c) Facebook, Inc. and its affiliates.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */
    "use strict";
    var l = require_object_assign();
    var n = "function" === typeof Symbol && Symbol.for;
    var p = n ? Symbol.for("react.element") : 60103;
    var q = n ? Symbol.for("react.portal") : 60106;
    var r = n ? Symbol.for("react.fragment") : 60107;
    var t = n ? Symbol.for("react.strict_mode") : 60108;
    var u = n ? Symbol.for("react.profiler") : 60114;
    var v = n ? Symbol.for("react.provider") : 60109;
    var w = n ? Symbol.for("react.context") : 60110;
    var x = n ? Symbol.for("react.forward_ref") : 60112;
    var y = n ? Symbol.for("react.suspense") : 60113;
    var z = n ? Symbol.for("react.memo") : 60115;
    var A = n ? Symbol.for("react.lazy") : 60116;
    var B = "function" === typeof Symbol && Symbol.iterator;
    function C(a) {
        for (var b = "https://reactjs.org/docs/error-decoder.html?invariant=" + a, c = 1; c < arguments.length; c++)
            b += "&args[]=" + encodeURIComponent(arguments[c]);
        return "Minified React error #" + a + "; visit " + b + " for the full message or use the non-minified dev environment for full errors and additional helpful warnings.";
    }
    var D = {
        isMounted: function () {
            return false;
        }, enqueueForceUpdate: function () {
        }, enqueueReplaceState: function () {
        }, enqueueSetState: function () {
        }
    };
    var E = {};
    function F(a, b, c) {
        this.props = a;
        this.context = b;
        this.refs = E;
        this.updater = c || D;
    }
    F.prototype.isReactComponent = {};
    F.prototype.setState = function (a, b) {
        if ("object" !== typeof a && "function" !== typeof a && null != a)
            throw Error(C(85));
        this.updater.enqueueSetState(this, a, b, "setState");
    };
    F.prototype.forceUpdate = function (a) {
        this.updater.enqueueForceUpdate(this, a, "forceUpdate");
    };
    function G() {
    }
    G.prototype = F.prototype;
    function H(a, b, c) {
        this.props = a;
        this.context = b;
        this.refs = E;
        this.updater = c || D;
    }
    var I = H.prototype = new G();
    I.constructor = H;
    l(I, F.prototype);
    I.isPureReactComponent = true;
    var J = { current: null };
    var K = Object.prototype.hasOwnProperty;
    var L = { key: true, ref: true, __self: true, __source: true };
    function M(a, b, c) {
        var e, d = {}, g = null, k = null;
        if (null != b)
            for (e in (void 0 !== b.ref && (k = b.ref), void 0 !== b.key && (g = "" + b.key), b))
                K.call(b, e) && !L.hasOwnProperty(e) && (d[e] = b[e]);
        var f = arguments.length - 2;
        if (1 === f)
            d.children = c;
        else if (1 < f) {
            for (var h = Array(f), m = 0; m < f; m++)
                h[m] = arguments[m + 2];
            d.children = h;
        }
        if (a && a.defaultProps)
            for (e in (f = a.defaultProps, f))
                void 0 === d[e] && (d[e] = f[e]);
        return { $$typeof: p, type: a, key: g, ref: k, props: d, _owner: J.current };
    }
    function N(a, b) {
        return { $$typeof: p, type: a.type, key: b, ref: a.ref, props: a.props, _owner: a._owner };
    }
    function O(a) {
        return "object" === typeof a && null !== a && a.$$typeof === p;
    }
    function escape2(a) {
        var b = { "=": "=0", ":": "=2" };
        return "$" + ("" + a).replace(/[=:]/g, function (a2) {
            return b[a2];
        });
    }
    var P = /\/+/g;
    var Q = [];
    function R(a, b, c, e) {
        if (Q.length) {
            var d = Q.pop();
            d.result = a;
            d.keyPrefix = b;
            d.func = c;
            d.context = e;
            d.count = 0;
            return d;
        }
        return { result: a, keyPrefix: b, func: c, context: e, count: 0 };
    }
    function S(a) {
        a.result = null;
        a.keyPrefix = null;
        a.func = null;
        a.context = null;
        a.count = 0;
        10 > Q.length && Q.push(a);
    }
    function T(a, b, c, e) {
        var d = typeof a;
        if ("undefined" === d || "boolean" === d)
            a = null;
        var g = false;
        if (null === a)
            g = true;
        else
            switch (d) {
                case "string":
                case "number":
                    g = true;
                    break;
                case "object":
                    switch (a.$$typeof) {
                        case p:
                        case q:
                            g = true;
                    }
            }
        if (g)
            return c(e, a, "" === b ? "." + U(a, 0) : b), 1;
        g = 0;
        b = "" === b ? "." : b + ":";
        if (Array.isArray(a))
            for (var k = 0; k < a.length; k++) {
                d = a[k];
                var f = b + U(d, k);
                g += T(d, f, c, e);
            }
        else if (null === a || "object" !== typeof a ? f = null : (f = B && a[B] || a["@@iterator"], f = "function" === typeof f ? f : null), "function" === typeof f)
            for (a = f.call(a), k = 0; !(d = a.next()).done;)
                d = d.value, f = b + U(d, k++), g += T(d, f, c, e);
        else if ("object" === d)
            throw c = "" + a, Error(C(31, "[object Object]" === c ? "object with keys {" + Object.keys(a).join(", ") + "}" : c, ""));
        return g;
    }
    function V(a, b, c) {
        return null == a ? 0 : T(a, "", b, c);
    }
    function U(a, b) {
        return "object" === typeof a && null !== a && null != a.key ? escape2(a.key) : b.toString(36);
    }
    function W(a, b) {
        a.func.call(a.context, b, a.count++);
    }
    function aa(a, b, c) {
        var e = a.result, d = a.keyPrefix;
        a = a.func.call(a.context, b, a.count++);
        Array.isArray(a) ? X(a, e, c, function (a2) {
            return a2;
        }) : null != a && (O(a) && (a = N(a, d + (!a.key || b && b.key === a.key ? "" : ("" + a.key).replace(P, "$&/") + "/") + c)), e.push(a));
    }
    function X(a, b, c, e, d) {
        var g = "";
        null != c && (g = ("" + c).replace(P, "$&/") + "/");
        b = R(b, g, e, d);
        V(a, aa, b);
        S(b);
    }
    var Y = { current: null };
    function Z() {
        var a = Y.current;
        if (null === a)
            throw Error(C(321));
        return a;
    }
    var ba = { ReactCurrentDispatcher: Y, ReactCurrentBatchConfig: { suspense: null }, ReactCurrentOwner: J, IsSomeRendererActing: { current: false }, assign: l };
    exports.Children = {
        map: function (a, b, c) {
            if (null == a)
                return a;
            var e = [];
            X(a, e, null, b, c);
            return e;
        }, forEach: function (a, b, c) {
            if (null == a)
                return a;
            b = R(null, null, b, c);
            V(a, W, b);
            S(b);
        }, count: function (a) {
            return V(a, function () {
                return null;
            }, null);
        }, toArray: function (a) {
            var b = [];
            X(a, b, null, function (a2) {
                return a2;
            });
            return b;
        }, only: function (a) {
            if (!O(a))
                throw Error(C(143));
            return a;
        }
    };
    exports.Component = F;
    exports.Fragment = r;
    exports.Profiler = u;
    exports.PureComponent = H;
    exports.StrictMode = t;
    exports.Suspense = y;
    exports.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = ba;
    exports.cloneElement = function (a, b, c) {
        if (null === a || void 0 === a)
            throw Error(C(267, a));
        var e = l({}, a.props), d = a.key, g = a.ref, k = a._owner;
        if (null != b) {
            void 0 !== b.ref && (g = b.ref, k = J.current);
            void 0 !== b.key && (d = "" + b.key);
            if (a.type && a.type.defaultProps)
                var f = a.type.defaultProps;
            for (h in b)
                K.call(b, h) && !L.hasOwnProperty(h) && (e[h] = void 0 === b[h] && void 0 !== f ? f[h] : b[h]);
        }
        var h = arguments.length - 2;
        if (1 === h)
            e.children = c;
        else if (1 < h) {
            f = Array(h);
            for (var m = 0; m < h; m++)
                f[m] = arguments[m + 2];
            e.children = f;
        }
        return {
            $$typeof: p,
            type: a.type,
            key: d,
            ref: g,
            props: e,
            _owner: k
        };
    };
    exports.createContext = function (a, b) {
        void 0 === b && (b = null);
        a = { $$typeof: w, _calculateChangedBits: b, _currentValue: a, _currentValue2: a, _threadCount: 0, Provider: null, Consumer: null };
        a.Provider = { $$typeof: v, _context: a };
        return a.Consumer = a;
    };
    exports.createElement = M;
    exports.createFactory = function (a) {
        var b = M.bind(null, a);
        b.type = a;
        return b;
    };
    exports.createRef = function () {
        return { current: null };
    };
    exports.forwardRef = function (a) {
        return { $$typeof: x, render: a };
    };
    exports.isValidElement = O;
    exports.lazy = function (a) {
        return { $$typeof: A, _ctor: a, _status: -1, _result: null };
    };
    exports.memo = function (a, b) {
        return { $$typeof: z, type: a, compare: void 0 === b ? null : b };
    };
    exports.useCallback = function (a, b) {
        return Z().useCallback(a, b);
    };
    exports.useContext = function (a, b) {
        return Z().useContext(a, b);
    };
    exports.useDebugValue = function () {
    };
    exports.useEffect = function (a, b) {
        return Z().useEffect(a, b);
    };
    exports.useImperativeHandle = function (a, b, c) {
        return Z().useImperativeHandle(a, b, c);
    };
    exports.useLayoutEffect = function (a, b) {
        return Z().useLayoutEffect(a, b);
    };
    exports.useMemo = function (a, b) {
        return Z().useMemo(a, b);
    };
    exports.useReducer = function (a, b, c) {
        return Z().useReducer(a, b, c);
    };
    exports.useRef = function (a) {
        return Z().useRef(a);
    };
    exports.useState = function (a) {
        return Z().useState(a);
    };
    exports.version = "16.13.1";
});

// node_modules/prop-types/lib/ReactPropTypesSecret.js
var require_ReactPropTypesSecret = __commonJS((exports, module) => {
    "use strict";
    var ReactPropTypesSecret = "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED";
    module.exports = ReactPropTypesSecret;
});

// node_modules/prop-types/checkPropTypes.js
var require_checkPropTypes = __commonJS((exports, module) => {
    "use strict";
    var printWarning = function () {
    };
    if (!isSSR) {
        var ReactPropTypesSecret = require_ReactPropTypesSecret();
        var loggedTypeFailures = {};
        printWarning = function (text) {
            var message = "Warning: " + text;
            if (typeof console !== "undefined") {
                console.error(message);
            }
            try {
                throw new Error(message);
            } catch (x) {
            }
        };
    }
    function checkPropTypes(typeSpecs, values, location, componentName, getStack) {
        if (!isSSR) {
            for (var typeSpecName in typeSpecs) {
                if (typeSpecs.hasOwnProperty(typeSpecName)) {
                    var error;
                    try {
                        if (typeof typeSpecs[typeSpecName] !== "function") {
                            var err = Error((componentName || "React class") + ": " + location + " type `" + typeSpecName + "` is invalid; it must be a function, usually from the `prop-types` package, but received `" + typeof typeSpecs[typeSpecName] + "`.");
                            err.name = "Invariant Violation";
                            throw err;
                        }
                        error = typeSpecs[typeSpecName](values, typeSpecName, componentName, location, null, ReactPropTypesSecret);
                    } catch (ex) {
                        error = ex;
                    }
                    if (error && !(error instanceof Error)) {
                        printWarning((componentName || "React class") + ": type specification of " + location + " `" + typeSpecName + "` is invalid; the type checker function must return `null` or an `Error` but returned a " + typeof error + ". You may have forgotten to pass an argument to the type checker creator (arrayOf, instanceOf, objectOf, oneOf, oneOfType, and shape all require an argument).");
                    }
                    if (error instanceof Error && !(error.message in loggedTypeFailures)) {
                        loggedTypeFailures[error.message] = true;
                        var stack = getStack ? getStack() : "";
                        printWarning("Failed " + location + " type: " + error.message + (stack != null ? stack : ""));
                    }
                }
            }
        }
    }
    module.exports = checkPropTypes;
});

// node_modules/react/cjs/react.development.js
var require_react_development = __commonJS((exports) => {
    /** @license React v16.13.1
     * react.development.js
     *
     * Copyright (c) Facebook, Inc. and its affiliates.
     *
     * This source code is licensed under the MIT license found in the
     * LICENSE file in the root directory of this source tree.
     */
    "use strict";
    if (!isSSR) {
        (function () {
            "use strict";
            var _assign = require_object_assign();
            var checkPropTypes = require_checkPropTypes();
            var ReactVersion = "16.13.1";
            var hasSymbol = typeof Symbol === "function" && Symbol.for;
            var REACT_ELEMENT_TYPE = hasSymbol ? Symbol.for("react.element") : 60103;
            var REACT_PORTAL_TYPE = hasSymbol ? Symbol.for("react.portal") : 60106;
            var REACT_FRAGMENT_TYPE = hasSymbol ? Symbol.for("react.fragment") : 60107;
            var REACT_STRICT_MODE_TYPE = hasSymbol ? Symbol.for("react.strict_mode") : 60108;
            var REACT_PROFILER_TYPE = hasSymbol ? Symbol.for("react.profiler") : 60114;
            var REACT_PROVIDER_TYPE = hasSymbol ? Symbol.for("react.provider") : 60109;
            var REACT_CONTEXT_TYPE = hasSymbol ? Symbol.for("react.context") : 60110;
            var REACT_CONCURRENT_MODE_TYPE = hasSymbol ? Symbol.for("react.concurrent_mode") : 60111;
            var REACT_FORWARD_REF_TYPE = hasSymbol ? Symbol.for("react.forward_ref") : 60112;
            var REACT_SUSPENSE_TYPE = hasSymbol ? Symbol.for("react.suspense") : 60113;
            var REACT_SUSPENSE_LIST_TYPE = hasSymbol ? Symbol.for("react.suspense_list") : 60120;
            var REACT_MEMO_TYPE = hasSymbol ? Symbol.for("react.memo") : 60115;
            var REACT_LAZY_TYPE = hasSymbol ? Symbol.for("react.lazy") : 60116;
            var REACT_BLOCK_TYPE = hasSymbol ? Symbol.for("react.block") : 60121;
            var REACT_FUNDAMENTAL_TYPE = hasSymbol ? Symbol.for("react.fundamental") : 60117;
            var REACT_RESPONDER_TYPE = hasSymbol ? Symbol.for("react.responder") : 60118;
            var REACT_SCOPE_TYPE = hasSymbol ? Symbol.for("react.scope") : 60119;
            var MAYBE_ITERATOR_SYMBOL = typeof Symbol === "function" && Symbol.iterator;
            var FAUX_ITERATOR_SYMBOL = "@@iterator";
            function getIteratorFn(maybeIterable) {
                if (maybeIterable === null || typeof maybeIterable !== "object") {
                    return null;
                }
                var maybeIterator = MAYBE_ITERATOR_SYMBOL && maybeIterable[MAYBE_ITERATOR_SYMBOL] || maybeIterable[FAUX_ITERATOR_SYMBOL];
                if (typeof maybeIterator === "function") {
                    return maybeIterator;
                }
                return null;
            }
            var ReactCurrentDispatcher = {
                current: null
            };
            var ReactCurrentBatchConfig = {
                suspense: null
            };
            var ReactCurrentOwner = {
                current: null
            };
            var BEFORE_SLASH_RE = /^(.*)[\\\/]/;
            function describeComponentFrame(name, source, ownerName) {
                var sourceInfo = "";
                if (source) {
                    var path = source.fileName;
                    var fileName = path.replace(BEFORE_SLASH_RE, "");
                    {
                        if (/^index\./.test(fileName)) {
                            var match = path.match(BEFORE_SLASH_RE);
                            if (match) {
                                var pathBeforeSlash = match[1];
                                if (pathBeforeSlash) {
                                    var folderName = pathBeforeSlash.replace(BEFORE_SLASH_RE, "");
                                    fileName = folderName + "/" + fileName;
                                }
                            }
                        }
                    }
                    sourceInfo = " (at " + fileName + ":" + source.lineNumber + ")";
                } else if (ownerName) {
                    sourceInfo = " (created by " + ownerName + ")";
                }
                return "\n    in " + (name || "Unknown") + sourceInfo;
            }
            var Resolved = 1;
            function refineResolvedLazyComponent(lazyComponent) {
                return lazyComponent._status === Resolved ? lazyComponent._result : null;
            }
            function getWrappedName(outerType, innerType, wrapperName) {
                var functionName = innerType.displayName || innerType.name || "";
                return outerType.displayName || (functionName !== "" ? wrapperName + "(" + functionName + ")" : wrapperName);
            }
            function getComponentName(type) {
                if (type == null) {
                    return null;
                }
                {
                    if (typeof type.tag === "number") {
                        error("Received an unexpected object in getComponentName(). This is likely a bug in React. Please file an issue.");
                    }
                }
                if (typeof type === "function") {
                    return type.displayName || type.name || null;
                }
                if (typeof type === "string") {
                    return type;
                }
                switch (type) {
                    case REACT_FRAGMENT_TYPE:
                        return "Fragment";
                    case REACT_PORTAL_TYPE:
                        return "Portal";
                    case REACT_PROFILER_TYPE:
                        return "Profiler";
                    case REACT_STRICT_MODE_TYPE:
                        return "StrictMode";
                    case REACT_SUSPENSE_TYPE:
                        return "Suspense";
                    case REACT_SUSPENSE_LIST_TYPE:
                        return "SuspenseList";
                }
                if (typeof type === "object") {
                    switch (type.$$typeof) {
                        case REACT_CONTEXT_TYPE:
                            return "Context.Consumer";
                        case REACT_PROVIDER_TYPE:
                            return "Context.Provider";
                        case REACT_FORWARD_REF_TYPE:
                            return getWrappedName(type, type.render, "ForwardRef");
                        case REACT_MEMO_TYPE:
                            return getComponentName(type.type);
                        case REACT_BLOCK_TYPE:
                            return getComponentName(type.render);
                        case REACT_LAZY_TYPE: {
                            var thenable = type;
                            var resolvedThenable = refineResolvedLazyComponent(thenable);
                            if (resolvedThenable) {
                                return getComponentName(resolvedThenable);
                            }
                            break;
                        }
                    }
                }
                return null;
            }
            var ReactDebugCurrentFrame = {};
            var currentlyValidatingElement = null;
            function setCurrentlyValidatingElement(element) {
                {
                    currentlyValidatingElement = element;
                }
            }
            {
                ReactDebugCurrentFrame.getCurrentStack = null;
                ReactDebugCurrentFrame.getStackAddendum = function () {
                    var stack = "";
                    if (currentlyValidatingElement) {
                        var name = getComponentName(currentlyValidatingElement.type);
                        var owner = currentlyValidatingElement._owner;
                        stack += describeComponentFrame(name, currentlyValidatingElement._source, owner && getComponentName(owner.type));
                    }
                    var impl = ReactDebugCurrentFrame.getCurrentStack;
                    if (impl) {
                        stack += impl() || "";
                    }
                    return stack;
                };
            }
            var IsSomeRendererActing = {
                current: false
            };
            var ReactSharedInternals = {
                ReactCurrentDispatcher,
                ReactCurrentBatchConfig,
                ReactCurrentOwner,
                IsSomeRendererActing,
                assign: _assign
            };
            {
                _assign(ReactSharedInternals, {
                    ReactDebugCurrentFrame,
                    ReactComponentTreeHook: {}
                });
            }
            function warn(format) {
                {
                    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                        args[_key - 1] = arguments[_key];
                    }
                    printWarning("warn", format, args);
                }
            }
            function error(format) {
                {
                    for (var _len2 = arguments.length, args = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
                        args[_key2 - 1] = arguments[_key2];
                    }
                    printWarning("error", format, args);
                }
            }
            function printWarning(level, format, args) {
                {
                    var hasExistingStack = args.length > 0 && typeof args[args.length - 1] === "string" && args[args.length - 1].indexOf("\n    in") === 0;
                    if (!hasExistingStack) {
                        var ReactDebugCurrentFrame2 = ReactSharedInternals.ReactDebugCurrentFrame;
                        var stack = ReactDebugCurrentFrame2.getStackAddendum();
                        if (stack !== "") {
                            format += "%s";
                            args = args.concat([stack]);
                        }
                    }
                    var argsWithFormat = args.map(function (item) {
                        return "" + item;
                    });
                    argsWithFormat.unshift("Warning: " + format);
                    Function.prototype.apply.call(console[level], console, argsWithFormat);
                    try {
                        var argIndex = 0;
                        var message = "Warning: " + format.replace(/%s/g, function () {
                            return args[argIndex++];
                        });
                        throw new Error(message);
                    } catch (x) {
                    }
                }
            }
            var didWarnStateUpdateForUnmountedComponent = {};
            function warnNoop(publicInstance, callerName) {
                {
                    var _constructor = publicInstance.constructor;
                    var componentName = _constructor && (_constructor.displayName || _constructor.name) || "ReactClass";
                    var warningKey = componentName + "." + callerName;
                    if (didWarnStateUpdateForUnmountedComponent[warningKey]) {
                        return;
                    }
                    error("Can't call %s on a component that is not yet mounted. This is a no-op, but it might indicate a bug in your application. Instead, assign to `this.state` directly or define a `state = {};` class property with the desired state in the %s component.", callerName, componentName);
                    didWarnStateUpdateForUnmountedComponent[warningKey] = true;
                }
            }
            var ReactNoopUpdateQueue = {
                isMounted: function (publicInstance) {
                    return false;
                },
                enqueueForceUpdate: function (publicInstance, callback, callerName) {
                    warnNoop(publicInstance, "forceUpdate");
                },
                enqueueReplaceState: function (publicInstance, completeState, callback, callerName) {
                    warnNoop(publicInstance, "replaceState");
                },
                enqueueSetState: function (publicInstance, partialState, callback, callerName) {
                    warnNoop(publicInstance, "setState");
                }
            };
            var emptyObject = {};
            {
                Object.freeze(emptyObject);
            }
            function Component(props, context, updater) {
                this.props = props;
                this.context = context;
                this.refs = emptyObject;
                this.updater = updater || ReactNoopUpdateQueue;
            }
            Component.prototype.isReactComponent = {};
            Component.prototype.setState = function (partialState, callback) {
                if (!(typeof partialState === "object" || typeof partialState === "function" || partialState == null)) {
                    {
                        throw Error("setState(...): takes an object of state variables to update or a function which returns an object of state variables.");
                    }
                }
                this.updater.enqueueSetState(this, partialState, callback, "setState");
            };
            Component.prototype.forceUpdate = function (callback) {
                this.updater.enqueueForceUpdate(this, callback, "forceUpdate");
            };
            {
                var deprecatedAPIs = {
                    isMounted: ["isMounted", "Instead, make sure to clean up subscriptions and pending requests in componentWillUnmount to prevent memory leaks."],
                    replaceState: ["replaceState", "Refactor your code to use setState instead (see https://github.com/facebook/react/issues/3236)."]
                };
                var defineDeprecationWarning = function (methodName, info) {
                    Object.defineProperty(Component.prototype, methodName, {
                        get: function () {
                            warn("%s(...) is deprecated in plain JavaScript React classes. %s", info[0], info[1]);
                            return void 0;
                        }
                    });
                };
                for (var fnName in deprecatedAPIs) {
                    if (deprecatedAPIs.hasOwnProperty(fnName)) {
                        defineDeprecationWarning(fnName, deprecatedAPIs[fnName]);
                    }
                }
            }
            function ComponentDummy() {
            }
            ComponentDummy.prototype = Component.prototype;
            function PureComponent(props, context, updater) {
                this.props = props;
                this.context = context;
                this.refs = emptyObject;
                this.updater = updater || ReactNoopUpdateQueue;
            }
            var pureComponentPrototype = PureComponent.prototype = new ComponentDummy();
            pureComponentPrototype.constructor = PureComponent;
            _assign(pureComponentPrototype, Component.prototype);
            pureComponentPrototype.isPureReactComponent = true;
            function createRef() {
                var refObject = {
                    current: null
                };
                {
                    Object.seal(refObject);
                }
                return refObject;
            }
            var hasOwnProperty = Object.prototype.hasOwnProperty;
            var RESERVED_PROPS = {
                key: true,
                ref: true,
                __self: true,
                __source: true
            };
            var specialPropKeyWarningShown, specialPropRefWarningShown, didWarnAboutStringRefs;
            {
                didWarnAboutStringRefs = {};
            }
            function hasValidRef(config) {
                {
                    if (hasOwnProperty.call(config, "ref")) {
                        var getter = Object.getOwnPropertyDescriptor(config, "ref").get;
                        if (getter && getter.isReactWarning) {
                            return false;
                        }
                    }
                }
                return config.ref !== void 0;
            }
            function hasValidKey(config) {
                {
                    if (hasOwnProperty.call(config, "key")) {
                        var getter = Object.getOwnPropertyDescriptor(config, "key").get;
                        if (getter && getter.isReactWarning) {
                            return false;
                        }
                    }
                }
                return config.key !== void 0;
            }
            function defineKeyPropWarningGetter(props, displayName) {
                var warnAboutAccessingKey = function () {
                    {
                        if (!specialPropKeyWarningShown) {
                            specialPropKeyWarningShown = true;
                            error("%s: `key` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://fb.me/react-special-props)", displayName);
                        }
                    }
                };
                warnAboutAccessingKey.isReactWarning = true;
                Object.defineProperty(props, "key", {
                    get: warnAboutAccessingKey,
                    configurable: true
                });
            }
            function defineRefPropWarningGetter(props, displayName) {
                var warnAboutAccessingRef = function () {
                    {
                        if (!specialPropRefWarningShown) {
                            specialPropRefWarningShown = true;
                            error("%s: `ref` is not a prop. Trying to access it will result in `undefined` being returned. If you need to access the same value within the child component, you should pass it as a different prop. (https://fb.me/react-special-props)", displayName);
                        }
                    }
                };
                warnAboutAccessingRef.isReactWarning = true;
                Object.defineProperty(props, "ref", {
                    get: warnAboutAccessingRef,
                    configurable: true
                });
            }
            function warnIfStringRefCannotBeAutoConverted(config) {
                {
                    if (typeof config.ref === "string" && ReactCurrentOwner.current && config.__self && ReactCurrentOwner.current.stateNode !== config.__self) {
                        var componentName = getComponentName(ReactCurrentOwner.current.type);
                        if (!didWarnAboutStringRefs[componentName]) {
                            error('Component "%s" contains the string ref "%s". Support for string refs will be removed in a future major release. This case cannot be automatically converted to an arrow function. We ask you to manually fix this case by using useRef() or createRef() instead. Learn more about using refs safely here: https://fb.me/react-strict-mode-string-ref', getComponentName(ReactCurrentOwner.current.type), config.ref);
                            didWarnAboutStringRefs[componentName] = true;
                        }
                    }
                }
            }
            var ReactElement = function (type, key, ref, self2, source, owner, props) {
                var element = {
                    $$typeof: REACT_ELEMENT_TYPE,
                    type,
                    key,
                    ref,
                    props,
                    _owner: owner
                };
                {
                    element._store = {};
                    Object.defineProperty(element._store, "validated", {
                        configurable: false,
                        enumerable: false,
                        writable: true,
                        value: false
                    });
                    Object.defineProperty(element, "_self", {
                        configurable: false,
                        enumerable: false,
                        writable: false,
                        value: self2
                    });
                    Object.defineProperty(element, "_source", {
                        configurable: false,
                        enumerable: false,
                        writable: false,
                        value: source
                    });
                    if (Object.freeze) {
                        Object.freeze(element.props);
                        Object.freeze(element);
                    }
                }
                return element;
            };
            function createElement(type, config, children) {
                var propName;
                var props = {};
                var key = null;
                var ref = null;
                var self2 = null;
                var source = null;
                if (config != null) {
                    if (hasValidRef(config)) {
                        ref = config.ref;
                        {
                            warnIfStringRefCannotBeAutoConverted(config);
                        }
                    }
                    if (hasValidKey(config)) {
                        key = "" + config.key;
                    }
                    self2 = config.__self === void 0 ? null : config.__self;
                    source = config.__source === void 0 ? null : config.__source;
                    for (propName in config) {
                        if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
                            props[propName] = config[propName];
                        }
                    }
                }
                var childrenLength = arguments.length - 2;
                if (childrenLength === 1) {
                    props.children = children;
                } else if (childrenLength > 1) {
                    var childArray = Array(childrenLength);
                    for (var i = 0; i < childrenLength; i++) {
                        childArray[i] = arguments[i + 2];
                    }
                    {
                        if (Object.freeze) {
                            Object.freeze(childArray);
                        }
                    }
                    props.children = childArray;
                }
                if (type && type.defaultProps) {
                    var defaultProps = type.defaultProps;
                    for (propName in defaultProps) {
                        if (props[propName] === void 0) {
                            props[propName] = defaultProps[propName];
                        }
                    }
                }
                {
                    if (key || ref) {
                        var displayName = typeof type === "function" ? type.displayName || type.name || "Unknown" : type;
                        if (key) {
                            defineKeyPropWarningGetter(props, displayName);
                        }
                        if (ref) {
                            defineRefPropWarningGetter(props, displayName);
                        }
                    }
                }
                return ReactElement(type, key, ref, self2, source, ReactCurrentOwner.current, props);
            }
            function cloneAndReplaceKey(oldElement, newKey) {
                var newElement = ReactElement(oldElement.type, newKey, oldElement.ref, oldElement._self, oldElement._source, oldElement._owner, oldElement.props);
                return newElement;
            }
            function cloneElement(element, config, children) {
                if (!!(element === null || element === void 0)) {
                    {
                        throw Error("React.cloneElement(...): The argument must be a React element, but you passed " + element + ".");
                    }
                }
                var propName;
                var props = _assign({}, element.props);
                var key = element.key;
                var ref = element.ref;
                var self2 = element._self;
                var source = element._source;
                var owner = element._owner;
                if (config != null) {
                    if (hasValidRef(config)) {
                        ref = config.ref;
                        owner = ReactCurrentOwner.current;
                    }
                    if (hasValidKey(config)) {
                        key = "" + config.key;
                    }
                    var defaultProps;
                    if (element.type && element.type.defaultProps) {
                        defaultProps = element.type.defaultProps;
                    }
                    for (propName in config) {
                        if (hasOwnProperty.call(config, propName) && !RESERVED_PROPS.hasOwnProperty(propName)) {
                            if (config[propName] === void 0 && defaultProps !== void 0) {
                                props[propName] = defaultProps[propName];
                            } else {
                                props[propName] = config[propName];
                            }
                        }
                    }
                }
                var childrenLength = arguments.length - 2;
                if (childrenLength === 1) {
                    props.children = children;
                } else if (childrenLength > 1) {
                    var childArray = Array(childrenLength);
                    for (var i = 0; i < childrenLength; i++) {
                        childArray[i] = arguments[i + 2];
                    }
                    props.children = childArray;
                }
                return ReactElement(element.type, key, ref, self2, source, owner, props);
            }
            function isValidElement2(object) {
                return typeof object === "object" && object !== null && object.$$typeof === REACT_ELEMENT_TYPE;
            }
            var SEPARATOR = ".";
            var SUBSEPARATOR = ":";
            function escape2(key) {
                var escapeRegex = /[=:]/g;
                var escaperLookup = {
                    "=": "=0",
                    ":": "=2"
                };
                var escapedString = ("" + key).replace(escapeRegex, function (match) {
                    return escaperLookup[match];
                });
                return "$" + escapedString;
            }
            var didWarnAboutMaps = false;
            var userProvidedKeyEscapeRegex = /\/+/g;
            function escapeUserProvidedKey(text) {
                return ("" + text).replace(userProvidedKeyEscapeRegex, "$&/");
            }
            var POOL_SIZE = 10;
            var traverseContextPool = [];
            function getPooledTraverseContext(mapResult, keyPrefix, mapFunction, mapContext) {
                if (traverseContextPool.length) {
                    var traverseContext = traverseContextPool.pop();
                    traverseContext.result = mapResult;
                    traverseContext.keyPrefix = keyPrefix;
                    traverseContext.func = mapFunction;
                    traverseContext.context = mapContext;
                    traverseContext.count = 0;
                    return traverseContext;
                } else {
                    return {
                        result: mapResult,
                        keyPrefix,
                        func: mapFunction,
                        context: mapContext,
                        count: 0
                    };
                }
            }
            function releaseTraverseContext(traverseContext) {
                traverseContext.result = null;
                traverseContext.keyPrefix = null;
                traverseContext.func = null;
                traverseContext.context = null;
                traverseContext.count = 0;
                if (traverseContextPool.length < POOL_SIZE) {
                    traverseContextPool.push(traverseContext);
                }
            }
            function traverseAllChildrenImpl(children, nameSoFar, callback, traverseContext) {
                var type = typeof children;
                if (type === "undefined" || type === "boolean") {
                    children = null;
                }
                var invokeCallback = false;
                if (children === null) {
                    invokeCallback = true;
                } else {
                    switch (type) {
                        case "string":
                        case "number":
                            invokeCallback = true;
                            break;
                        case "object":
                            switch (children.$$typeof) {
                                case REACT_ELEMENT_TYPE:
                                case REACT_PORTAL_TYPE:
                                    invokeCallback = true;
                            }
                    }
                }
                if (invokeCallback) {
                    callback(traverseContext, children, nameSoFar === "" ? SEPARATOR + getComponentKey(children, 0) : nameSoFar);
                    return 1;
                }
                var child;
                var nextName;
                var subtreeCount = 0;
                var nextNamePrefix = nameSoFar === "" ? SEPARATOR : nameSoFar + SUBSEPARATOR;
                if (Array.isArray(children)) {
                    for (var i = 0; i < children.length; i++) {
                        child = children[i];
                        nextName = nextNamePrefix + getComponentKey(child, i);
                        subtreeCount += traverseAllChildrenImpl(child, nextName, callback, traverseContext);
                    }
                } else {
                    var iteratorFn = getIteratorFn(children);
                    if (typeof iteratorFn === "function") {
                        {
                            if (iteratorFn === children.entries) {
                                if (!didWarnAboutMaps) {
                                    warn("Using Maps as children is deprecated and will be removed in a future major release. Consider converting children to an array of keyed ReactElements instead.");
                                }
                                didWarnAboutMaps = true;
                            }
                        }
                        var iterator = iteratorFn.call(children);
                        var step;
                        var ii = 0;
                        while (!(step = iterator.next()).done) {
                            child = step.value;
                            nextName = nextNamePrefix + getComponentKey(child, ii++);
                            subtreeCount += traverseAllChildrenImpl(child, nextName, callback, traverseContext);
                        }
                    } else if (type === "object") {
                        var addendum = "";
                        {
                            addendum = " If you meant to render a collection of children, use an array instead." + ReactDebugCurrentFrame.getStackAddendum();
                        }
                        var childrenString = "" + children;
                        {
                            {
                                throw Error("Objects are not valid as a React child (found: " + (childrenString === "[object Object]" ? "object with keys {" + Object.keys(children).join(", ") + "}" : childrenString) + ")." + addendum);
                            }
                        }
                    }
                }
                return subtreeCount;
            }
            function traverseAllChildren(children, callback, traverseContext) {
                if (children == null) {
                    return 0;
                }
                return traverseAllChildrenImpl(children, "", callback, traverseContext);
            }
            function getComponentKey(component, index) {
                if (typeof component === "object" && component !== null && component.key != null) {
                    return escape2(component.key);
                }
                return index.toString(36);
            }
            function forEachSingleChild(bookKeeping, child, name) {
                var func = bookKeeping.func, context = bookKeeping.context;
                func.call(context, child, bookKeeping.count++);
            }
            function forEachChildren(children, forEachFunc, forEachContext) {
                if (children == null) {
                    return children;
                }
                var traverseContext = getPooledTraverseContext(null, null, forEachFunc, forEachContext);
                traverseAllChildren(children, forEachSingleChild, traverseContext);
                releaseTraverseContext(traverseContext);
            }
            function mapSingleChildIntoContext(bookKeeping, child, childKey) {
                var result = bookKeeping.result, keyPrefix = bookKeeping.keyPrefix, func = bookKeeping.func, context = bookKeeping.context;
                var mappedChild = func.call(context, child, bookKeeping.count++);
                if (Array.isArray(mappedChild)) {
                    mapIntoWithKeyPrefixInternal(mappedChild, result, childKey, function (c) {
                        return c;
                    });
                } else if (mappedChild != null) {
                    if (isValidElement2(mappedChild)) {
                        mappedChild = cloneAndReplaceKey(mappedChild, keyPrefix + (mappedChild.key && (!child || child.key !== mappedChild.key) ? escapeUserProvidedKey(mappedChild.key) + "/" : "") + childKey);
                    }
                    result.push(mappedChild);
                }
            }
            function mapIntoWithKeyPrefixInternal(children, array, prefix, func, context) {
                var escapedPrefix = "";
                if (prefix != null) {
                    escapedPrefix = escapeUserProvidedKey(prefix) + "/";
                }
                var traverseContext = getPooledTraverseContext(array, escapedPrefix, func, context);
                traverseAllChildren(children, mapSingleChildIntoContext, traverseContext);
                releaseTraverseContext(traverseContext);
            }
            function mapChildren(children, func, context) {
                if (children == null) {
                    return children;
                }
                var result = [];
                mapIntoWithKeyPrefixInternal(children, result, null, func, context);
                return result;
            }
            function countChildren(children) {
                return traverseAllChildren(children, function () {
                    return null;
                }, null);
            }
            function toArray(children) {
                var result = [];
                mapIntoWithKeyPrefixInternal(children, result, null, function (child) {
                    return child;
                });
                return result;
            }
            function onlyChild(children) {
                if (!isValidElement2(children)) {
                    {
                        throw Error("React.Children.only expected to receive a single React element child.");
                    }
                }
                return children;
            }
            function createContext(defaultValue, calculateChangedBits) {
                if (calculateChangedBits === void 0) {
                    calculateChangedBits = null;
                } else {
                    {
                        if (calculateChangedBits !== null && typeof calculateChangedBits !== "function") {
                            error("createContext: Expected the optional second argument to be a function. Instead received: %s", calculateChangedBits);
                        }
                    }
                }
                var context = {
                    $$typeof: REACT_CONTEXT_TYPE,
                    _calculateChangedBits: calculateChangedBits,
                    _currentValue: defaultValue,
                    _currentValue2: defaultValue,
                    _threadCount: 0,
                    Provider: null,
                    Consumer: null
                };
                context.Provider = {
                    $$typeof: REACT_PROVIDER_TYPE,
                    _context: context
                };
                var hasWarnedAboutUsingNestedContextConsumers = false;
                var hasWarnedAboutUsingConsumerProvider = false;
                {
                    var Consumer = {
                        $$typeof: REACT_CONTEXT_TYPE,
                        _context: context,
                        _calculateChangedBits: context._calculateChangedBits
                    };
                    Object.defineProperties(Consumer, {
                        Provider: {
                            get: function () {
                                if (!hasWarnedAboutUsingConsumerProvider) {
                                    hasWarnedAboutUsingConsumerProvider = true;
                                    error("Rendering <Context.Consumer.Provider> is not supported and will be removed in a future major release. Did you mean to render <Context.Provider> instead?");
                                }
                                return context.Provider;
                            },
                            set: function (_Provider) {
                                context.Provider = _Provider;
                            }
                        },
                        _currentValue: {
                            get: function () {
                                return context._currentValue;
                            },
                            set: function (_currentValue) {
                                context._currentValue = _currentValue;
                            }
                        },
                        _currentValue2: {
                            get: function () {
                                return context._currentValue2;
                            },
                            set: function (_currentValue2) {
                                context._currentValue2 = _currentValue2;
                            }
                        },
                        _threadCount: {
                            get: function () {
                                return context._threadCount;
                            },
                            set: function (_threadCount) {
                                context._threadCount = _threadCount;
                            }
                        },
                        Consumer: {
                            get: function () {
                                if (!hasWarnedAboutUsingNestedContextConsumers) {
                                    hasWarnedAboutUsingNestedContextConsumers = true;
                                    error("Rendering <Context.Consumer.Consumer> is not supported and will be removed in a future major release. Did you mean to render <Context.Consumer> instead?");
                                }
                                return context.Consumer;
                            }
                        }
                    });
                    context.Consumer = Consumer;
                }
                {
                    context._currentRenderer = null;
                    context._currentRenderer2 = null;
                }
                return context;
            }
            function lazy(ctor) {
                var lazyType = {
                    $$typeof: REACT_LAZY_TYPE,
                    _ctor: ctor,
                    _status: -1,
                    _result: null
                };
                {
                    var defaultProps;
                    var propTypes;
                    Object.defineProperties(lazyType, {
                        defaultProps: {
                            configurable: true,
                            get: function () {
                                return defaultProps;
                            },
                            set: function (newDefaultProps) {
                                error("React.lazy(...): It is not supported to assign `defaultProps` to a lazy component import. Either specify them where the component is defined, or create a wrapping component around it.");
                                defaultProps = newDefaultProps;
                                Object.defineProperty(lazyType, "defaultProps", {
                                    enumerable: true
                                });
                            }
                        },
                        propTypes: {
                            configurable: true,
                            get: function () {
                                return propTypes;
                            },
                            set: function (newPropTypes) {
                                error("React.lazy(...): It is not supported to assign `propTypes` to a lazy component import. Either specify them where the component is defined, or create a wrapping component around it.");
                                propTypes = newPropTypes;
                                Object.defineProperty(lazyType, "propTypes", {
                                    enumerable: true
                                });
                            }
                        }
                    });
                }
                return lazyType;
            }
            function forwardRef(render) {
                {
                    if (render != null && render.$$typeof === REACT_MEMO_TYPE) {
                        error("forwardRef requires a render function but received a `memo` component. Instead of forwardRef(memo(...)), use memo(forwardRef(...)).");
                    } else if (typeof render !== "function") {
                        error("forwardRef requires a render function but was given %s.", render === null ? "null" : typeof render);
                    } else {
                        if (render.length !== 0 && render.length !== 2) {
                            error("forwardRef render functions accept exactly two parameters: props and ref. %s", render.length === 1 ? "Did you forget to use the ref parameter?" : "Any additional parameter will be undefined.");
                        }
                    }
                    if (render != null) {
                        if (render.defaultProps != null || render.propTypes != null) {
                            error("forwardRef render functions do not support propTypes or defaultProps. Did you accidentally pass a React component?");
                        }
                    }
                }
                return {
                    $$typeof: REACT_FORWARD_REF_TYPE,
                    render
                };
            }
            function isValidElementType(type) {
                return typeof type === "string" || typeof type === "function" || type === REACT_FRAGMENT_TYPE || type === REACT_CONCURRENT_MODE_TYPE || type === REACT_PROFILER_TYPE || type === REACT_STRICT_MODE_TYPE || type === REACT_SUSPENSE_TYPE || type === REACT_SUSPENSE_LIST_TYPE || typeof type === "object" && type !== null && (type.$$typeof === REACT_LAZY_TYPE || type.$$typeof === REACT_MEMO_TYPE || type.$$typeof === REACT_PROVIDER_TYPE || type.$$typeof === REACT_CONTEXT_TYPE || type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_FUNDAMENTAL_TYPE || type.$$typeof === REACT_RESPONDER_TYPE || type.$$typeof === REACT_SCOPE_TYPE || type.$$typeof === REACT_BLOCK_TYPE);
            }
            function memo(type, compare) {
                {
                    if (!isValidElementType(type)) {
                        error("memo: The first argument must be a component. Instead received: %s", type === null ? "null" : typeof type);
                    }
                }
                return {
                    $$typeof: REACT_MEMO_TYPE,
                    type,
                    compare: compare === void 0 ? null : compare
                };
            }
            function resolveDispatcher() {
                var dispatcher = ReactCurrentDispatcher.current;
                if (!(dispatcher !== null)) {
                    {
                        throw Error("Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for one of the following reasons:\n1. You might have mismatching versions of React and the renderer (such as React DOM)\n2. You might be breaking the Rules of Hooks\n3. You might have more than one copy of React in the same app\nSee https://fb.me/react-invalid-hook-call for tips about how to debug and fix this problem.");
                    }
                }
                return dispatcher;
            }
            function useContext(Context, unstable_observedBits) {
                var dispatcher = resolveDispatcher();
                {
                    if (unstable_observedBits !== void 0) {
                        error("useContext() second argument is reserved for future use in React. Passing it is not supported. You passed: %s.%s", unstable_observedBits, typeof unstable_observedBits === "number" && Array.isArray(arguments[2]) ? "\n\nDid you call array.map(useContext)? Calling Hooks inside a loop is not supported. Learn more at https://fb.me/rules-of-hooks" : "");
                    }
                    if (Context._context !== void 0) {
                        var realContext = Context._context;
                        if (realContext.Consumer === Context) {
                            error("Calling useContext(Context.Consumer) is not supported, may cause bugs, and will be removed in a future major release. Did you mean to call useContext(Context) instead?");
                        } else if (realContext.Provider === Context) {
                            error("Calling useContext(Context.Provider) is not supported. Did you mean to call useContext(Context) instead?");
                        }
                    }
                }
                return dispatcher.useContext(Context, unstable_observedBits);
            }
            function useState(initialState) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useState(initialState);
            }
            function useReducer(reducer, initialArg, init) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useReducer(reducer, initialArg, init);
            }
            function useRef(initialValue) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useRef(initialValue);
            }
            function useEffect(create, deps) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useEffect(create, deps);
            }
            function useLayoutEffect(create, deps) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useLayoutEffect(create, deps);
            }
            function useCallback(callback, deps) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useCallback(callback, deps);
            }
            function useMemo(create, deps) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useMemo(create, deps);
            }
            function useImperativeHandle(ref, create, deps) {
                var dispatcher = resolveDispatcher();
                return dispatcher.useImperativeHandle(ref, create, deps);
            }
            function useDebugValue(value, formatterFn) {
                {
                    var dispatcher = resolveDispatcher();
                    return dispatcher.useDebugValue(value, formatterFn);
                }
            }
            var propTypesMisspellWarningShown;
            {
                propTypesMisspellWarningShown = false;
            }
            function getDeclarationErrorAddendum() {
                if (ReactCurrentOwner.current) {
                    var name = getComponentName(ReactCurrentOwner.current.type);
                    if (name) {
                        return "\n\nCheck the render method of `" + name + "`.";
                    }
                }
                return "";
            }
            function getSourceInfoErrorAddendum(source) {
                if (source !== void 0) {
                    var fileName = source.fileName.replace(/^.*[\\\/]/, "");
                    var lineNumber = source.lineNumber;
                    return "\n\nCheck your code at " + fileName + ":" + lineNumber + ".";
                }
                return "";
            }
            function getSourceInfoErrorAddendumForProps(elementProps) {
                if (elementProps !== null && elementProps !== void 0) {
                    return getSourceInfoErrorAddendum(elementProps.__source);
                }
                return "";
            }
            var ownerHasKeyUseWarning = {};
            function getCurrentComponentErrorInfo(parentType) {
                var info = getDeclarationErrorAddendum();
                if (!info) {
                    var parentName = typeof parentType === "string" ? parentType : parentType.displayName || parentType.name;
                    if (parentName) {
                        info = "\n\nCheck the top-level render call using <" + parentName + ">.";
                    }
                }
                return info;
            }
            function validateExplicitKey(element, parentType) {
                if (!element._store || element._store.validated || element.key != null) {
                    return;
                }
                element._store.validated = true;
                var currentComponentErrorInfo = getCurrentComponentErrorInfo(parentType);
                if (ownerHasKeyUseWarning[currentComponentErrorInfo]) {
                    return;
                }
                ownerHasKeyUseWarning[currentComponentErrorInfo] = true;
                var childOwner = "";
                if (element && element._owner && element._owner !== ReactCurrentOwner.current) {
                    childOwner = " It was passed a child from " + getComponentName(element._owner.type) + ".";
                }
                setCurrentlyValidatingElement(element);
                {
                    error('Each child in a list should have a unique "key" prop.%s%s See https://fb.me/react-warning-keys for more information.', currentComponentErrorInfo, childOwner);
                }
                setCurrentlyValidatingElement(null);
            }
            function validateChildKeys(node, parentType) {
                if (typeof node !== "object") {
                    return;
                }
                if (Array.isArray(node)) {
                    for (var i = 0; i < node.length; i++) {
                        var child = node[i];
                        if (isValidElement2(child)) {
                            validateExplicitKey(child, parentType);
                        }
                    }
                } else if (isValidElement2(node)) {
                    if (node._store) {
                        node._store.validated = true;
                    }
                } else if (node) {
                    var iteratorFn = getIteratorFn(node);
                    if (typeof iteratorFn === "function") {
                        if (iteratorFn !== node.entries) {
                            var iterator = iteratorFn.call(node);
                            var step;
                            while (!(step = iterator.next()).done) {
                                if (isValidElement2(step.value)) {
                                    validateExplicitKey(step.value, parentType);
                                }
                            }
                        }
                    }
                }
            }
            function validatePropTypes(element) {
                {
                    var type = element.type;
                    if (type === null || type === void 0 || typeof type === "string") {
                        return;
                    }
                    var name = getComponentName(type);
                    var propTypes;
                    if (typeof type === "function") {
                        propTypes = type.propTypes;
                    } else if (typeof type === "object" && (type.$$typeof === REACT_FORWARD_REF_TYPE || type.$$typeof === REACT_MEMO_TYPE)) {
                        propTypes = type.propTypes;
                    } else {
                        return;
                    }
                    if (propTypes) {
                        setCurrentlyValidatingElement(element);
                        checkPropTypes(propTypes, element.props, "prop", name, ReactDebugCurrentFrame.getStackAddendum);
                        setCurrentlyValidatingElement(null);
                    } else if (type.PropTypes !== void 0 && !propTypesMisspellWarningShown) {
                        propTypesMisspellWarningShown = true;
                        error("Component %s declared `PropTypes` instead of `propTypes`. Did you misspell the property assignment?", name || "Unknown");
                    }
                    if (typeof type.getDefaultProps === "function" && !type.getDefaultProps.isReactClassApproved) {
                        error("getDefaultProps is only used on classic React.createClass definitions. Use a static property named `defaultProps` instead.");
                    }
                }
            }
            function validateFragmentProps(fragment) {
                {
                    setCurrentlyValidatingElement(fragment);
                    var keys = Object.keys(fragment.props);
                    for (var i = 0; i < keys.length; i++) {
                        var key = keys[i];
                        if (key !== "children" && key !== "key") {
                            error("Invalid prop `%s` supplied to `React.Fragment`. React.Fragment can only have `key` and `children` props.", key);
                            break;
                        }
                    }
                    if (fragment.ref !== null) {
                        error("Invalid attribute `ref` supplied to `React.Fragment`.");
                    }
                    setCurrentlyValidatingElement(null);
                }
            }
            function createElementWithValidation(type, props, children) {
                var validType = isValidElementType(type);
                if (!validType) {
                    var info = "";
                    if (type === void 0 || typeof type === "object" && type !== null && Object.keys(type).length === 0) {
                        info += " You likely forgot to export your component from the file it's defined in, or you might have mixed up default and named imports.";
                    }
                    var sourceInfo = getSourceInfoErrorAddendumForProps(props);
                    if (sourceInfo) {
                        info += sourceInfo;
                    } else {
                        info += getDeclarationErrorAddendum();
                    }
                    var typeString;
                    if (type === null) {
                        typeString = "null";
                    } else if (Array.isArray(type)) {
                        typeString = "array";
                    } else if (type !== void 0 && type.$$typeof === REACT_ELEMENT_TYPE) {
                        typeString = "<" + (getComponentName(type.type) || "Unknown") + " />";
                        info = " Did you accidentally export a JSX literal instead of a component?";
                    } else {
                        typeString = typeof type;
                    }
                    {
                        error("React.createElement: type is invalid -- expected a string (for built-in components) or a class/function (for composite components) but got: %s.%s", typeString, info);
                    }
                }
                var element = createElement.apply(this, arguments);
                if (element == null) {
                    return element;
                }
                if (validType) {
                    for (var i = 2; i < arguments.length; i++) {
                        validateChildKeys(arguments[i], type);
                    }
                }
                if (type === REACT_FRAGMENT_TYPE) {
                    validateFragmentProps(element);
                } else {
                    validatePropTypes(element);
                }
                return element;
            }
            var didWarnAboutDeprecatedCreateFactory = false;
            function createFactoryWithValidation(type) {
                var validatedFactory = createElementWithValidation.bind(null, type);
                validatedFactory.type = type;
                {
                    if (!didWarnAboutDeprecatedCreateFactory) {
                        didWarnAboutDeprecatedCreateFactory = true;
                        warn("React.createFactory() is deprecated and will be removed in a future major release. Consider using JSX or use React.createElement() directly instead.");
                    }
                    Object.defineProperty(validatedFactory, "type", {
                        enumerable: false,
                        get: function () {
                            warn("Factory.type is deprecated. Access the class directly before passing it to createFactory.");
                            Object.defineProperty(this, "type", {
                                value: type
                            });
                            return type;
                        }
                    });
                }
                return validatedFactory;
            }
            function cloneElementWithValidation(element, props, children) {
                var newElement = cloneElement.apply(this, arguments);
                for (var i = 2; i < arguments.length; i++) {
                    validateChildKeys(arguments[i], newElement.type);
                }
                validatePropTypes(newElement);
                return newElement;
            }
            {
                try {
                    var frozenObject = Object.freeze({});
                    var testMap = new Map([[frozenObject, null]]);
                    var testSet = new Set([frozenObject]);
                    testMap.set(0, 0);
                    testSet.add(0);
                } catch (e) {
                }
            }
            var createElement$1 = createElementWithValidation;
            var cloneElement$1 = cloneElementWithValidation;
            var createFactory = createFactoryWithValidation;
            var Children = {
                map: mapChildren,
                forEach: forEachChildren,
                count: countChildren,
                toArray,
                only: onlyChild
            };
            exports.Children = Children;
            exports.Component = Component;
            exports.Fragment = REACT_FRAGMENT_TYPE;
            exports.Profiler = REACT_PROFILER_TYPE;
            exports.PureComponent = PureComponent;
            exports.StrictMode = REACT_STRICT_MODE_TYPE;
            exports.Suspense = REACT_SUSPENSE_TYPE;
            exports.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED = ReactSharedInternals;
            exports.cloneElement = cloneElement$1;
            exports.createContext = createContext;
            exports.createElement = createElement$1;
            exports.createFactory = createFactory;
            exports.createRef = createRef;
            exports.forwardRef = forwardRef;
            exports.isValidElement = isValidElement2;
            exports.lazy = lazy;
            exports.memo = memo;
            exports.useCallback = useCallback;
            exports.useContext = useContext;
            exports.useDebugValue = useDebugValue;
            exports.useEffect = useEffect;
            exports.useImperativeHandle = useImperativeHandle;
            exports.useLayoutEffect = useLayoutEffect;
            exports.useMemo = useMemo;
            exports.useReducer = useReducer;
            exports.useRef = useRef;
            exports.useState = useState;
            exports.version = ReactVersion;
        })();
    }
});

// node_modules/react/index.js
var require_react = __commonJS((exports, module) => {
    "use strict";
    if (isSSR) {
        module.exports = require_react_production_min();
    } else {
        module.exports = require_react_development();
    }
});

// dist/esm/index.js
const react = __toModule(require_react());
var spacer = function (times, tabStop) {
    if (times === 0) {
        return "";
    }
    return new Array(times * tabStop).fill(" ").join("");
};
function _typeof(obj) {
    "@babel/helpers - typeof";
    if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
        _typeof = function (obj2) {
            return typeof obj2;
        };
    } else {
        _typeof = function (obj2) {
            return obj2 && typeof Symbol === "function" && obj2.constructor === Symbol && obj2 !== Symbol.prototype ? "symbol" : typeof obj2;
        };
    }
    return _typeof(obj);
}
function _toConsumableArray(arr) {
    return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}
function _arrayWithoutHoles(arr) {
    if (Array.isArray(arr))
        return _arrayLikeToArray(arr);
}
function _iterableToArray(iter) {
    if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter))
        return Array.from(iter);
}
function _unsupportedIterableToArray(o, minLen) {
    if (!o)
        return;
    if (typeof o === "string")
        return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    if (n === "Object" && o.constructor)
        n = o.constructor.name;
    if (n === "Map" || n === "Set")
        return Array.from(o);
    if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n))
        return _arrayLikeToArray(o, minLen);
}
function _arrayLikeToArray(arr, len) {
    if (len == null || len > arr.length)
        len = arr.length;
    for (var i = 0, arr2 = new Array(len); i < len; i++)
        arr2[i] = arr[i];
    return arr2;
}
function _nonIterableSpread() {
    throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}
/*!
 * isobject <https://github.com/jonschlinkert/isobject>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */
function isObject(val) {
    return val != null && typeof val === "object" && Array.isArray(val) === false;
}
/*!
 * is-plain-object <https://github.com/jonschlinkert/is-plain-object>
 *
 * Copyright (c) 2014-2017, Jon Schlinkert.
 * Released under the MIT License.
 */
function isObjectObject(o) {
    return isObject(o) === true && Object.prototype.toString.call(o) === "[object Object]";
}
function isPlainObject(o) {
    var ctor, prot;
    if (isObjectObject(o) === false)
        return false;
    ctor = o.constructor;
    if (typeof ctor !== "function")
        return false;
    prot = ctor.prototype;
    if (isObjectObject(prot) === false)
        return false;
    if (prot.hasOwnProperty("isPrototypeOf") === false) {
        return false;
    }
    return true;
}
var commonjsGlobal = typeof globalThis !== "undefined" ? globalThis : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : {};
function unwrapExports(x) {
    return x && x.__esModule && Object.prototype.hasOwnProperty.call(x, "default") ? x["default"] : x;
}
function createCommonjsModule(fn, module) {
    return module = { exports: {} }, fn(module, module.exports), module.exports;
}
var dist = createCommonjsModule(function (module, exports) {
    var __assign2 = commonjsGlobal && commonjsGlobal.__assign || function () {
        __assign2 = Object.assign || function (t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s)
                    if (Object.prototype.hasOwnProperty.call(s, p))
                        t[p] = s[p];
            }
            return t;
        };
        return __assign2.apply(this, arguments);
    };
    var __spreadArrays = commonjsGlobal && commonjsGlobal.__spreadArrays || function () {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++)
            s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };
    Object.defineProperty(exports, "__esModule", { value: true });
    var seen = [];
    function isObj(value) {
        var type = typeof value;
        return value !== null && (type === "object" || type === "function");
    }
    function isRegexp(value) {
        return Object.prototype.toString.call(value) === "[object RegExp]";
    }
    function getOwnEnumPropSymbols(object) {
        return Object.getOwnPropertySymbols(object).filter(function (keySymbol) {
            return Object.prototype.propertyIsEnumerable.call(object, keySymbol);
        });
    }
    function prettyPrint(input, options, pad) {
        if (pad === void 0) {
            pad = "";
        }
        var defaultOptions = {
            indent: "	",
            singleQuotes: true
        };
        var combinedOptions = __assign2(__assign2({}, defaultOptions), options);
        var tokens;
        if (combinedOptions.inlineCharacterLimit === void 0) {
            tokens = {
                newLine: "\n",
                newLineOrSpace: "\n",
                pad,
                indent: pad + combinedOptions.indent
            };
        } else {
            tokens = {
                newLine: "@@__PRETTY_PRINT_NEW_LINE__@@",
                newLineOrSpace: "@@__PRETTY_PRINT_NEW_LINE_OR_SPACE__@@",
                pad: "@@__PRETTY_PRINT_PAD__@@",
                indent: "@@__PRETTY_PRINT_INDENT__@@"
            };
        }
        var expandWhiteSpace = function (string) {
            if (combinedOptions.inlineCharacterLimit === void 0) {
                return string;
            }
            var oneLined = string.replace(new RegExp(tokens.newLine, "g"), "").replace(new RegExp(tokens.newLineOrSpace, "g"), " ").replace(new RegExp(tokens.pad + "|" + tokens.indent, "g"), "");
            if (oneLined.length <= combinedOptions.inlineCharacterLimit) {
                return oneLined;
            }
            return string.replace(new RegExp(tokens.newLine + "|" + tokens.newLineOrSpace, "g"), "\n").replace(new RegExp(tokens.pad, "g"), pad).replace(new RegExp(tokens.indent, "g"), pad + combinedOptions.indent);
        };
        if (seen.indexOf(input) !== -1) {
            return '"[Circular]"';
        }
        if (input === null || input === void 0 || typeof input === "number" || typeof input === "boolean" || typeof input === "function" || typeof input === "symbol" || isRegexp(input)) {
            return String(input);
        }
        if (input instanceof Date) {
            return "new Date('" + input.toISOString() + "')";
        }
        if (Array.isArray(input)) {
            if (input.length === 0) {
                return "[]";
            }
            seen.push(input);
            var ret = "[" + tokens.newLine + input.map(function (el, i) {
                var eol = input.length - 1 === i ? tokens.newLine : "," + tokens.newLineOrSpace;
                var value = prettyPrint(el, combinedOptions, pad + combinedOptions.indent);
                if (combinedOptions.transform) {
                    value = combinedOptions.transform(input, i, value);
                }
                return tokens.indent + value + eol;
            }).join("") + tokens.pad + "]";
            seen.pop();
            return expandWhiteSpace(ret);
        }
        if (isObj(input)) {
            var objKeys_1 = __spreadArrays(Object.keys(input), getOwnEnumPropSymbols(input));
            if (combinedOptions.filter) {
                objKeys_1 = objKeys_1.filter(function (el) {
                    return combinedOptions.filter && combinedOptions.filter(input, el);
                });
            }
            if (objKeys_1.length === 0) {
                return "{}";
            }
            seen.push(input);
            var ret = "{" + tokens.newLine + objKeys_1.map(function (el, i) {
                var eol = objKeys_1.length - 1 === i ? tokens.newLine : "," + tokens.newLineOrSpace;
                var isSymbol = typeof el === "symbol";
                var isClassic = !isSymbol && /^[a-z$_][a-z$_0-9]*$/i.test(el.toString());
                var key = isSymbol || isClassic ? el : prettyPrint(el, combinedOptions);
                var value = prettyPrint(input[el], combinedOptions, pad + combinedOptions.indent);
                if (combinedOptions.transform) {
                    value = combinedOptions.transform(input, el, value);
                }
                return tokens.indent + String(key) + ": " + value + eol;
            }).join("") + tokens.pad + "}";
            seen.pop();
            return expandWhiteSpace(ret);
        }
        input = String(input).replace(/[\r\n]/g, function (x) {
            return x === "\n" ? "\\n" : "\\r";
        });
        if (!combinedOptions.singleQuotes) {
            input = input.replace(/"/g, '\\"');
            return '"' + input + '"';
        }
        input = input.replace(/\\?'/g, "\\'");
        return "'" + input + "'";
    }
    exports.prettyPrint = prettyPrint;
});
unwrapExports(dist);
var dist_1 = dist.prettyPrint;
function sortObject(value) {
    if (value === null || _typeof(value) !== "object") {
        return value;
    }
    if (value instanceof Date || value instanceof RegExp) {
        return value;
    }
    if (Array.isArray(value)) {
        return value.map(sortObject);
    }
    return Object.keys(value).sort().reduce(function (result, key) {
        if (key === "_owner") {
            return result;
        }
        if (key === "current") {
            result[key] = "[Circular]";
        } else {
            result[key] = sortObject(value[key]);
        }
        return result;
    }, {});
}
var createStringTreeNode = function createStringTreeNode2(value) {
    return {
        type: "string",
        value
    };
};
var createNumberTreeNode = function createNumberTreeNode2(value) {
    return {
        type: "number",
        value
    };
};
var createReactElementTreeNode = function createReactElementTreeNode2(displayName, props, defaultProps, childrens) {
    return {
        type: "ReactElement",
        displayName,
        props,
        defaultProps,
        childrens
    };
};
var createReactFragmentTreeNode = function createReactFragmentTreeNode2(key, childrens) {
    return {
        type: "ReactFragment",
        key,
        childrens
    };
};
var supportFragment = Boolean(react.Fragment);
var getReactElementDisplayName = function getReactElementDisplayName2(element) {
    return element.type.displayName || (element.type.name !== "_default" ? element.type.name : null) || (typeof element.type === "function" ? "No Display Name" : element.type);
};
var noChildren = function noChildren2(propsValue, propName) {
    return propName !== "children";
};
var onlyMeaningfulChildren = function onlyMeaningfulChildren2(children) {
    return children !== true && children !== false && children !== null && children !== "";
};
var filterProps = function filterProps2(originalProps, cb) {
    var filteredProps = {};
    Object.keys(originalProps).filter(function (key) {
        return cb(originalProps[key], key);
    }).forEach(function (key) {
        return filteredProps[key] = originalProps[key];
    });
    return filteredProps;
};
var parseReactElement = function parseReactElement2(element, options) {
    var _options$displayName = options.displayName, displayNameFn = _options$displayName === void 0 ? getReactElementDisplayName : _options$displayName;
    if (typeof element === "string") {
        return createStringTreeNode(element);
    } else if (typeof element === "number") {
        return createNumberTreeNode(element);
    } else if (!/* @__PURE__ */ react.default.isValidElement(element)) {
        throw new Error("react-element-to-jsx-string: Expected a React.Element, got `".concat(_typeof(element), "`"));
    }
    var displayName = displayNameFn(element);
    var props = filterProps(element.props, noChildren);
    if (element.ref !== null) {
        props.ref = element.ref;
    }
    var key = element.key;
    if (typeof key === "string" && key.search(/^\./)) {
        props.key = key;
    }
    var defaultProps = filterProps(element.type.defaultProps || {}, noChildren);
    var childrens = react.default.Children.toArray(element.props.children).filter(onlyMeaningfulChildren).map(function (child) {
        return parseReactElement2(child, options);
    });
    if (supportFragment && element.type === react.Fragment) {
        return createReactFragmentTreeNode(key, childrens);
    }
    return createReactElementTreeNode(displayName, props, defaultProps, childrens);
};
function noRefCheck() {
}
var inlineFunction = function inlineFunction2(fn) {
    return fn.toString().split("\n").map(function (line) {
        return line.trim();
    }).join("");
};
var preserveFunctionLineBreak = function preserveFunctionLineBreak2(fn) {
    return fn.toString();
};
var defaultFunctionValue = inlineFunction;
var formatFunction = function (fn, options) {
    var _options$functionValu = options.functionValue, functionValue = _options$functionValu === void 0 ? defaultFunctionValue : _options$functionValu, showFunctions = options.showFunctions;
    if (!showFunctions && functionValue === defaultFunctionValue) {
        return functionValue(noRefCheck);
    }
    return functionValue(fn);
};
var formatComplexDataStructure = function (value, inline, lvl, options) {
    var normalizedValue = sortObject(value);
    var stringifiedValue = dist_1(normalizedValue, {
        transform: function transform(currentObj, prop, originalResult) {
            var currentValue = currentObj[prop];
            if (currentValue && /* @__PURE__ */ react.isValidElement(currentValue)) {
                return formatTreeNode(parseReactElement(currentValue, options), true, lvl, options);
            }
            if (typeof currentValue === "function") {
                return formatFunction(currentValue, options);
            }
            return originalResult;
        }
    });
    if (inline) {
        return stringifiedValue.replace(/\s+/g, " ").replace(/{ /g, "{").replace(/ }/g, "}").replace(/\[ /g, "[").replace(/ ]/g, "]");
    }
    return stringifiedValue.replace(/\t/g, spacer(1, options.tabStop)).replace(/\n([^$])/g, "\n".concat(spacer(lvl + 1, options.tabStop), "$1"));
};
var escape = function escape2(s) {
    return s.replace(/"/g, "&quot;");
};
var formatPropValue = function formatPropValue2(propValue, inline, lvl, options) {
    if (typeof propValue === "number") {
        return "{".concat(String(propValue), "}");
    }
    if (typeof propValue === "string") {
        return '"'.concat(escape(propValue), '"');
    }
    if (_typeof(propValue) === "symbol") {
        var symbolDescription = propValue.valueOf().toString().replace(/Symbol\((.*)\)/, "$1");
        if (!symbolDescription) {
            return "{Symbol()}";
        }
        return "{Symbol('".concat(symbolDescription, "')}");
    }
    if (typeof propValue === "function") {
        return "{".concat(formatFunction(propValue, options), "}");
    }
    if (/* @__PURE__ */ react.isValidElement(propValue)) {
        return "{".concat(formatTreeNode(parseReactElement(propValue, options), true, lvl, options), "}");
    }
    if (propValue instanceof Date) {
        return '{new Date("'.concat(propValue.toISOString(), '")}');
    }
    if (isPlainObject(propValue) || Array.isArray(propValue)) {
        return "{".concat(formatComplexDataStructure(propValue, inline, lvl, options), "}");
    }
    return "{".concat(String(propValue), "}");
};
var formatProp = function (name, hasValue, value, hasDefaultValue, defaultValue, inline, lvl, options) {
    if (!hasValue && !hasDefaultValue) {
        throw new Error('The prop "'.concat(name, '" has no value and no default: could not be formatted'));
    }
    var usedValue = hasValue ? value : defaultValue;
    var useBooleanShorthandSyntax = options.useBooleanShorthandSyntax, tabStop = options.tabStop;
    var formattedPropValue = formatPropValue(usedValue, inline, lvl, options);
    var attributeFormattedInline = " ";
    var attributeFormattedMultiline = "\n".concat(spacer(lvl + 1, tabStop));
    var isMultilineAttribute = formattedPropValue.includes("\n");
    if (useBooleanShorthandSyntax && formattedPropValue === "{false}" && !hasDefaultValue) {
        attributeFormattedInline = "";
        attributeFormattedMultiline = "";
    } else if (useBooleanShorthandSyntax && formattedPropValue === "{true}") {
        attributeFormattedInline += "".concat(name);
        attributeFormattedMultiline += "".concat(name);
    } else {
        attributeFormattedInline += "".concat(name, "=").concat(formattedPropValue);
        attributeFormattedMultiline += "".concat(name, "=").concat(formattedPropValue);
    }
    return {
        attributeFormattedInline,
        attributeFormattedMultiline,
        isMultilineAttribute
    };
};
var mergeSiblingPlainStringChildrenReducer = function (previousNodes, currentNode) {
    var nodes = previousNodes.slice(0, previousNodes.length > 0 ? previousNodes.length - 1 : 0);
    var previousNode = previousNodes[previousNodes.length - 1];
    if (previousNode && (currentNode.type === "string" || currentNode.type === "number") && (previousNode.type === "string" || previousNode.type === "number")) {
        nodes.push(createStringTreeNode(String(previousNode.value) + String(currentNode.value)));
    } else {
        if (previousNode) {
            nodes.push(previousNode);
        }
        nodes.push(currentNode);
    }
    return nodes;
};
var isKeyOrRefProps = function isKeyOrRefProps2(propName) {
    return ["key", "ref"].includes(propName);
};
var sortPropsByNames = function (shouldSortUserProps) {
    return function (props) {
        var haveKeyProp = props.includes("key");
        var haveRefProp = props.includes("ref");
        var userPropsOnly = props.filter(function (oneProp) {
            return !isKeyOrRefProps(oneProp);
        });
        var sortedProps = shouldSortUserProps ? _toConsumableArray(userPropsOnly.sort()) : _toConsumableArray(userPropsOnly);
        if (haveRefProp) {
            sortedProps.unshift("ref");
        }
        if (haveKeyProp) {
            sortedProps.unshift("key");
        }
        return sortedProps;
    };
};
function createPropFilter(props, filter) {
    if (Array.isArray(filter)) {
        return function (key) {
            return filter.indexOf(key) === -1;
        };
    } else {
        return function (key) {
            return filter(props[key], key);
        };
    }
}
var compensateMultilineStringElementIndentation = function compensateMultilineStringElementIndentation2(element, formattedElement, inline, lvl, options) {
    var tabStop = options.tabStop;
    if (element.type === "string") {
        return formattedElement.split("\n").map(function (line, offset) {
            if (offset === 0) {
                return line;
            }
            return "".concat(spacer(lvl, tabStop)).concat(line);
        }).join("\n");
    }
    return formattedElement;
};
var formatOneChildren = function formatOneChildren2(inline, lvl, options) {
    return function (element) {
        return compensateMultilineStringElementIndentation(element, formatTreeNode(element, inline, lvl, options), inline, lvl, options);
    };
};
var onlyPropsWithOriginalValue = function onlyPropsWithOriginalValue2(defaultProps, props) {
    return function (propName) {
        var haveDefaultValue = Object.keys(defaultProps).includes(propName);
        return !haveDefaultValue || haveDefaultValue && defaultProps[propName] !== props[propName];
    };
};
var isInlineAttributeTooLong = function isInlineAttributeTooLong2(attributes, inlineAttributeString, lvl, tabStop, maxInlineAttributesLineLength) {
    if (!maxInlineAttributesLineLength) {
        return attributes.length > 1;
    }
    return spacer(lvl, tabStop).length + inlineAttributeString.length > maxInlineAttributesLineLength;
};
var shouldRenderMultilineAttr = function shouldRenderMultilineAttr2(attributes, inlineAttributeString, containsMultilineAttr, inline, lvl, tabStop, maxInlineAttributesLineLength) {
    return (isInlineAttributeTooLong(attributes, inlineAttributeString, lvl, tabStop, maxInlineAttributesLineLength) || containsMultilineAttr) && !inline;
};
var formatReactElementNode = function (node, inline, lvl, options) {
    var type = node.type, _node$displayName = node.displayName, displayName = _node$displayName === void 0 ? "" : _node$displayName, childrens = node.childrens, _node$props = node.props, props = _node$props === void 0 ? {} : _node$props, _node$defaultProps = node.defaultProps, defaultProps = _node$defaultProps === void 0 ? {} : _node$defaultProps;
    if (type !== "ReactElement") {
        throw new Error('The "formatReactElementNode" function could only format node of type "ReactElement". Given:  '.concat(type));
    }
    var filterProps2 = options.filterProps, maxInlineAttributesLineLength = options.maxInlineAttributesLineLength, showDefaultProps = options.showDefaultProps, sortProps = options.sortProps, tabStop = options.tabStop;
    var out = "<".concat(displayName);
    var outInlineAttr = out;
    var outMultilineAttr = out;
    var containsMultilineAttr = false;
    var visibleAttributeNames = [];
    var propFilter = createPropFilter(props, filterProps2);
    Object.keys(props).filter(propFilter).filter(onlyPropsWithOriginalValue(defaultProps, props)).forEach(function (propName) {
        return visibleAttributeNames.push(propName);
    });
    Object.keys(defaultProps).filter(propFilter).filter(function () {
        return showDefaultProps;
    }).filter(function (defaultPropName) {
        return !visibleAttributeNames.includes(defaultPropName);
    }).forEach(function (defaultPropName) {
        return visibleAttributeNames.push(defaultPropName);
    });
    var attributes = sortPropsByNames(sortProps)(visibleAttributeNames);
    attributes.forEach(function (attributeName) {
        var _formatProp = formatProp(attributeName, Object.keys(props).includes(attributeName), props[attributeName], Object.keys(defaultProps).includes(attributeName), defaultProps[attributeName], inline, lvl, options), attributeFormattedInline = _formatProp.attributeFormattedInline, attributeFormattedMultiline = _formatProp.attributeFormattedMultiline, isMultilineAttribute = _formatProp.isMultilineAttribute;
        if (isMultilineAttribute) {
            containsMultilineAttr = true;
        }
        outInlineAttr += attributeFormattedInline;
        outMultilineAttr += attributeFormattedMultiline;
    });
    outMultilineAttr += "\n".concat(spacer(lvl, tabStop));
    if (shouldRenderMultilineAttr(attributes, outInlineAttr, containsMultilineAttr, inline, lvl, tabStop, maxInlineAttributesLineLength)) {
        out = outMultilineAttr;
    } else {
        out = outInlineAttr;
    }
    if (childrens && childrens.length > 0) {
        var newLvl = lvl + 1;
        out += ">";
        if (!inline) {
            out += "\n";
            out += spacer(newLvl, tabStop);
        }
        out += childrens.reduce(mergeSiblingPlainStringChildrenReducer, []).map(formatOneChildren(inline, newLvl, options)).join(!inline ? "\n".concat(spacer(newLvl, tabStop)) : "");
        if (!inline) {
            out += "\n";
            out += spacer(newLvl - 1, tabStop);
        }
        out += "</".concat(displayName, ">");
    } else {
        if (!isInlineAttributeTooLong(attributes, outInlineAttr, lvl, tabStop, maxInlineAttributesLineLength)) {
            out += " ";
        }
        out += "/>";
    }
    return out;
};
var REACT_FRAGMENT_TAG_NAME_SHORT_SYNTAX = "";
var REACT_FRAGMENT_TAG_NAME_EXPLICIT_SYNTAX = "React.Fragment";
var toReactElementTreeNode = function toReactElementTreeNode2(displayName, key, childrens) {
    var props = {};
    if (key) {
        props = {
            key
        };
    }
    return {
        type: "ReactElement",
        displayName,
        props,
        defaultProps: {},
        childrens
    };
};
var isKeyedFragment = function isKeyedFragment2(_ref) {
    var key = _ref.key;
    return Boolean(key);
};
var hasNoChildren = function hasNoChildren2(_ref2) {
    var childrens = _ref2.childrens;
    return childrens.length === 0;
};
var formatReactFragmentNode = function (node, inline, lvl, options) {
    var type = node.type, key = node.key, childrens = node.childrens;
    if (type !== "ReactFragment") {
        throw new Error('The "formatReactFragmentNode" function could only format node of type "ReactFragment". Given: '.concat(type));
    }
    var useFragmentShortSyntax = options.useFragmentShortSyntax;
    var displayName;
    if (useFragmentShortSyntax) {
        if (hasNoChildren(node) || isKeyedFragment(node)) {
            displayName = REACT_FRAGMENT_TAG_NAME_EXPLICIT_SYNTAX;
        } else {
            displayName = REACT_FRAGMENT_TAG_NAME_SHORT_SYNTAX;
        }
    } else {
        displayName = REACT_FRAGMENT_TAG_NAME_EXPLICIT_SYNTAX;
    }
    return formatReactElementNode(toReactElementTreeNode(displayName, key, childrens), inline, lvl, options);
};
var jsxStopChars = ["<", ">", "{", "}"];
var shouldBeEscaped = function shouldBeEscaped2(s) {
    return jsxStopChars.some(function (jsxStopChar) {
        return s.includes(jsxStopChar);
    });
};
var escape$1 = function escape2(s) {
    if (!shouldBeEscaped(s)) {
        return s;
    }
    return "{`".concat(s, "`}");
};
var preserveTrailingSpace = function preserveTrailingSpace2(s) {
    var result = s;
    if (result.endsWith(" ")) {
        result = result.replace(/^(.*?)(\s+)$/, "$1{'$2'}");
    }
    if (result.startsWith(" ")) {
        result = result.replace(/^(\s+)(.*)$/, "{'$1'}$2");
    }
    return result;
};
var formatTreeNode = function (node, inline, lvl, options) {
    if (node.type === "number") {
        return String(node.value);
    }
    if (node.type === "string") {
        return node.value ? "".concat(preserveTrailingSpace(escape$1(String(node.value)))) : "";
    }
    if (node.type === "ReactElement") {
        return formatReactElementNode(node, inline, lvl, options);
    }
    if (node.type === "ReactFragment") {
        return formatReactFragmentNode(node, inline, lvl, options);
    }
    throw new TypeError('Unknow format type "'.concat(node.type, '"'));
};
var formatTree = function (node, options) {
    return formatTreeNode(node, false, 0, options);
};
var reactElementToJsxString = function reactElementToJsxString2(element) {
    var _ref = arguments.length > 1 && arguments[1] !== void 0 ? arguments[1] : {}, _ref$filterProps = _ref.filterProps, filterProps2 = _ref$filterProps === void 0 ? [] : _ref$filterProps, _ref$showDefaultProps = _ref.showDefaultProps, showDefaultProps = _ref$showDefaultProps === void 0 ? true : _ref$showDefaultProps, _ref$showFunctions = _ref.showFunctions, showFunctions = _ref$showFunctions === void 0 ? false : _ref$showFunctions, functionValue = _ref.functionValue, _ref$tabStop = _ref.tabStop, tabStop = _ref$tabStop === void 0 ? 2 : _ref$tabStop, _ref$useBooleanShorth = _ref.useBooleanShorthandSyntax, useBooleanShorthandSyntax = _ref$useBooleanShorth === void 0 ? true : _ref$useBooleanShorth, _ref$useFragmentShort = _ref.useFragmentShortSyntax, useFragmentShortSyntax = _ref$useFragmentShort === void 0 ? true : _ref$useFragmentShort, _ref$sortProps = _ref.sortProps, sortProps = _ref$sortProps === void 0 ? true : _ref$sortProps, maxInlineAttributesLineLength = _ref.maxInlineAttributesLineLength, displayName = _ref.displayName;
    if (!element) {
        throw new Error("react-element-to-jsx-string: Expected a ReactElement");
    }
    var options = {
        filterProps: filterProps2,
        showDefaultProps,
        showFunctions,
        functionValue,
        tabStop,
        useBooleanShorthandSyntax,
        useFragmentShortSyntax,
        sortProps,
        maxInlineAttributesLineLength,
        displayName
    };
    return formatTree(parseReactElement(element, options), options);
};
var esm_default = reactElementToJsxString;
export {
    esm_default as default,
    inlineFunction,
    preserveFunctionLineBreak
};
