import { get } from 'lodash-es'

export const api = async (
  url: string,
  body?: any,
  opt?: { method?: 'GET' | 'POST' }
) => {
  const options: any = {
    method: get(opt, 'method') || 'POST',
    headers: {
      Accept: 'application/json',
      'Sec-Fetch-Dest': 'script',
      'Content-Type': 'application/json;charset=UTF-8',
    },
  }

  if (body) {
    options.body = JSON.stringify(body)
  }

  const res = await fetch(url, options)
  return await res.json()
}
