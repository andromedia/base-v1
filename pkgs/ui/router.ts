export * from './router/core/basename'
export * from './router/core/history'
export * from './router/core/hooks'
export * from './router/core/Link'
export * from './router/core/Route'
export * from './router/core/routeMatcher'
export * from './router/core/Router'
export * from './router/core/RouterContext'
export * from './router/core/simpleRouteMatcherFactory'
export * from './router/core/Switch'

export const isSSR = !!(window as any).isSSR
