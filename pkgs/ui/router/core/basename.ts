export const getPathWithoutBasename = (basename: string) => {
  if ((window as any).isSSR) {
    const pathname = (window as any).ssr.url
    return pathname.substring(basename.length)
  } 
  return document.location.pathname.substring(basename.length)
}
