import get from 'lodash-es/get'
import { observer } from 'mobx-react-lite'
import React, { useState } from 'react'
import { useRouter } from './core'
import type { IPages } from './router'

export const MainLayout = observer(
  ({
    pages,
    children,
    ssr,
    serverProps,
  }: React.PropsWithChildren<{
    pages: IPages
    ssr?: { url: string; props: any }
    serverProps: any
    children: (renderLayout: () => void) => any
  }>): any => {
    const { path } = useRouter()
    let routerPath = path
    let sprops = get(serverProps, 'layout.props') || {
      path: '',
      isLoading: serverProps.isLoading,
    }

    if (ssr) {
      routerPath = ssr.url
      sprops = get(ssr.props, 'layout.props', {})
    }

    const layout = getLayout(routerPath, pages)
    const [_, _render] = useState(1)
    const renderLayout = () => {
      _render(Math.random())
    }

    sprops.path = routerPath

    if (layout === null) {
      return children(renderLayout)
    }
    const { Layout } = layout
    const renderedChildren = children(renderLayout)

    return <Layout {...sprops} children={renderedChildren} />
  }
)

const getLayout = (
  path: string,
  pages: IPages
): {
  path: string
  Layout: React.FC<{ isLoading: boolean }>
  server: boolean
} | null => {
  const slashedPath = path.indexOf('/') === path.length - 1 ? path : path + '/'
  const p = slashedPath.split('/')

  while (p.length > 0) {
    p.pop()

    const pr = [...p, '_layout'].join('/')
    if (pages[pr]) {
      return { path: pr, Layout: pages[pr].c, server: pages[pr].s }
    }
  }

  return null
}
