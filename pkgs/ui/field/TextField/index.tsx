import { TextField } from '@fluentui/react'
import { observer } from 'mobx-react-lite'
import React from 'react'

interface IProps {
  style?: any
  label?: string
  onChange: (text: string | undefined) => void
  value: string
  type: string
}

export default observer(({ label, value, onChange, type }: IProps) => {

  return (
    <TextField
      label={label}
      value={value}
      type={type}
      autoComplete=""
      canRevealPassword={type !== 'string'}
      onChange={(e, text) => {
        onChange(text)
      }}
    />
  )
})
