import {
  DatePicker,
  DayOfWeek,
  IDatePickerStrings,
  mergeStyleSets,
} from '@fluentui/react'
import { startCase } from 'lodash-es'
import { runInAction } from 'mobx'
import { observer, useLocalStore } from 'mobx-react-lite'
import React from 'react'

interface IDateProps {
  style?: any
  label?: string
  value?: Date | string
  onSelectDate: (date: Date | null | undefined) => void
}

export default observer(({ label, onSelectDate, value }: IDateProps) => {
  const controlClass = mergeStyleSets({
    control: {
      margin: '0 0 15px 0',
      maxWidth: '300px',
    },
  })

  const DayPickerStrings: IDatePickerStrings = {
    months: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],

    shortMonths: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],

    days: [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ],

    shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],

    goToToday: 'Go to today',
    prevMonthAriaLabel: 'Go to previous month',
    nextMonthAriaLabel: 'Go to next month',
    prevYearAriaLabel: 'Go to previous year',
    nextYearAriaLabel: 'Go to next year',
    closeButtonAriaLabel: 'Close date picker',
    monthPickerHeaderAriaLabel: '{0}, select to change the year',
    yearPickerHeaderAriaLabel: '{0}, select to change the month',
  }

  const finalValue = typeof value === 'string' ? new Date(value) : value

  return (
    <>
      <DatePicker
        label={startCase(label)}
        className={controlClass.control}
        firstDayOfWeek={DayOfWeek.Sunday}
        strings={DayPickerStrings}
        value={finalValue}
        placeholder="Select a date..."
        ariaLabel="Select a date"
        onSelectDate={(date: Date | null | undefined) => {
          onSelectDate(date)
        }}
      />
    </>
  )
})
