const { build } = require('esbuild');

build({
    entryPoints: ["src/index.ts"],
    outfile: 'dist/index.js',
    platform: "node",
    format: "esm",
    external: ['react', 'react-dom', 'mobx', 'mobx-react-lite', 'db-main'],
    bundle: true,
    define: {
        __DEV__: true
    },
    logLevel: "silent"
})


build({
    entryPoints: ["src/index.ts"],
    outfile: 'dist/index.cjs',
    platform: "node",
    format: "cjs",
    external: ['react', 'react-dom', 'mobx', 'mobx-react-lite', 'db-main'],
    bundle: true,
    define: {
        __DEV__: true
    },
    logLevel: "silent"
})
