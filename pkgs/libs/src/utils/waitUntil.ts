export const waitUntil = (condition: () => any) => {
  return new Promise<void>((resolve) => {
    const c = setInterval(() => {
      if (condition()) {
        clearInterval(c)
        resolve()
      }
    }, 100)
  })
}
 