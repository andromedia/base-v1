export { matchRoute } from './router/match-route'
export { waitUntil } from './utils/waitUntil'

import { db as dbImpl, dbAll as dbAllImpl } from 'db'
import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'
import { prepareAllDBClient, prepareDBClient } from './db/db-frontend'

export type IRouteResult = Promise<void | { props: Record<string, any> }>

export const db: typeof dbImpl = prepareDBClient('main')

export const dbAll: typeof dbAllImpl = prepareAllDBClient()

type IServerProcess = (props: {
  req: FastifyRequest
  res: FastifyReply
  db: typeof db
  dbAll: typeof dbAll
  jwt: FastifyInstance['jwt']
  params?: any
}) => IRouteResult

export type Server = IServerProcess
