export const prepareDBClient = (dbname: string) => {
  let proxy = new Proxy(
    {},
    {
      get(_, name) {
        const post = async (params: any) => {
          const url = '/_data'
          const options = {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Sec-Fetch-Dest': 'script',
              'Content-Type': 'application/json;charset=UTF-8',
            },
            body: JSON.stringify(params),
          }

          const res = await fetch(url, options)
          return await res.json()
        }

        return {
          async definition() {
            return await post({
              type: 'definition',
              db: dbname,
              table: name,
            })
          },
          async query(params: any) {
            return await post({
              type: 'query',
              db: dbname,
              table: name,
              params,
            })
          },
          async insert(params: any) {
            return await post({
              type: 'insert',
              db: dbname,
              table: name,
              params,
            })
          },
          async update(params: any) {
            return await post({
              type: 'update',
              db: dbname,
              table: name,
              params,
            })
          },
          async delete(params: any) {
            return await post({
              type: 'delete',
              db: dbname,
              table: name,
              params,
            })
          },
        }
      },
    }
  )
  return proxy as any
}

export const prepareAllDBClient = (): any => {
  let proxy = new Proxy(
    {},
    {
      get(_, name: string) {
        return prepareDBClient(name)
      },
    }
  )
  return proxy as any
}
