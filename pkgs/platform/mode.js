const fs = require("fs")
const { join } = require("path")

const excludes = {
    user: {
        "**/.git": true,
        "**/.svn": true,
        "**/.hg": true,
        "**/CVS": true,
        "**/.DS_Store": true,
        "integrations": true,
        "patches": true,
        "test": true,
        ".eslintrc.js": true,
        ".npmrc": true,
        "**/.gitignore": true,
        ".prettierignore": true,
        "**/babel.config.js": true,
        "**/postcss.config.js": true,
        "**/tsconfig.json": true,
        "**/node_modules": true,
        "**/yarn.lock": true,
        "**/babel.config.json": true,
        "**/snowpack.config.js": true,
        "**/web-test-runner.config.js": true,
        "**/types": true,
        "**/.prettierrc": true,
        "db/entry": true,
        ".vscode": true,
        ".build": true,
        "pkgs": true,
    },
    dev: {
    },
}

const settingFile = join(process.cwd(), '..', '..', ".vscode", "settings.json")
const setting = JSON.parse(fs.readFileSync(settingFile, "utf-8"))
const mode = process.argv[2]

switch (mode) {
    case "dev":
        setting["files.exclude"] = excludes.dev
        fs.writeFileSync(settingFile, JSON.stringify(setting, null, 2))
        break
    case "user":
        setting["files.exclude"] = excludes.user
        fs.writeFileSync(settingFile, JSON.stringify(setting, null, 2))
        break
    default:
        console.log("yarn mode [dev|user]")
}