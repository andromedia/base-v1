;(function () {
  var lastTime = 0
  var vendors = ['ms', 'moz', 'webkit', 'o']
  const win = window as any
  for (var x = 0; x < vendors.length && !win.requestAnimationFrame; ++x) {
    win.requestAnimationFrame = win[vendors[x] + 'RequestAnimationFrame']
    win.cancelAnimationFrame =
      win[vendors[x] + 'CancelAnimationFrame'] ||
      win[vendors[x] + 'CancelRequestAnimationFrame']
  }

  if (!win.requestAnimationFrame)
    win.requestAnimationFrame = function (callback: any, element: any) {
      var currTime = new Date().getTime()
      var timeToCall = Math.max(0, 16 - (currTime - lastTime))
      var id = win.setTimeout(function () {
        callback(currTime + timeToCall)
      }, timeToCall)
      lastTime = currTime + timeToCall
      return id
    }

  if (!win.cancelAnimationFrame)
    win.cancelAnimationFrame = function (id: any) {
      clearTimeout(id)
    }
})()
