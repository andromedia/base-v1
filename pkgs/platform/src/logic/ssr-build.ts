import { BuildOptions } from 'esbuild'
import { join } from 'path'
import { dirs } from './dirs'
import { build } from 'esbuild'

export const buildSSR = async () => {
  const pkg = require(join(dirs.web, 'package.json'))
  const external = Object.keys(pkg.dependencies)
  const options: BuildOptions = {
    entryPoints: [join(dirs.web, 'src', 'pages-ssr.tsx')],
    format: 'cjs',
    bundle: true,
    platform: 'node',
    sourcemap: true,
    tsconfig: join(dirs.web, 'tsconfig.json'),
    outfile: join(dirs.client, 'ssr', 'index.js'),
    // external: external,
    executable: dirs.esbuild,
    logLevel: 'silent',
  }
  await build(options)

  try {
    delete require.cache[join(dirs.client, 'ssr', 'index.js')]
    const ssr = require(join(dirs.client, 'ssr', 'index.js'))
    return ssr.default; 
  } catch (e) {
    console.log('\nError when building SSR:\n\n')
    console.error(e)
  }
}
