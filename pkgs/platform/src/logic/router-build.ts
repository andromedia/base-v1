import { build } from 'esbuild'
import { readdir, stat, writeFile } from 'fs-extra'
import { join } from 'path'
import { dirs } from './dirs'
import { buildSSR } from './ssr-build'

export interface IRoute {
  name: string
  path?: string
  compiled: any
  params: object | null
}
export type IRouteIndex = Record<string, IRoute>
export const buildServerRoutes = async () => {
  const files = await rrdir(dirs.server)
  
  const exports: any = {}
  for (let f of files) {
    if (f.endsWith('.ts')) {
      const path = f
        .substr(dirs.server.length + 1, f.length - dirs.server.length - 4)
        .split('/')

      if (path.length > 1) {
        if (exports[path[0]] === undefined) {
          exports[path[0]] = []
        }
        exports[path[0]].push({
          key: path.slice(1).join('/'),
          path: path.join('/'),
        })
      }
    }
  }

  const text = `
  ${Object.entries(exports)
    .map(([key, value]: any) => {
      return `export const ${key} = {
      ${value
        .map((e: any) => {
          return `'${e.key}': require('./${e.path}')`
        })
        .join(',\n')}
    }`
    })
    .join('\n')}
  `

  const index = join(dirs.server, '_index.ts')
  await writeFile(index, text)
  await build({
    entryPoints: [index],
    outfile: join(dirs.serverbuild, 'index.js'),
    bundle: true,
    executable: dirs.esbuild,
    define: {
      'process.env.NODE_ENV': '"production"',
    },
    format: 'cjs',
    platform: 'node',
    // plugins: [
    //   nodeExternalsPlugin({
    //     packagePath: join(dirs.web, 'package.json'),
    //   }),
    // ],
  })
}

export const client = { routes: {} as IRouteIndex }
export const buildClientRoutes = () => {
  return new Promise<void>(async (resolve) => {
    // clean-up route paths cache
    for (let i in client.routes) {
      delete client.routes[i]
    }

    const pages = (await rrdir(dirs.pages)).filter(
      (e) => e.substr(e.length - 3) === 'tsx'
    )

    delete require.cache[join(dirs.serverbuild, 'index.js')]
    const server = require(join(dirs.serverbuild, 'index.js'))

    // extract all routes from pages
    pages.forEach((p) => {
      const patharr = p.substr(dirs.pages.length).split('.')
      patharr.pop()

      const path = 'pages' + patharr.join('.')
      const name =
        'p' +
        path
          .split('/')
          .map((e, idx) => {
            if (idx === 0) return ''
            return capitalize(e)
          })
          .join('')
          .replace('~', '$')
          .replace(/[^A-Za-z0-9$]/gi, '_')

      const finalPath = patharr.join('.')
      client.routes[finalPath] = {
        compiled: server.pages[finalPath.substr(1)],
        name,
        path: finalPath,
        params: null,
      }
    })

    // write pages.jsx to index all our routes in clientside
    const outimports: string[] = []
    const outexports: string[] = []
    const svrimports: string[] = []
    const svrexports: string[] = []

    for (let o of Object.values(client.routes)) {
      let hasServerRoute = false
      if (o.compiled && o.compiled.server) {
        hasServerRoute = true
      }


      svrimports.push(`import ${o.name} from "../pages${o.path}";`)
      svrexports.push(
        `  "${o.path}": {
  c:${o.name},
  s:${hasServerRoute ? 'true' : 'false'}
}`
      )

      if (o.name.indexOf('_layout') >= 0) {
        outimports.push(`import ${o.name} from "../pages${o.path}";`)
        outexports.push(
          `  "${o.path}": {
    c:${o.name},
    s:${hasServerRoute ? 'true' : 'false'}
  }`
        )
      } else {
        outexports.push(
          `  "${o.path}": {
    c:lazy(() => import("../pages${o.path}")), 
    s:${hasServerRoute ? 'true' : 'false'}
  }`
        )
      }
    }

    const out = `
${outimports.join('\n')}
import { lazy } from 'react';
const pages = {
${outexports.join(',\n')}
};
export default pages;
`

    await writeFile(join(dirs.web, 'src', 'pages.tsx'), out)

    const outsvr = `
${svrimports.join('\n')}
import React from 'react';
import { renderToString } from 'react-dom/server';
import MainRouter from 'components/ui/router/router';
import * as router from 'components/ui/router';

const pages = {
${svrexports.join(',\n')}
};
export default {
  pages,
  MainRouter,
  React,
  router,
  renderToString
};
`

    await writeFile(join(dirs.web, 'src', 'pages-ssr.tsx'), outsvr)
    resolve()
  })
}

async function rrdir(dir: string, allFiles: string[] = []) {
  const files = (await readdir(dir)).map((f) => join(dir, f))
  allFiles.push(...files)
  await Promise.all(
    files.map(async (f) => (await stat(f)).isDirectory() && rrdir(f, allFiles))
  )
  return allFiles.map((e) => e.replace(/\\/gi, '/'))
}

const capitalize = (s: string) => {
  return s.charAt(0).toUpperCase() + s.slice(1)
}
