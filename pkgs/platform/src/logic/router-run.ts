import chalk from 'chalk'
import { FastifyReply, FastifyRequest } from 'fastify'
import { readdir, stat } from 'fs-extra'
import { join } from 'path'
import { dirs } from './dirs'
import { IRoute, client } from './router-build'
import { dataRouter } from './router-run-data'
const { matchRoute } = require('libs/dist/index.cjs')

export const runServerRoute = async (
  req: FastifyRequest,
  reply: FastifyReply
) => {
  if (req.url === '/_data') {
    await dataRouter(req, reply)
    return
  }

  delete require.cache[join(dirs.serverbuild, 'index.js')]
  const server = require(join(dirs.serverbuild, 'index.js'))

  let route: IRoute | null = null
  for (let r of Object.keys(server.pages || []).map((e) => '/' + e)) {
    const result = matchRoute(req.url, r)
    if (result) {
      route = { ...client.routes[r] }
      route.path = r
      route.params = result
      route.compiled = server.pages[r.substr(1)]
    }
  }

  const opt = {
    req,
    res: reply,
    reply,
    db: await getDB('main'),
    params: null as any,
    dbAll: await getDB(),
  }
  if (
    route &&
    route.path &&
    route.compiled &&
    typeof route.compiled.server === 'function'
  ) {
    process.stdout.write(
      `${chalk.grey(
        `[ ${chalk.blue(`routes`)} ]`
      )} ${req.method.toUpperCase()} ${req.url}\n`
    )
    opt.params = route.params

    return {
      layout: await runLayout(req.url, route, server, opt),
      page: await route.compiled.server(opt),
    }
  } else {
    return {
      layout: await runLayout(req.url, route, server, opt),
    }
  }
  return
}

const runLayout = async (
  path: string,
  _route: IRoute | null,
  server: any,
  opt: any
) => {
  const p = path.split('/')
  const pages = server.pages

  while (p.length > 0) {
    p.pop()

    let pr = [...p, '_layout'].join('/')
    if (pr.indexOf('/') === 0) {
      pr = pr.substr(1)
    }

    if (pages[pr] && pages[pr].server) {
      const result = await pages[pr].server(opt)
      return result
    }
  }

  return null
}

export const getDB = async (dbname?: string) => {
  let dbs: Record<string, any> = {}
  for (let d of await readdir(dirs.db)) {
    const dir = join(dirs.db, d)
    if ((await stat(dir)).isDirectory()) {
      dbs[d] = require(join(dir, 'dist', 'index.js'))
    }
  }

  const dbnames = Object.keys(dbs)
  if (dbnames.length === 0) {
    return {}
  }

  if (dbname) {
    if (dbnames.indexOf(dbname)) {
      return dbs[dbname]
    }
    return {}
  }

  return dbs
}
