import { FastifyReply, FastifyRequest } from 'fastify'

const { dbAll } = require('db/dist/index.js')

export const dataRouter = async (req: FastifyRequest, reply: FastifyReply) => {
  if (req.method.toLowerCase() === 'post') {
    const body: {
      type: 'query' | 'insert' | 'update' | 'delete' | 'definition'
      db: string
      table: string
      params: string
    } = req.body as any

    // TODO:
    // untuk security kita menggunakan features yg ada di server/db/[table].tsx
    // setiap feature itu menentukan page tertentu (diambil dari http x-page header) boleh
    // mengakses query/insert/update/delete.
    // Setiap role yg diperbolehkan mengakses page (beserta feature nya) tertentu disimpan
    // di database, karena role jg di simpan di database.

    const db = dbAll[body.db]

    if (!db) {
      reply.code(403).send('Forbidden')
      return
    }

    let result: any = null
    switch (body.type) {
      case 'definition':
        result = db[body.table].definition()

        reply.send(
          JSON.stringify({
            db: result.db,
            rels: result.relations,
            columns: result.columns,
          })  
        )
        return
        break
      case 'query':
        result = await db[body.table].query(body.params)
        break
      case 'insert':
        result = await db[body.table].insert(body.params)
        break
      case 'update':
        result = await db[body.table].update(body.params)
        break
      case 'delete':
        result = await db[body.table].insert(body.params)
        break
    }
    reply.removeHeader('Content-Length')
    reply.removeHeader('Transfer-encoding')
    reply.send(JSON.stringify(result))

    // reply.send(JSON.stringify(req.body, null, 2))
    return
  }

  reply.code(403).send('Forbidden')
}
