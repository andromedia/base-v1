import chalk from 'chalk'
import fastify, { FastifyInstance } from 'fastify'
import { IncomingMessage, Server, ServerResponse } from 'http'
import { development } from '../dev/dev'
import { HOST, PORT } from '../index'
import { production } from '../prod/prod'
import { registerAuth } from './auth'
export type IServer = FastifyInstance<Server, IncomingMessage, ServerResponse>

export type IServerArgs = {
  server: IServer
  devPort?: number
}

export const startServer = async (mode: 'dev' | 'prod', devPort?: number) => {
  const server: IServer = fastify()
  registerAuth(server)

  if (mode === 'dev') {
    await development({ server, devPort })
  } else {
    await production({ server })
  }
  server.listen(PORT, HOST, async (err) => {
    if (err) {
      console.error(err)
      process.exit(0)
    }

    if (mode === 'dev') {
      console.log(`${chalk.grey(`[ server ]`)} Server Ready [${mode.toUpperCase()}]`)
    } else {
      console.log(
        `${chalk.grey(`[ server ]`)} Server Listening at ${HOST}:${PORT}`
      )
    }
  })
}
