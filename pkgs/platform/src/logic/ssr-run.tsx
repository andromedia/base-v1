import { FastifyRequest } from 'fastify'
// import { findInvalidElement } from './find-invalid-el'

const { matchRoute } = require('libs/dist/index.cjs')
export const renderSSR = async (
  req: FastifyRequest,
  serverProps: any,
  ssr: any
) => {
  const { React, pages, MainRouter, renderToString } = ssr

  let route: any = null
  for (let [k, v] of Object.entries(ssr.pages || [])) {
    if (matchRoute(req.url, k)) {
      route = v
    }
  }
  if (route) {
    try {
      const ssrProps = {
        url: req.url,
        props: serverProps,
      }
      const el = React.createElement(MainRouter, {
        pages,
        ssr: ssrProps,
      })
      const result = renderToString(el)
      return result
    } catch (e) {
      console.log('error', e)
    }
  } else {
    console.log(`no route found: ${req.url} `)
  }
  return
}
