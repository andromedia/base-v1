import { join } from 'path'

const root = join(process.cwd(), '..', '..')

export const dirs = {
  web: join(root, 'web'),
  pages: join(root, 'web', 'pages'),
  components: join(root, 'web', 'components'),
  server: join(root, 'server'),
  db: join(root, 'db'),
  client: join(root, '.build', 'client'),
  esbuild: join(root, 'node_modules', 'esbuild'),
  serverbuild: join(root, '.build', 'server'),
}
