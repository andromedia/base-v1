const fs = require('fs').promises

export const isFile = async (file: string) => {
  try {
    const stat = await fs.lstat(file)
    return stat.isFile()
  } catch (e) {
    return false
  }
}
