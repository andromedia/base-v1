import chalk from 'chalk'
import chokidar from 'chokidar'
import fstatic from 'fastify-static'
import { readFile, writeFile } from 'fs-extra'
import http from 'http'
import startCase from 'lodash-es/startCase'
import { basename, join } from 'path'
import { dirs } from '../logic/dirs'
import { buildClientRoutes, buildServerRoutes } from '../logic/router-build'
import { runServerRoute } from '../logic/router-run'
import { IServerArgs } from '../logic/server'
import { buildSSR } from '../logic/ssr-build'
import { renderSSR } from '../logic/ssr-run'
import { isFile } from '../logic/utils'

export async function development({ server, devPort }: IServerArgs) {
  await prepareServerRoute()

  server.register(fstatic, {
    root: join(dirs.web, 'public'),
    serve: false,
  })

  server.all('*', async (req, reply) => {
    let serverProps: any = null

    const publicFile = join(dirs.web, 'public', req.url)
    if (await isFile(publicFile)) {
      reply.sendFile(req.url)
      return
    }

    if (
      !req.headers['get-server-props'] &&
      req.headers['sec-fetch-dest'] === 'empty' &&
      req.headers['sec-fetch-mode'] === 'cors' &&
      req.method.toLowerCase() === 'get'
    ) {
      serverProps = null
    } else {
      serverProps = runServerRoute(req, reply)
    }

    if (req.headers['get-server-props']) {
      reply.send(await serverProps)
    } else {
      const proxy = http.request(
        {
          hostname: 'localhost',
          port: devPort,
          path: req.url,
          method: req.method,
        },
        (res: any) => {
          let body = ''
          res.on('data', (output: string) => {
            body += output
          })
          res.on('end', async () => {
            reply.headers(res.headers)

            if ((res.headers['content-type'] || '').indexOf('html') >= 0) {
              body = body.replace(
                '</title>',
                `</title>\n<script>
                window.___DEV___ = true;
                window.HMR_WEBSOCKET_URL = "ws://localhost:${devPort}"
                if (!window.serverProps) { 
                  window.serverProps = ${JSON.stringify({
                    isLoaded: true,
                    props: {},
                    path: req.url,
                  })};
                }
                </script>
                `
              )

              if (serverProps instanceof Promise) {
                const sprops = await serverProps
                if (sprops) {
                  const ssr = await buildSSR()
                  const page = await renderSSR(req, sprops, ssr)

                  reply.send(
                    body
                      .replace(
                        '</title>',
                        `</title><script>
                    window.serverProps = ${JSON.stringify({
                      ...sprops,
                      isLoaded: true,
                      path: req.url,
                    })};</script>`
                      )
                      .replace(
                        '<noscript>You need to enable JavaScript to run this app.</noscript>',
                        ''
                      )
                      .replace(
                        /\<div(.*)id=['"]root['"](.*)\>\<\/div\>/,
                        `<div$1 id="root" $2>${page}</div>`
                      )
                  )
                } else {
                  reply.send(body)
                }
              } else {
                reply.send(body)
              }
            } else {
              reply.send(body)
            }
          })
        }
      )

      proxy.on('error', (error) => {
        console.error(error)
      })

      proxy.end()
    }
  })
}

const prepareServerRoute = async () => {
  watchServer()
  watchWeb()

  await buildServerRoutes() // server route must be built first, because it is required by client routes
  await buildClientRoutes()
}

const watchServer = () => {
  const watcher = chokidar.watch(dirs.server, {
    ignored: /_index.ts/, // ignore dotfiles
    persistent: true,
    alwaysStat: false,
    ignoreInitial: true,
  })
  watcher.on('add', async (e: string) => {
    const content = await readFile(e)
    if (content.length === 0) {
      await writeFile(
        e,
        `
import { Server } from 'libs'

export const server: Server = async ({ req, db, dbAll }) => {
  return {
    props: {
    }, 
  }
}
      `.trim()
      )
    }
    await buildServerRoutes()
  })
  watcher.on('unlink', async () => {
    await buildServerRoutes()
  })
  watcher.on('change', async (p) => {
    process.stdout.write(
      `${chalk.grey(`[ server ]`)} server/${p
        .replace(/\\/gi, '/')
        .substr(dirs.server.length + 1)}`
    )
    await buildServerRoutes()
    process.stdout.write(`${chalk.green(' [done]')}\n`)
  })
}
const watchWeb = () => {
  const createNewReactFile = async (e: string, mode: 'page' | 'comp') => {
    const content = await readFile(e)
    if (content.length === 0) {
      const name = startCase(basename(e.replace(/\W\s/g, ''), 'tsx'))
      await writeFile(
        e,
        `
/** @jsx jsx */
import { jsx } from '@emotion/react'
import { observer } from 'mobx-react-lite'

${
  mode === 'page'
    ? `
const Page${name} = (props: any) => {
  return <div>Page ${name}</div>
}

export default observer(Page${name});
`.trim()
    : `
export const Page${name} = observer((props: any) => {
  return <div>Page ${name}</div>
})
`.trim()
}

      `.trim()
      )
    }
  }
  const compsWatcher = chokidar.watch(dirs.components, {
    persistent: true,
    alwaysStat: false,
    ignoreInitial: true,
  })
  compsWatcher.on('add', async (e: string) => {
    createNewReactFile(e, 'comp')
  })
  const pagesWatcher = chokidar.watch(dirs.pages, {
    ignored: /_index.ts/, // ignore dotfiles
    persistent: true,
    alwaysStat: false,
    ignoreInitial: true,
  })
  pagesWatcher.on('add', async (e: string) => {
    createNewReactFile(e, 'page')
    await buildClientRoutes()
  })
  pagesWatcher.on('unlink', async () => {
    await buildClientRoutes()
  })
}
