import fstatic from 'fastify-static'
import { readFile } from 'fs-extra'
import { join } from 'path'
import { dirs } from '../logic/dirs'
import { buildClientRoutes, buildServerRoutes } from '../logic/router-build'
import { runServerRoute } from '../logic/router-run'
import { IServerArgs } from '../logic/server'
import { buildSSR } from '../logic/ssr-build'
import { renderSSR } from '../logic/ssr-run'
import { isFile } from '../logic/utils'

export const production = async ({ server }: IServerArgs) => {
  await prepareServerRoute()
  const ssr = await buildSSR()

  server.register(fstatic, {
    root: dirs.client,
    serve: false,
  })

  const index = await readFile(join(dirs.client, 'index.html'), 'utf-8')

  server.all('*', async (req, reply) => {
    let serverProps: any = { props: {} }

    if (
      !req.headers['get-server-props'] &&
      req.headers['sec-fetch-dest'] === 'empty' &&
      req.headers['sec-fetch-mode'] === 'cors' &&
      req.method.toLowerCase() === 'get'
    ) {
      serverProps = { page: {}, layout: {} }
    } else {
      serverProps = await runServerRoute(req, reply)
    }

    if (req.headers['get-server-props']) {
      reply.send(await serverProps)
      return
    }

    const publicFile = join(dirs.client, req.url)
    if (await isFile(publicFile)) {
      reply.sendFile(req.url)
      return
    }

    const page = await renderSSR(req, serverProps, ssr)

    reply.type('text/html; charset=UTF-8')
    const body = index
      .replace(
        '</title>',
        `</title><script>window.serverProps = ${JSON.stringify({
          ...serverProps,
          isLoaded: true,
          path: req.url,
        })};</script>`
      )
      .replace(
        '<noscript>You need to enable JavaScript to run this app.</noscript>',
        ''
      )
      .replace(
        /\<div(.*)id=['"]root['"](.*)\>\<\/div\>/,
        `<div$1 id="root" $2>${page}</div>`
      )
    reply.send(body)
  })
}

const prepareServerRoute = async () => {
  await buildServerRoutes() // server route must be built first, because it is required by client routes
  await buildClientRoutes()
}
