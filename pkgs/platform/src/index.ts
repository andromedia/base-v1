import 'core-js'
import { startServer } from './logic/server'
import arg from 'arg'
import get from 'lodash-es/get'

require('browser-env')()
require('./logic/raf')
window.Symbol = Symbol
window.Map = Map
window.Set = Set
;(window as any).isSSR = true
;(global as any).jest = 1

const args = arg({
  '--port': Number,
})

export const PORT = get(args, '--port', 3200)
export const HOST = '0.0.0.0'

const mode = get(args, '_.0', 'dev')

switch (mode) {
  case 'dev':
    startServer('dev', PORT + 1)
    break

  case 'prod':
    startServer('prod')
    break
}
