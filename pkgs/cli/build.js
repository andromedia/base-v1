const { build } = require('esbuild');

build({
    entryPoints: ["src/index.tsx"],
    outdir: '../../.build/cli',
    external: ['fsevents'],
    platform: "node",
    format: "cjs",
    bundle: true,
    define: {
        __DEV__: true
    },
    logLevel: "silent"
})
