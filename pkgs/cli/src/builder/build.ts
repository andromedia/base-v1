import { build as esbuild, BuildOptions } from 'esbuild'
// import execa from 'execa'
// import { pathExists, remove } from 'fs-extra'
// import { runInAction } from 'mobx'
import { join } from 'path'
import get from 'lodash-es/get'
// import { dev } from '../dev/dev'
// import { startBuildTimer, stopBuildTimer } from '../dev/watch-and-build'
import { dirs } from '../libs/dirs'
import { nodeExternalsPlugin } from 'esbuild-node-externals'

export const externals: Record<string, any> = {
  libs: true,
  platform: true,
}

export const build = async (
  path: string,
  opt?: { outfile?: string; external?: string[]; entryPoints?: string[] }
) => {
  const options: BuildOptions = {
    entryPoints: get(opt, 'entryPoints') || [join(path, 'src', 'index.ts')],
    format: path.indexOf('libs') >= 0 ? 'esm' : 'cjs',
    bundle: true,
    platform: 'node',
    sourcemap: true,
    tsconfig: join(path, 'tsconfig.json'),
    outfile: get(opt, 'outfile') || join(path, 'dist', 'index.js'),
    external: [
      'react',
      'react-dom',
      'mobx',
      'browser-env',
      'mobx-react-lite',
      'libs',
      'canvas',
      'clientprod',
      'knex',
      'snowpack',
      'axios',
      'db-main',
      'fsevents',
    ],
    executable: dirs.esbuild,
    logLevel: 'silent',
  }
  if (path.indexOf(dirs.db) === 0) {
    options.external = [...get(opt, 'external', []), ...Object.keys(externals)]
    options.plugins = [
      nodeExternalsPlugin({
        packagePath: join(path, 'package.json'),
      }),
    ]
  }

  await esbuild(options)
}

// export const buildTypes = async (type: keyof typeof workspaces) => {
//   runInAction(() => {
//     dev.build.current = type
//     dev.app.server.log += `[ typing ] Generating types for ${type}...\n`
//   })
//   startBuildTimer()

//   const path = workspaces[type]
//   if (await pathExists(join(path, 'dist', 'types'))) {
//     await remove(join(path, 'dist', 'types'))
//   }

//   let success = true
//   const tsc = join(dirs.node_modules, 'typescript', 'lib', 'tsc.js')

//   try {
//     const child = execa.node(tsc, ['--build', join(path, 'tsconfig.json')], {
//       all: true,
//       env: { FORCE_COLOR: 'true' },
//     })

//     child.all?.on('data', (e) => {
//       const output = e.toString('utf-8')
//       runInAction(() => {
//         dev.app.server.log += output
//       })
//     })

//     await child
//   } catch (e) {
//     runInAction(() => {
//       dev.app.server.log += e.stack
//     })
//   }

//   stopBuildTimer()
//   runInAction(() => {
//     dev.build.current = ''
//     if (success) {
//       dev.app.server.log += `[ typing ] Typings for ${type} generated\n`
//     }
//   })
// }
