import execa from 'execa'
import { join } from 'path'
import { build } from '../builder/build'
import { dirs } from '../libs/dirs'
import chalk from 'chalk'
import { PORT } from '..'
import { get } from 'lodash-es'
import { copy, writeFile } from 'fs-extra'

const list: any = [
  {
    name: 'libs',
    dir: dirs.libs,
    onAfterBuild: async () => {},
  },
  {
    name: 'platform',
    dir: dirs.platform,
    out: join(dirs.build, 'platform', 'index.js'),
  },
  // {
  //   name: 'client',
  //   dir: dirs.client,
  //   entryPoints: [join(dirs.client, 'dist', 'pages-server.js')],
  //   out: join(dirs.client, 'build', 'index.js'),
  //   external: ['react', 'react-dom'],
  // },
]

export const startProd = (build = true) => {
  setTimeout(async () => {
    process.stdout.write('\033c')
    process.stdout.write(
      `Starting ${chalk.bold(chalk.green(`Plansys`))} in ${chalk.underline(
        chalk.blueBright(`Production`)
      )} mode \n\n`
    )

    if (build) {
      console.log(
        chalk.grey(
          'To skip snowpack build, next time execute this command: \n'
        ) + 'yarn prod serve\n'
      )

      await buildTailwind()

      await buildWeb()
    } else {
      console.log('Skipping snowpack build...\n')
    }

    await copy(
      join(dirs.web, 'tsconfig.json'),
      join(dirs.client, 'tsconfig.json')
    )
    await writeFile(
      join(dirs.client, 'package.json'),
      JSON.stringify(
        {
          name: 'clientprod',
          version: '1.0.0',
        },
        null,
        2
      )
    )

    await buildServer()
    await runPlatform(PORT)
  })
}

const buildTailwind = async () => {
  const yarn = execa(
    'yarn',
    ['web', 'tailwindcss', 'build', '-o', 'src/tailwind-prod.css'],
    {
      cwd: dirs.root,
      all: true,
      env: { FORCE_COLOR: 'true' },
    }
  )

  yarn.all?.pipe(process.stdout)
  await yarn
}

const buildWeb = async () => {
  const yarn = execa('yarn', ['build'], {
    cwd: dirs.web,
    all: true,
    env: { FORCE_COLOR: 'true' },
  })

  yarn.all?.pipe(process.stdout)
  await yarn
}

const buildServer = async () => {
  for (let i of list) {
    process.stdout.write(`${chalk.grey(`[ server ]`)} Building ${i.name} `)
    await build(i.dir, {
      outfile: i.out,
      entryPoints: get(i, 'entryPoints'),
      external: i.external,
    })
    process.stdout.write(`${chalk.green(`[done]`)}\n`)
  }
}

const runPlatform = async (port: number) => {
  const platform = execa.node(
    join(dirs.build, 'platform', 'index.js'),
    ['prod', '--port', port.toString()],
    {
      cwd: join(dirs.platform),
      env: { FORCE_COLOR: 'true' },
      all: true,
    }
  )

  platform.all?.pipe(process.stdout)
  await platform
}
