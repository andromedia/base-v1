import { startDev } from './dev/dev'
import { startProd } from './prod/prod'
import arg from 'arg'
import get from 'lodash-es/get'
const args = arg({
  '--port': Number,
})

const mode = get(args, '_.0', 'dev')
export const PORT = get(args, '--port', 3200)
switch (mode) {
  case 'dev':
    startDev()
    break
  case 'prod':
    startProd(get(args, '_.1') !== 'serve')
    break
}

console.log(args)
