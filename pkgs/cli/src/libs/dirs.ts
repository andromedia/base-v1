import { join } from 'path'

const root = join(process.cwd(), '..', '..')
export const dirs = {
  root,
  cli: join(process.cwd(), 'src'),
  libs: join(root, 'pkgs', 'libs'),
  ui: join(root, 'pkgs', 'ui'),
  pkgs: join(root, 'pkgs'),
  webui: join(root, 'web', 'components', 'ui'),
  basegit: join(root, '.build', 'basegit'),
  client: join(root, '.build', 'client'),
  build: join(root, '.build'),
  platform: join(root, 'pkgs', 'platform'),
  web: join(root, 'web'),
  mobile: join(root, 'mobile'),
  db: join(root, 'db'),
  node_modules: join(root, 'node_modules'),
  bin: join(root, 'node_modules', '.bin'),
  esbuild: join(root, 'node_modules', 'esbuild')
}
