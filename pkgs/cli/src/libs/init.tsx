import React from 'react'
import { Instance, render, Text } from 'ink'

export let currentInk: Instance | null = null
export const init = (component: any) => {
  const Dummy = () => {
    return <Text>Starting...</Text>
  }

  const dummy = render(<Dummy />)
  dummy.unmount()
  dummy.cleanup()
  dummy.clear()

  setTimeout(() => {
    process.stdout.write('\033c')
    currentInk = render(component)
  })
}
