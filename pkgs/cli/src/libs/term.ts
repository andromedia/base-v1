export class Term {
  clear() {
    const readline = require('readline')
    readline.cursorTo(process.stdout, 0, 0)
    readline.clearScreenDown(process.stdout)
  }
}

const term = new Term()
export default term
