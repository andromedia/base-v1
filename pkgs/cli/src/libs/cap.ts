export const cap = (text: string, height: number, up?: number): string => {
  if (typeof text !== 'string') {
    return ''
  }

  const rows = text.trim().split('\n')

  if (rows.length < height) return rows.join('\n')
  if (up && up > 0) {
    if (rows.length - height - up >= 0) {
      return rows.slice(rows.length - height - up, rows.length - up).join('\n')
    } else {
      return rows.slice(0, height).join('\n')
    }
  }
  return rows.slice(rows.length - height).join('\n')
}
