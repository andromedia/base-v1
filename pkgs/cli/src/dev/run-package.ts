import execa, { ExecaChildProcess } from 'execa'
import { join } from 'path'
import { PORT } from '..'
import { dirs } from '../libs/dirs'

export const runPackage = (
  app: 'web' | 'mobile' | 'platform',
  callback?: (
    output: string,
    child: ExecaChildProcess,
    mode: 'error' | 'data'
  ) => void,
  args?: any
) => {
  const command: any[] = ['', []]

  if (app === 'web') {
    command[0] = join(dirs.node_modules, 'snowpack', 'index.bin.js')
    command[1] = ['dev', ...(args || []), '--port', (PORT + 1).toString()]
  } else if (app === 'platform') {
    command[0] = join(dirs.build, 'platform', 'index.js')
    command[1] = ['dev', '--port', PORT.toString()]
  }

  const res = execa.node(command[0], command[1], {
    cwd: join(dirs[app]),
    env: { FORCE_COLOR: 'true' },
  })
  const { stdout, stderr } = res

  if (stdout && stderr) {
    stderr.addListener('data', (e) => {
      const output = e.toString('utf-8')
      if (callback) {
        callback(output, res, 'error')
      }
    })
    stdout.addListener('data', (e) => {
      const output = e.toString('utf-8')
      if (callback) {
        callback(output, res, 'data')
      }
    })
  }

  return res
}
