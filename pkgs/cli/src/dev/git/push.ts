import { copy, readdir, remove } from 'fs-extra'
import { commit, push as gitpush, add } from 'isomorphic-git'
import { join } from 'path'
import { dirs } from '../../libs/dirs'
import { clone } from './clone'
import globby from 'globby'

const http = require('isomorphic-git/http/node')
const fs = require('fs')

export const push = async (msg: string, log: (text: string) => void) => {
  log('Cloning...\n')
  await clone(log)

  const pkgs = join(dirs.root, 'pkgs')
  const gitpkgs = join(dirs.basegit, 'pkgs')

  for (let i of await readdir(pkgs)) {
    const from = join(pkgs, i)
    const to = join(gitpkgs, i)
    await remove(to)
    await copy(from, to)
    log(`Copying ${i}`)
  }

  const paths = await globby(['./**', './**/.*'], {
    gitignore: true,
    cwd: dirs.basegit,
  })
  for (const filepath of paths) {
    await add({ fs, dir: dirs.basegit, filepath })
  }

  await commit({
    fs,
    dir: dirs.basegit,
    author: {
      name: 'Andromedia Deployer',
      email: 'deploy@andromedia.co.id',
    },
    message: msg,
  })

  await gitpush({
    fs,
    http,
    dir: dirs.basegit,
    remote: 'origin',
    ref: 'master',
    onAuth: () => ({
      username: 'androdeploy',
      password: 'andromediadeploy123',
    }),
  })
}
