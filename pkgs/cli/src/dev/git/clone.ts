import { pathExists, remove } from 'fs-extra'
import { dirs } from '../../libs/dirs'
import git from 'isomorphic-git'

const http = require('isomorphic-git/http/node')
const fs = require('fs')

export const clone = async (log: (text: string) => void) => {
  if (await pathExists(dirs.basegit)) {
    await remove(dirs.basegit)
  }

  await git.clone({
    fs,
    http,
    dir: dirs.basegit,
    depth: 1,
    url: 'https://bitbucket.org/andromedia/base.git',
    onMessage: (e) => {
      log(e)
    },
  })
}
