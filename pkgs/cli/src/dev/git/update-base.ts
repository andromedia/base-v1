import execa from 'execa'
import { copy, pathExists, readdirSync, remove, stat } from 'fs-extra'
import { join } from 'path'
import { exit } from 'process'
import { dirs } from '../../libs/dirs'
import { clone } from './clone'
import { pull } from './pull'

export const updateBase = async (log: (text: string) => void) => {
  if (await pathExists(dirs.basegit)) {
    log('Pulling...\n')
    await pull(log)
  } else {
    log('Cloning...\n')
    await clone(log)
  }
  const files = readdirSync(dirs.pkgs)

  try {
    for (const file of files) {
      const pkdir = join(dirs.pkgs, file)
      if ((await pathExists(pkdir)) && (await stat(pkdir)).isDirectory()) {
        const files = readdirSync(pkdir)
        for (const file of files) {
          await remove(join(pkdir, file))
        }
        await copy(join(dirs.basegit, 'pkgs', file), pkdir)
      }
    }
  } catch (e) {
    log(e)
  }

  log('Running Yarn...\n')
  const yarn = execa('yarn', [''], {
    cwd: dirs.root,
    all: true,
    env: { FORCE_COLOR: 'true' },
  })

  yarn.all?.on('data', (e) => {
    const output = e.toString('utf-8')
    log(output)
  })
  await yarn
  log('Update success. Exiting DEVPlansys, please run "yarn start" again.')
  exit()
}
