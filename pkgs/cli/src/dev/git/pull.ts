import git from 'isomorphic-git'
import { dirs } from '../../libs/dirs'

const http = require('isomorphic-git/http/node')
const fs = require('fs')

export const pull = async (log: (text: string) => void) => {
  await git.pull({
    fs,
    http,
    dir: dirs.basegit,
    author: {
      name: 'andromedia',
      email: 'deploy@andromedia.co.id',
    },
    url: 'https://bitbucket.org/andromedia/base.git',
    onMessage: (e) => {
      log(e)
    },
  })
}
