import { mkdirp, pathExists, readFile, remove, writeFile } from 'fs-extra'
import { Discovery } from './discovery'
import { toLower } from 'lodash-es'
import { runInAction } from 'mobx'
import { join } from 'path'
import { build } from '../../builder/build'
import { DiscoveryOracle } from './discovery/oracle'
import { DiscoveryPostgresql } from './discovery/postgersql'

const discovery = {
  postgresql: DiscoveryPostgresql,
  oracledb: DiscoveryOracle,
}

export const introspectDB = async (dir: string, conn: any) => {
  const { knex, dbconfig }: any = require(join(dir, 'dist', 'index.js'))
  const Dclass = (discovery as any)[dbconfig.client]

  const query = async (sql: string, params?: any) => {
    const res = await knex.raw(sql, params)
    if (dbconfig.client === 'oracledb') return res
    else if (dbconfig.client === 'postgresql') return res.rows
  }

  const mkdir = async (dir: string) => {
    if (await pathExists(dir)) {
      await remove(dir)
    }
    await mkdirp(dir)
  }

  if (Dclass) {
    runInAction(() => {
      conn.building = true
      conn.log = `Building db:${conn.name}...`
    })

    const dc: Discovery = new Dclass({ knex })
    const tables = await query(await dc.buildQueryTables({}))
    const indexRaw = await readFile(join(dir, 'src', 'index.ts'), 'utf-8')
    const index = indexRaw.split('// export generated model')
    await writeFile(
      join(dir, 'src', 'index.ts'),
      `${index[0].trim()}

// export generated model
`.trim()
    )

    await mkdir(join(dir, 'src', 'generated'))

    let exportModels = []

    const tcref: Record<string, string> = {}

    for (let table of tables) {
      tcref[table.name] = toLower(table.name)
    }

    const imports: Record<string, Record<string, Record<string, boolean>>> = {}
    const relations: Record<
      string,
      Record<
        string,
        {
          relation: 'Model.HasManyRelation' | 'Model.BelongsToOneRelation'
          typings: string
          modelClass: string
          join: {
            from: string
            to: string
          }
        }
      >
    > = {}

    runInAction(() => {
      conn.log = `Generating ${tables.length} tables\n\n`
    })

    // build relations graph
    for (let table of tables) {
      if (!relations[table.name]) relations[table.name] = {}
      if (!imports[table.name]) imports[table.name] = {}
      const impr = imports[table.name]
      const belongsTo = await query(
        await dc.buildQueryForeignKeys('', table.name)
      )

      const relation = relations[table.name]
      belongsTo.forEach((e: any) => {
        let targetTable: string = e.pkTableName
        let fromTable: string = e.fkTableName
        if (!impr[`./${tcref[targetTable]}`])
          impr[`./${tcref[targetTable]}`] = {}
        impr[`./${tcref[targetTable]}`][toLower(tcref[targetTable])] = true
        impr[`./${tcref[targetTable]}`][`${tcref[targetTable]}_options`] = true
        relation[targetTable] = {
          modelClass: toLower(tcref[targetTable]),
          typings: `${tcref[targetTable]}_options`,
          relation: 'Model.BelongsToOneRelation',
          join: {
            from: `${fromTable}.${e.fkColumnName}`,
            to: `${targetTable}.${e.pkColumnName}`,
          },
        }

        if (!relations[targetTable]) relations[targetTable] = {}
        const targetRelation = relations[targetTable]
        targetRelation[fromTable] = {
          modelClass: toLower(tcref[fromTable]),
          typings: `${tcref[fromTable]}_options`,
          relation: 'Model.HasManyRelation',
          join: {
            from: `${targetTable}.${e.pkColumnName}`,
            to: `${fromTable}.${e.fkColumnName}`,
          },
        }

        if (!imports[targetTable]) imports[targetTable] = {}
        if (!imports[targetTable][`./${tcref[fromTable]}`])
          imports[targetTable][`./${tcref[fromTable]}`] = {}

        imports[targetTable][`./${tcref[fromTable]}`][
          `${toLower(tcref[fromTable])}`
        ] = true
        imports[targetTable][`./${tcref[fromTable]}`][
          `${tcref[fromTable]}_options`
        ] = true
      })
    }

    // write all generated table
    for (let table of tables) {
      const ids = await query(await dc.buildQueryPrimaryKeys('', table.name))
      const columns = await dc.discoverModelFields(table.name, {
        owner: table.owner,
      })
      const relationsMapping: Record<string, string> = {}
      const relationsStatic: Record<string, string> = {}
      for (let r of Object.entries(relations[table.name])) {
        relationsMapping[r[0]] = `{
          relation: ${r[1].relation},
          modelClass: ${r[1].modelClass},
          join: {
            from: "${r[1].join.from}",
            to: "${r[1].join.to}",
          }
        }`
        relationsStatic[r[0]] = `{
          relation: "${r[1].relation}",
          table: "${r[1].modelClass}",
          join: {
            from: "${r[1].join.from}",
            to: "${r[1].join.to}",
          }
        }`
      }

      const className = toLower(table.name)
      const modelName = toLower(table.name)
      const source = `
import { Model, PartialModelGraph, PartialModelObject } from 'objection';
const dbconfig = require('../../config.js')

${Object.entries(imports[table.name])
  .map((e) => {
    return `import  { ${Object.keys(e[1]).join(', ')} } from "${e[0]}"`
  })
  .join('\n')}

export class ${modelName} extends Model {
  static get tableName() {
    return '${table.name}';
  }

  static get idColumn() {
    return ${JSON.stringify(
      ids.length === 1 ? ids[0].columnName : ids.map((e: any) => e.columnName)
    )}
  }

  static get relationMappings() {
    return { 
      ${Object.entries(relationsMapping)
        .map((e: any) => {
          return `${e[0]}:${e[1]}`
        })
        .join(',\n\t')}
    }
  }

  static db = {
    name: "${conn.name}",
    type: dbconfig.client
  }

  static relations = { 
    ${Object.entries(relationsStatic)
      .map((e: any) => {
        return `${e[0]}:${e[1]}`
      })
      .join(',\n\t')}
  }
  
  static columns = { 
    ${columns
      .map((e) => {
        let type: any = e.type
        if (type === 'date') {
          type = 'Date'
        }
        if (type === 'json') {
          type = 'string'
        }

        return `${toLower((e as any).columnName)} : { name: "${
          e.columnName
        }", type: "${type}" },`
      })
      .join('\n    ')}
  };
}

export interface ${className}_select {
  _fields?: true,
  ${columns
    .map((e) => {
      return `${toLower((e as any).columnName)}?: boolean | string`
    })
    .join('\n\t')}${'\n\t'}${Object.entries(relations[table.name])
        .map((e) => {
          return `${e[0]}?: boolean | ${e[1].typings}`
        })
        .join('\n\t')}
}

export interface ${className}_where {
  _and?: any,
  _or?:any,
  ${columns
    .map((e) => {
      let jstype: any = e.type

      if (jstype === 'date') {
        jstype = 'Date'
      }
      if (jstype === 'json') {
        jstype = 'string'
      }

      const colName = toLower((e as any).columnName)
      const basic = `[string, ${jstype}] | [string, ${jstype}, 'or' | 'and']`
      const intype = `['in' | 'notin', ${jstype}[]]`

      let specific = ''
      if (jstype === 'Date' || 'number') {
        specific = ` | ['between', ${jstype}, ${jstype}] | ['between', ${jstype}, ${jstype}, 'or' | 'and']`
      }

      return `${colName}?: ${jstype} | ${intype} | ${basic} ${specific}`
    })
    .join('\n\t')}
}

export interface ${className}_insert  {
  ${columns
    .map((e) => {
      let jstype: any = e.type

      if (jstype === 'date') {
        jstype = 'Date'
      }
      if (jstype === 'json') {
        jstype = 'string'
      }

      const nullable = (e as any).nullable
      const colName = toLower((e as any).columnName)
      const isPrimaryKey =
        ids.map((e: any) => e.columnName).indexOf(colName) >= 0

      return `${colName}${
        nullable !== 'NO' || isPrimaryKey ? '?' : ''
      }: ${jstype}`
    })
    .filter((e) => !!e)
    .join('\n\t')}
}

export interface ${className}_update  {
  data: {
    ${columns
      .map((e) => {
        let jstype: any = e.type

        if (jstype === 'date') {
          jstype = 'Date'
        }
        if (jstype === 'json') {
          jstype = 'string'
        }

        const colName = toLower((e as any).columnName)
        return `${colName}?: ${jstype}`
      })
      .filter((e) => !!e)
      .join('\n\t')}
  },
  where: ${className}_where
}

export interface ${className}_options {
  select?: ${className}_select
  where?: ${className}_where | (() => any)
  limit?: number
  group?: (${columns
    .map((e) => `'${toLower((e as any).columnName)}'`)
    .join(' | ')})[]
  order?: (${columns
    .map((e) => `'${toLower((e as any).columnName)}'`)
    .join(' | ')} | {
    column: ${columns
      .map((e) => `'${toLower((e as any).columnName)}'`)
      .join(' | ')};
    order: 'asc' | 'desc' | 'ASC' | 'DESC'
  })[]
  offset?: number
}

export default {
  definition: () => ${className},
  update: async (fields: ${className}_update) => {
    return await ${className}.query()
      .update(fields.data as PartialModelObject<${className}>)
      .where(fields.where as any);
  }, 
  delete: async (where: ${className}_where) => {
    return await ${className}.query()
      .delete()
      .where(where as any);
  },
  insert: async (fields: ${className}_insert) => {
    return await ${modelName}.query().insertGraph(fields as PartialModelGraph<${className}_insert>);
  }, 
  query: async (props?: ${className}_options) => {
    const defaultCols = Object.keys(${className}.columns);
    const colInput = (props && props.select) ? Object.keys(props.select) : defaultCols;
    const colTables = [];
    const colRels = [];
    const where = props?.where

    for (let col of colInput) {
      if (col !== '_fields') {
        if (defaultCols.indexOf(col) >= 0) {
          if (props?.select && typeof (props?.select as any)[col] === 'string') {
            colTables.push('raw:' + col);
          } else {
            colTables.push(col);
          }

        } else {
          colRels.push(col);
        }
      }
    }
    const query = ${modelName}.query();

    if (props?.limit) {
      query.limit(props?.limit)
    }

    if (props?.order) {
      query.orderBy(props?.order)
    }

    if (props?.group) {
      query.groupByRaw(props?.group as any)
    }

    if (props?.offset) {
      query.offset(props?.offset)
    }

    if (colInput.indexOf('_fields') >= 0) {
      for (let i of Object.keys(post.columns as any)) {
        colTables.push(i);
      }
    }

    if (colTables.length > 0) {
      query.select(
        ...colTables.map((e) => {
          if (e.indexOf('raw:') === 0) {
            return Model.knex().raw((props?.select as any)[e.substr(4)])
          } else {
            const col = (${className}.columns as any)[e]
            if (col.name !== e) {
              return { [e]: col.name }
            }
            return col.name
          }
        })
      )
    } else { // we have to do this to select no columns
      query.select('');
    }

    if (where) {
      if (typeof where === 'function') {
        query.where(where)
      } else {
        for (let [k, e] of Object.entries(where)) {

          if (!Array.isArray(e)) {
            e = [e];
          }

          let andor = e[e.length - 1] === 'or' ? 'or' : 'and'
          if (andor === 'and') {
            if (e[0] === 'between') {
              query.whereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.whereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.whereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.andWhere(k, e[0], e[1])
              } else {
                query.andWhere(k, e[0])
              }
            }
          } else if (andor === 'or') {
            if (e[0] === 'between') {
              query.orWhereBetween(k, [e[1], e[2]])
            } else if (e[0] === 'in') {
              query.orWhereIn(k, e[1])
            } else if (e[0] === 'notin') {
              query.orWhereNotIn(k, e[1])
            } else {
              if (e.length > 1) {
                query.orWhere(k, e[0], e[1])
              } else {
                query.orWhere(k, e[0])
              }
            }
          }
        }
      }
    }

    if (colRels.length > 0) {
      for (let i of colRels) {
        buildRelation(query, i, (props as any).select[i])
      }
    }

    return await query;
  },
  _types: (_where: ${modelName}_where | (() => any)) => {}
}


const buildRelation = (query: any, relname: string, fkcols: any) => {
  query.withGraphFetched(\`\${relname}(selectColumns)\`).modifiers({
    selectColumns(builder: any) {
      if (typeof fkcols === 'object' && fkcols.select) {
        let fkfields = []
        let fkrels = []
        const fkall = Object.keys((builder.modelClass() as any).columns)
        for (let i in fkcols.select) {
          if (fkall.indexOf(i) >= 0) {
            fkfields.push(i)
          } else {
            fkrels.push(i)
          }
        }

        if (fkfields.length > 0) {
          builder.select(...fkfields);
        } else { // select no columns
          builder.select('');
        }

        if (fkrels.length > 0) {
          for (let ffk of fkrels) {
            buildRelation(builder, ffk, fkcols.select[ffk])
            builder.withGraphFetched(ffk)
          }
        }
      }
    },
  })
}

`.trim()

      exportModels.push(
        `export { default as ${className} } from './generated/${className}'`
      )

      runInAction(() => {
        conn.log += `Generated: ${className}.ts\n`
      })

      await writeFile(join(dir, 'src', 'generated', className + '.ts'), source)
    }

    await writeFile(
      join(dir, 'src', 'index.ts'),
      `
${index[0].trim()}

// export generated model

${exportModels.join('\n')}
`.trim()
    )

    await build(dir)

    runInAction(() => {
      conn.log = `Generated ${tables.length} models`
      conn.building = false
    })
  }
}
