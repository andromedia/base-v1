import {
  Discovery,
  IDiscoveryOptions,
  IField,
  IFieldType,
  IIndex,
} from '../discovery'
import extend from 'lodash-es/extend'
import clone from 'lodash-es/cloneDeep'

interface IOraConn {
  schema?: string
  knex: any
}
export class DiscoveryOracle extends Discovery {
  _conn: IOraConn
  _options: IDiscoveryOptions = {}

  constructor(conn: IOraConn) {
    super()
    this._conn = { schema: 'public', ...conn }
    this._options.schema = this._conn.schema
  }

  async executeSQL(sql: string, params?: any): Promise<any[]> {
    const result = await this._conn.knex.raw(sql, params)
    return result
  }

  async showFields(_table: string): Promise<IField[]> {
    return []
  }

  async showIndexes(_table: string): Promise<IIndex[]> {
    return []
  }

  paginateSQL(
    sql: string,
    orderBy: string,
    inputOptions?: IDiscoveryOptions
  ): string {
    function getPagination(filter?: IDiscoveryOptions) {
      const pagination = []
      if (filter && (filter.limit || filter.offset || filter.skip)) {
        let offset = Number(filter.offset)
        if (!offset) {
          offset = Number(filter.skip)
        }
        if (offset) {
          pagination.push('R >= ' + (offset + 1))
        } else {
          offset = 0
        }
        const limit = Number(filter.limit)
        if (limit) {
          pagination.push('R <= ' + (offset + limit))
        }
      }
      return pagination
    }

    const pagination = getPagination(inputOptions)
    orderBy = orderBy || '1'
    if (pagination.length) {
      return (
        'SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY ' +
        orderBy +
        ') R, ' +
        sql.substring(7) +
        ') WHERE ' +
        pagination.join(' AND ')
      )
    } else {
      return sql
    }
  }
  buildQueryTables(options: IDiscoveryOptions): string {
    let sqlTables = null
    const owner = options.owner || options.schema

    if (options.all && !owner) {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", ' +
          'table_name AS "name", ' +
          'owner AS "owner" FROM all_tables',
        'owner, table_name',
        options
      )
    } else if (owner) {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", ' +
          'table_name AS "name", ' +
          'owner AS "owner" FROM all_tables WHERE owner=\'' +
          owner +
          "'",
        'owner, table_name',
        options
      )
    } else {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", ' +
          'table_name AS "name", SYS_CONTEXT(\'USERENV\',' +
          ' \'SESSION_USER\') AS "owner" FROM user_tables',
        'table_name',
        options
      )
    }
    return sqlTables
  }

  buildQueryViews(options: IDiscoveryOptions): string {
    let sqlViews = ''
    if (options.views) {
      const owner = options.owner || options.schema

      if (options.all && !owner) {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", ' +
            'view_name AS "name", owner AS "owner" FROM all_views',
          'owner, view_name',
          options
        )
      } else if (owner) {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", ' +
            'view_name AS "name", owner AS "owner" FROM all_views ' +
            "WHERE owner='" +
            owner +
            "'",
          'owner, view_name',
          options
        )
      } else {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", ' +
            "view_name AS \"name\", SYS_CONTEXT('USERENV', 'SESSION_USER')" +
            ' AS "owner" FROM user_views',
          'view_name',
          options
        )
      }
    }
    return sqlViews
  }
  buildQueryColumns(owner: string, table: string): string {
    let sql = null
    if (owner) {
      sql = this.paginateSQL(
        'SELECT owner AS "owner", table_name AS ' +
          ' "tableName", column_name AS "columnName", data_type AS "dataType",' +
          ' data_length AS "dataLength", data_precision AS "dataPrecision",' +
          ' data_scale AS "dataScale", nullable AS "nullable"' +
          ' FROM all_tab_columns' +
          " WHERE owner='" +
          owner +
          "'" +
          (table ? " AND table_name='" + table + "'" : ''),
        'table_name, column_id',
        {}
      )
    } else {
      sql = this.paginateSQL(
        'SELECT' +
          " SYS_CONTEXT('USERENV', 'SESSION_USER') " +
          ' AS "owner", table_name AS "tableName", column_name AS "columnName",' +
          ' data_type AS "dataType",' +
          ' data_length AS "dataLength", data_precision AS "dataPrecision",' +
          ' data_scale AS "dataScale", nullable AS "nullable"' +
          ' FROM user_tab_columns' +
          (table ? " WHERE table_name='" + table + "'" : ''),
        'table_name, column_id',
        {}
      )
    }
    sql += ' ORDER BY column_id'
    return sql
  }
  buildQueryPrimaryKeys(owner: string, table: string): string {
    let sql =
      'SELECT uc.owner AS "owner", ' +
      'uc.table_name AS "tableName", col.column_name AS "columnName",' +
      ' col.position AS "keySeq", uc.constraint_name AS "pkName" FROM' +
      (owner
        ? ' ALL_CONSTRAINTS uc, ALL_CONS_COLUMNS col'
        : ' USER_CONSTRAINTS uc, USER_CONS_COLUMNS col') +
      " WHERE uc.constraint_type='P' AND " +
      'uc.constraint_name=col.constraint_name'

    if (owner) {
      sql += " AND uc.owner='" + owner + "'"
    }
    if (table) {
      sql += " AND uc.table_name='" + table + "'"
    }
    sql +=
      ' ORDER BY uc.owner, col.constraint_name, uc.table_name, ' +
      'col.position'
    return sql
  }
  buildQueryForeignKeys(owner: string, table: string): string {
    let sql =
      'SELECT a.owner AS "fkOwner", a.constraint_name AS "fkName", ' +
      'a.table_name AS "fkTableName", a.column_name AS "fkColumnName", ' +
      'a.position AS "keySeq", jcol.owner AS "pkOwner", ' +
      'jcol.constraint_name AS "pkName", jcol.table_name AS "pkTableName", ' +
      'jcol.column_name AS "pkColumnName"' +
      ' FROM' +
      ' (SELECT' +
      ' uc.owner, uc.table_name, uc.constraint_name, uc.r_constraint_name, ' +
      'col.column_name, col.position' +
      ' FROM' +
      (owner
        ? ' ALL_CONSTRAINTS uc, ALL_CONS_COLUMNS col'
        : ' USER_CONSTRAINTS uc, USER_CONS_COLUMNS col') +
      ' WHERE' +
      " uc.constraint_type='R' and uc.constraint_name=col.constraint_name"
    if (owner) {
      sql += " AND uc.owner='" + owner + "'"
    }
    if (table) {
      sql += " AND uc.table_name='" + table + "'"
    }
    sql +=
      ' ) a' +
      ' INNER JOIN' +
      ' USER_CONS_COLUMNS jcol' +
      ' ON' +
      ' a.r_constraint_name=jcol.constraint_name'
    return sql
  }
  buildQueryExportedForeignKeys(owner: string, table: string): string {
    let sql =
      'SELECT a.constraint_name AS "fkName", a.owner AS "fkOwner", ' +
      'a.table_name AS "fkTableName",' +
      ' a.column_name AS "fkColumnName", a.position AS "keySeq",' +
      ' jcol.constraint_name AS "pkName", jcol.owner AS "pkOwner",' +
      ' jcol.table_name AS "pkTableName", jcol.column_name AS "pkColumnName"' +
      ' FROM' +
      ' (SELECT' +
      ' uc1.table_name, uc1.constraint_name, uc1.r_constraint_name, ' +
      'col.column_name, col.position, col.owner' +
      ' FROM' +
      (owner
        ? ' ALL_CONSTRAINTS uc, ALL_CONSTRAINTS uc1, ALL_CONS_COLUMNS col'
        : ' USER_CONSTRAINTS uc, USER_CONSTRAINTS uc1, USER_CONS_COLUMNS col') +
      ' WHERE' +
      " uc.constraint_type='P' and" +
      ' uc1.r_constraint_name = uc.constraint_name and' +
      " uc1.constraint_type = 'R' and" +
      ' uc1.constraint_name=col.constraint_name'
    if (owner) {
      sql += " and col.owner='" + owner + "'"
    }
    if (table) {
      sql += " and uc.table_Name='" + table + "'"
    }
    sql +=
      ' ) a' +
      ' INNER JOIN' +
      ' USER_CONS_COLUMNS jcol' +
      ' ON' +
      ' a.r_constraint_name=jcol.constraint_name' +
      ' order by a.owner, a.table_name, a.position'

    return sql
  }

  buildPropertyType(
    colDef: { dataType: string; dataLength: number },
    _options?: IDiscoveryOptions
  ): IFieldType {
    const oracleType = colDef.dataType
    const dataLength = colDef.dataLength
    const type = oracleType.toUpperCase()
    switch (type) {
      case 'CHAR':
        if (dataLength === 1) {
          // Treat char(1) as boolean
          return 'boolean'
        } else {
          return 'string'
        }
        break
      case 'VARCHAR':
      case 'VARCHAR2':
      case 'LONG VARCHAR':
      case 'NCHAR':
      case 'NVARCHAR2':
        return 'string'
      case 'LONG':
      case 'BLOB':
      case 'CLOB':
      case 'NCLOB':
        return 'binary'
      case 'NUMBER':
      case 'INTEGER':
      case 'DECIMAL':
      case 'DOUBLE':
      case 'FLOAT':
      case 'BIGINT':
      case 'SMALLINT':
      case 'REAL':
      case 'NUMERIC':
      case 'BINARY_FLOAT':
      case 'BINARY_DOUBLE':
      case 'UROWID':
      case 'ROWID':
        return 'number'
      case 'DATE':
      case 'TIMESTAMP':
        return 'date'
      default:
        return 'string'
    }
  }

  async discoverModelIndexes(_table: string): Promise<Record<string, IIndex>> {
    return {}
  }

  setDefaultOptions(options: IDiscoveryOptions): void {
    this._options = clone(options)
  }
  setNullableProperty(_options: IDiscoveryOptions): void {}
  getDefaultSchema(): string {
    return 'public'
  }
  async getArgs(
    table: string,
    inputOptions: IDiscoveryOptions
  ): Promise<{
    owner: string
    schema: string
    table: string
    options: IDiscoveryOptions
  }> {
    const options = extend(this._options, inputOptions)
    return {
      owner: options.owner || options.schema || '',
      schema: options.owner || options.schema || '',
      table: table,
      options: options,
    }
  }

  async discoverModelFields(
    _table: string,
    _options: IDiscoveryOptions
  ): Promise<Record<string, any>[]> {
    const args = await this.getArgs(_table, _options)
    const { owner, table } = args

    const sql = this.buildQueryColumns(owner, table)
    const results = await this.executeSQL(sql)
    return results.map((r) => {
      r.type = this.buildPropertyType(r)
      return r
    })
  }
}
