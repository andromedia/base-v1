import {
  Discovery,
  IDiscoveryOptions,
  IField,
  IFieldType,
  IIndex,
} from '../discovery'
import extend from 'lodash-es/extend'
import clone from 'lodash-es/cloneDeep'

interface IPgConn {
  schema?: string
  knex: any
}
export class DiscoveryPostgresql extends Discovery {
  _conn: IPgConn
  _options: IDiscoveryOptions = {}

  constructor(conn: IPgConn) {
    super()
    this._conn = { schema: 'public', ...conn }
    this._options.schema = this._conn.schema
  }

  async executeSQL(sql: string, params?: any): Promise<any[]> {
    const result = await this._conn.knex.raw(sql, params)
    return result.rows
  }

  async showFields(table: string): Promise<IField[]> {
    const sql =
      'SELECT column_name AS "column", data_type AS "type", ' +
      'datetime_precision AS time_precision, ' +
      'is_nullable AS "nullable", character_maximum_length as "length"' + // , data_default AS "Default"'
      ' FROM "information_schema"."columns" WHERE table_name=\'' +
      table +
      "' and table_schema='" +
      this._conn.schema +
      "'"

    const fields = await this.executeSQL(sql)
    fields.forEach((field) => {
      field.type = this.buildPropertyType({ dataType: field.type })
      field.dbtype = this.mapPostgreSQLDatatypes(
        field.type,
        field.length,
        field.time_precision
      )
    })

    return fields
  }

  mapPostgreSQLDatatypes(
    typeName: string,
    typeLength: number,
    typeTimePrecision: number
  ) {
    const type = typeName.toUpperCase()
    let strPrecision = ''
    if (typeTimePrecision < 6) {
      // default is 6
      strPrecision = '(' + typeTimePrecision + ') '
    }
    switch (type) {
      case 'CHARACTER VARYING':
      case 'VARCHAR':
        return typeLength ? 'VARCHAR(' + typeLength + ')' : 'VARCHAR(1024)'
      case 'TIMESTAMP WITHOUT TIME ZONE':
        return 'TIMESTAMP ' + strPrecision + 'WITHOUT TIME ZONE'
      case 'TIMESTAMP WITH TIME ZONE':
        return 'TIMESTAMP ' + strPrecision + 'WITH TIME ZONE'
      case 'TIME WITHOUT TIME ZONE':
        return 'TIME ' + strPrecision + 'WITHOUT TIME ZONE'
      case 'TIME WITH TIME ZONE':
        return 'TIME ' + strPrecision + 'WITH TIME ZONE'
      default:
        return type
    }
  }
  async showIndexes(table: string): Promise<IIndex[]> {
    const sql =
      'SELECT t.relname AS "table", i.relname AS "name", ' +
      'am.amname AS "type", ix.indisprimary AS "primary", ' +
      'ix.indisunique AS "unique", ' +
      'ARRAY(SELECT pg_get_indexdef(ix.indexrelid, k + 1, true) ' +
      '  FROM generate_subscripts(ix.indkey, 1) AS k ' +
      '  ORDER BY k ) AS "keys", ' +
      'ARRAY(SELECT ' +
      "  CASE ix.indoption[k] & 1 WHEN 1 THEN 'DESC' ELSE 'ASC' END " +
      '  FROM generate_subscripts(ix.indoption, 1) AS k ' +
      '  ORDER BY k ' +
      ') AS "order" ' +
      'FROM pg_class t, pg_class i, pg_index ix, pg_am am, ' +
      'pg_namespace ns WHERE t.oid = ix.indrelid AND ' +
      'i.oid = ix.indexrelid AND ' +
      'i.relam = am.oid AND ' +
      "t.relkind='r' AND t.relname='" +
      table +
      "'" +
      " and (ns.oid = t.relnamespace and ns.nspname='" +
      this._conn.schema +
      "')"

    return await this.executeSQL(sql)
  }

  paginateSQL(
    sql: string,
    orderBy: string,
    inputOptions?: IDiscoveryOptions
  ): string {
    const options = extend(this._options, inputOptions)

    let limit = ''
    if (options.offset || options.skip || options.limit) {
      limit = ' OFFSET ' + (options.offset || options.skip || 0) // Offset starts from 0
      if (options.limit) {
        limit = limit + ' LIMIT ' + options.limit
      }
    }
    if (!orderBy) {
      sql += ' ORDER BY ' + orderBy
    }
    return sql + limit
  }
  buildQueryTables(options: IDiscoveryOptions): string {
    let sqlTables = null
    const owner = options.owner || options.schema

    if (options.all && !owner) {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", table_name AS "name", table_schema AS "owner"' +
          ' FROM information_schema.tables',
        'table_schema, table_name',
        options
      )
    } else if (owner) {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", table_name AS "name", table_schema AS "owner"' +
          " FROM information_schema.tables WHERE table_schema='" +
          owner +
          "'",
        'table_schema, table_name',
        options
      )
    } else {
      sqlTables = this.paginateSQL(
        'SELECT \'table\' AS "type", table_name AS "name",' +
          ' table_schema AS "owner" FROM information_schema.tables WHERE table_schema=current_schema()',
        'table_name',
        options
      )
    }
    return sqlTables
  }

  buildQueryViews(options: IDiscoveryOptions): string {
    let sqlViews = ''
    if (options.views) {
      const owner = options.owner || options.schema

      if (options.all && !owner) {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", table_name AS "name",' +
            ' table_schema AS "owner" FROM information_schema.views',
          'table_schema, table_name',
          options
        )
      } else if (owner) {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", table_name AS "name",' +
            ' table_schema AS "owner" FROM information_schema.views WHERE table_schema=\'' +
            owner +
            "'",
          'table_schema, table_name',
          options
        )
      } else {
        sqlViews = this.paginateSQL(
          'SELECT \'view\' AS "type", table_name AS "name",' +
            ' current_schema() AS "owner" FROM information_schema.views',
          'table_name',
          options
        )
      }
    }
    return sqlViews
  }
  buildQueryColumns(owner: string, table: string): string {
    let sql = ''
    if (owner) {
      sql = this.paginateSQL(
        'SELECT table_schema AS "owner", table_name AS "tableName", column_name AS "columnName",' +
          'data_type AS "dataType", character_maximum_length AS "dataLength", numeric_precision AS "dataPrecision",' +
          ' numeric_scale AS "dataScale", is_nullable AS "nullable"' +
          ' FROM information_schema.columns' +
          " WHERE table_schema='" +
          owner +
          "'" +
          (table ? " AND table_name='" + table + "'" : ''),
        'table_name, ordinal_position',
        {}
      )
    } else {
      sql = this.paginateSQL(
        'SELECT current_schema() AS "owner", table_name AS "tableName",' +
          ' column_name AS "columnName",' +
          ' data_type AS "dataType", character_maximum_length AS "dataLength", numeric_precision AS "dataPrecision",' +
          ' numeric_scale AS "dataScale", is_nullable AS "nullable"' +
          ' FROM information_schema.columns' +
          (table ? " WHERE table_name='" + table + "'" : ''),
        'table_name, ordinal_position',
        {}
      )
    }
    return sql
  }
  buildQueryPrimaryKeys(owner: string, table: string): string {
    let sql =
      'SELECT kc.table_schema AS "owner", ' +
      'kc.table_name AS "tableName", kc.column_name AS "columnName",' +
      ' kc.ordinal_position AS "keySeq",' +
      ' kc.constraint_name AS "pkName" FROM' +
      ' information_schema.key_column_usage kc' +
      ' JOIN information_schema.table_constraints tc' +
      ' ON kc.table_name = tc.table_name AND kc.table_schema = tc.table_schema' +
      ' AND kc.constraint_name = tc.constraint_name' +
      " WHERE tc.constraint_type='PRIMARY KEY'"

    if (owner) {
      sql += " AND kc.table_schema='" + owner + "'"
    }
    if (table) {
      sql += " AND kc.table_name='" + table + "'"
    }
    sql += ' ORDER BY kc.table_schema, kc.table_name, kc.ordinal_position'
    return sql
  }
  buildQueryForeignKeys(owner: string, table: string): string {
    let sql =
      'SELECT tc.table_schema AS "fkOwner", tc.constraint_name AS "fkName", tc.table_name AS "fkTableName",' +
      ' kcu.column_name AS "fkColumnName", kcu.ordinal_position AS "keySeq",' +
      ' ccu.table_schema AS "pkOwner",' +
      ' (SELECT constraint_name' +
      ' FROM information_schema.table_constraints tc2' +
      ' WHERE tc2.constraint_type = \'PRIMARY KEY\' and tc2.table_name=ccu.table_name limit 1) AS "pkName",' +
      ' ccu.table_name AS "pkTableName", ccu.column_name AS "pkColumnName"' +
      ' FROM information_schema.table_constraints tc' +
      ' JOIN information_schema.key_column_usage AS kcu' +
      ' ON tc.constraint_schema = kcu.constraint_schema AND tc.constraint_name = kcu.constraint_name' +
      ' JOIN information_schema.constraint_column_usage ccu' +
      ' ON ccu.constraint_schema = tc.constraint_schema AND ccu.constraint_name = tc.constraint_name' +
      " WHERE tc.constraint_type = 'FOREIGN KEY'"
    if (owner) {
      sql += " AND tc.table_schema='" + owner + "'"
    }
    if (table) {
      sql += " AND tc.table_name='" + table + "'"
    }
    return sql
  }
  buildQueryExportedForeignKeys(owner: string, table: string): string {
    let sql =
      'SELECT kcu.constraint_name AS "fkName", kcu.table_schema AS "fkOwner", kcu.table_name AS "fkTableName",' +
      ' kcu.column_name AS "fkColumnName", kcu.ordinal_position AS "keySeq",' +
      ' (SELECT constraint_name' +
      ' FROM information_schema.table_constraints tc2' +
      ' WHERE tc2.constraint_type = \'PRIMARY KEY\' and tc2.table_name=ccu.table_name limit 1) AS "pkName",' +
      ' ccu.table_schema AS "pkOwner", ccu.table_name AS "pkTableName", ccu.column_name AS "pkColumnName"' +
      ' FROM' +
      ' information_schema.constraint_column_usage ccu' +
      ' JOIN information_schema.key_column_usage kcu' +
      ' ON ccu.constraint_schema = kcu.constraint_schema AND ccu.constraint_name = kcu.constraint_name' +
      ' WHERE kcu.position_in_unique_constraint IS NOT NULL'
    if (owner) {
      sql += " and ccu.table_schema='" + owner + "'"
    }
    if (table) {
      sql += " and ccu.table_name='" + table + "'"
    }
    sql += ' order by kcu.table_schema, kcu.table_name, kcu.ordinal_position'

    return sql
  }

  buildPropertyType(
    colDef: { dataType: string },
    _options?: IDiscoveryOptions
  ): IFieldType {
    const mysqlType = colDef.dataType
    const type = mysqlType.toUpperCase()
    switch (type) {
      case 'BOOLEAN':
        return 'boolean'
      case 'CHARACTER VARYING':
      case 'VARCHAR':
      case 'CHARACTER':
      case 'TEXT':
        return 'string'
      case 'BYTEA':
        return 'binary'
      case 'SMALLINT':
      case 'INTEGER':
      case 'BIGINT':
      case 'DECIMAL':
      case 'NUMERIC':
      case 'REAL':
      case 'DOUBLE PRECISION':
      case 'FLOAT':
      case 'SERIAL':
      case 'BIGSERIAL':
        return 'number'
      case 'DATE':
      case 'TIMESTAMP':
      case 'TIMESTAMP WITHOUT TIME ZONE':
      case 'TIMESTAMP WITH TIME ZONE':
      case 'TIME':
      case 'TIME WITHOUT TIME ZONE':
      case 'TIME WITH TIME ZONE':
        return 'date'
      case 'JSON':
      case 'JSONB':
        return 'json'
      case 'POINT':
        return 'geopoint'
      default:
        return 'string'
    }
  }

  async discoverModelIndexes(table: string): Promise<Record<string, IIndex>> {
    const indexes = await this.showIndexes(table)
    const indexData: Record<string, IIndex> = {}
    indexes.forEach(function (index) {
      indexData[index.name] = index
    })
    return indexData
  }

  setDefaultOptions(options: IDiscoveryOptions): void {
    this._options = clone(options)
  }
  setNullableProperty(_options: IDiscoveryOptions): void {}
  getDefaultSchema(): string {
    return 'public'
  }
  async getArgs(
    table: string,
    inputOptions: IDiscoveryOptions
  ): Promise<{
    owner: string
    schema: string
    table: string
    options: IDiscoveryOptions
  }> {
    const options = extend(this._options, inputOptions)
    return {
      owner: options.owner || options.schema || '',
      schema: options.owner || options.schema || '',
      table: table,
      options: options,
    }
  }

  async discoverModelFields(
    table: string,
    options: IDiscoveryOptions
  ): Promise<Record<string, any>[]> {
    const args = await this.getArgs(table, options)
    let schema = args.schema

    table = args.table
    options = args.options

    if (!schema) {
      schema = this.getDefaultSchema()
    }
    this.setDefaultOptions(options)

    const sql = this.buildQueryColumns(schema, table)
    const results = await this.executeSQL(sql)
    return results.map((r) => {
      // PostgreSQL accepts float(1) to float(24) as selecting the `real` type,
      // while float(25) to float(53) select `double precision`
      // https://www.postgresql.org/docs/9.4/static/datatype-numeric.html
      if (r.dataType === 'real' || r.dataType === 'double precision')
        r.dataType = 'float'
      // Set data precision to be `null` if the data type is either integer,
      // bigint or smallint. This is to avoid syntax errors when updating/migrating
      // model with a discovered schema that consists of those data types
      if (
        r.dataType === 'integer' ||
        r.dataType === 'bigint' ||
        r.dataType === 'smallint'
      )
        r.dataPrecision = null
      r.type = this.buildPropertyType(r)
      this.setNullableProperty(r)
      return r
    })
  }
}
