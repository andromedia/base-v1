import execa, { ExecaChildProcess } from 'execa'
import { mkdirp, pathExists, readdir, remove, writeFile } from 'fs-extra'
import { Box, Text, useInput, useStdout } from 'ink'
import SelectInput from 'ink-select-input/build'
import { capitalize } from 'lodash-es'
import { action, observable, runInAction } from 'mobx'
import { observer } from 'mobx-react-lite'
import { join } from 'path'
import React from 'react'
import { build, externals } from '../../builder/build'
import { cap } from '../../libs/cap'
import { dirs } from '../../libs/dirs'
import { dev, RootMenuKey } from '../dev'
import { CreateConn } from './create'
import { introspectDB } from './introspect'

import chokidar from 'chokidar'
class DB {
  isReady = false

  async createEntry() {
    const dir = {
      entry: join(dirs.db, 'entry'),
      src: join(dirs.db, 'entry', 'src'),
    }

    for (let e of Object.values(dir)) {
      await mkdirp(e)
    }

    for (let [k, e] of Object.entries(entryFiles)) {
      await writeFile(join(dirs.db, 'entry', k), e)
    }
  }

  async init() {
    let list: string[] = []

    try {
      list = await readdir(dirs.db)
    } catch (e) {}

    if (list.filter((e) => e !== 'entry').length === 0) {
      await this.createEntry()
      await writeFile(
        join(dirs.db, 'entry', 'src', 'index.ts'),
        `export const db = {}
         export const dbAll = {}`
      )
    } else {
      runInAction(() => {
        for (let i of list) {
          if (i === 'entry') continue
          if (!meta.active) {
            meta.active = i
          }
          meta.conn[i] = {
            log: '',
            name: i,
            dir: join(dirs.db, i),
            child: null,
            building: false,
          }
        }
      })
      for (let d of list) {
        const dir = join(dirs.db, d)
        externals['db-' + d] = dir
      }
      await writeFile(
        join(dirs.db, 'entry', 'src', 'index.ts'),
        `
${list
  .filter((e) => e !== 'entry')
  .map((e) => {
    return `import * as db${e} from 'db-${e}'`
  })
  .join('\n')}

export const db = db${list.filter((e) => e !== 'entry')[0]};
  
export const dbAll = {
  ${list
    .filter((e) => e !== 'entry')
    .map((e) => {
      return `${e}: db${e}`
    })
    .join(',\n  ')}
};
      `
      )
    }

    this.isReady = true
  }

  async buildEntry() {
    await build(join(dirs.db, 'entry'))
  }

  async buildAll() {
    if (!this.isReady) {
      await this.init()
    }
    for (let d of Object.keys(meta.conn)) {
      const conn = meta.conn[d]
      const dir = join(dirs.db, d)

      runInAction(() => {
        conn.log += `Building db:${d}...`
        conn.building = true
      })

      if (await pathExists(join(dir, 'dist'))) {
        await remove(join(dir, 'dist'))
      }
      await build(dir)

      runInAction(() => {
        conn.building = false
        conn.log = ''
      })

      await this.run(d, dir)

      this.watchAndBuild(d, dir)
    }
    await this.buildEntry()
  }

  async run(name: string, dir: string) {
    const child = execa.node(join(dir, 'dist', 'index.js'), {
      all: true,
      env: { FORCE_COLOR: 'true' },
    })

    child.all?.on('data', (e) => {
      const output = e.toString('utf-8')
      runInAction(() => {
        meta.conn[name].log += output
      })
    })
    await child
  }

  async watchAndBuild(name: string, dir: string) {
    const conn = meta.conn[name]
    if (conn) {
      const watcher = chokidar.watch(join(dir, 'src'), {
        ignored: /(^|[\/\\])\../, // ignore dotfiles
        persistent: true,
        alwaysStat: false,
        ignoreInitial: true,
      })

      const rebuild = async () => {
        const conn = meta.conn[name]
        if (conn.building) return

        if (await pathExists(join(dir, 'dist'))) {
          await remove(join(dir, 'dist'))
        }
        runInAction(() => {
          conn.log = `Building db:${name}...`
        })
        await build(dir)

        runInAction(() => {
          conn.log = ``
          conn.building = false
        })

        await this.buildEntry()
        await this.run(name, dir)
      }

      watcher.on('change', rebuild)
      watcher.on('add', rebuild)
      watcher.on('unlink', rebuild)
    }
  }
}
export const db = new DB()

const meta = observable({
  conn: {} as Record<
    string,
    {
      log: string
      dir: string
      name: string
      child: ExecaChildProcess | null
      building: boolean
    }
  >,
  pane: '',
  active: '',
  log: '',
})
export const DbComponent = observer(() => {
  const conn = meta.conn[meta.active]
  useInput(
    action((_input, key) => {
      if (dev.pane === 'db') {
        if (meta.pane === '') meta.pane = 'main'
        else {
          if (meta.pane === 'main') {
            if (key.upArrow || key.escape) {
              dev.pane = 'root'
            }
            if (key.return) {
              meta.pane = 'menu'
              if (meta.active === 'new_conn') {
                meta.pane = 'create'
              }
              return
            }

            const conns = Object.keys(meta.conn)
            conns.push('new_conn')
            const idx = conns.indexOf(meta.active)
            if (key.leftArrow) {
              if (idx - 1 < 0) {
                meta.active = conns[conns.length - 1]
              } else {
                meta.active = conns[idx - 1]
              }
            }
            if (key.rightArrow) {
              if (idx + 1 > conns.length - 1) {
                meta.active = conns[0]
              } else {
                meta.active = conns[idx + 1]
              }
            }
          } else if (meta.pane === 'menu') {
            if (key.return || key.escape) {
              meta.pane = 'main'
            }
          }
        }
      } else {
        meta.pane = ''
      }
    })
  )
  const { stdout } = useStdout()
  const size = {
    w: stdout?.columns || 80,
    h: stdout?.rows || 24,
  }

  if (
    dev.pane === 'db' &&
    (meta.pane === 'create' || Object.keys(meta.conn).length === 0)
  ) {
    return (
      <CreateConn
        done={async () => {
          runInAction(() => {
            meta.pane = 'creating'
            meta.log = ''
          })

          const res = execa('yarn', [], {
            cwd: dirs.root,
            env: { FORCE_COLOR: 'true' },
            all: true,
          })

          res.all?.addListener('data', (e) => {
            meta.log += e
          })
          await res

          await db.init()
          await db.buildAll()

          runInAction(() => {
            meta.pane = ''
          })
        }}
      />
    )
  }

  if (meta.pane === 'creating') {
    const log = meta.log.split('\n')
    return (
      <Box flexGrow={1} marginTop={1} flexDirection={'column'}>
        <Text>
          {log
            .slice(log.length - 5)
            .map((e) => {
              if (e.length > 80) {
                return e.substr(0, 80) + '...'
              } else {
                return e
              }
            })
            .join('\n')}
        </Text>
      </Box>
    )
  }

  return (
    <Box alignItems={'stretch'} flexGrow={1} flexDirection={'column'}>
      <Box height={1} marginBottom={1}>
        <RootMenuKey />
        <Text color={'blue'} bold={true}>
          └{' '}
        </Text>
        {Object.keys(meta.conn).map((e, idx) => {
          return (
            <Box marginRight={1} key={idx}>
              <Text
                underline={meta.active === e}
                bold={true}
                color={
                  dev.pane === 'db' && meta.active === e ? 'yellow' : 'white'
                }
              >
                {capitalize(e)}
              </Text>
            </Box>
          )
        })}

        <Box marginRight={1}>
          <Text
            underline={meta.active === 'new_conn'}
            bold={true}
            color={
              dev.pane === 'db' && meta.active === 'new_conn'
                ? 'yellow'
                : 'white'
            }
          >
            New
          </Text>
        </Box>
      </Box>
      <Box flexGrow={1}>
        {(meta.pane === 'main' || meta.pane === '') && (
          <Text>{cap(conn ? conn.log : '', size.h - 3, dev.up)} </Text>
        )}
        {meta.pane === 'menu' &&
        Object.keys(meta.conn).length > 0 &&
        meta.active !== 'new_conn' ? (
          <Box flexDirection={'column'}>
            <Box marginBottom={1}>
              <Text>
                Press <Text bold={true}>[esc]</Text> to back,{' '}
                <Text bold={true}>[enter]</Text> to select:
              </Text>
            </Box>
            <SelectInput
              isFocused={true}
              onSelect={async (e) => {
                const conn = meta.conn[meta.active]
                if (conn) {
                  switch (e.value) {
                    case 'clear-log':
                      runInAction(() => {
                        conn.log = ''
                      })
                      break
                    case 'introspect':
                      introspectDB(conn.dir, conn)
                      break
                  }
                }
              }}
              items={[
                {
                  label: 'Clear Log',
                  value: 'clear-log',
                },
                {
                  label: 'Introspect - Generate Model',
                  value: 'introspect',
                },
              ]}
            />
          </Box>
        ) : (
          (Object.keys(meta.conn).length === 0 ||
            meta.active === 'new_conn') && (
            <Text>
              Press{' '}
              <Text color="magentaBright" bold={true}>
                [enter]
              </Text>{' '}
              to create DB Connection
            </Text>
          )
        )}
      </Box>
    </Box>
  )
})

const entryFiles = {
  'package.json': `
  {
    "name": "db",
    "version": "0.1.1",
    "license": "UNLICENSED",
    "main": "dist/index.js",
    "types": "src/index.ts",
    "scripts": {
        "postinstall": "cd ../.. && yarn patch-package"
    },
    "dependencies": {},
    "devDependencies": {}
  }`,
  'tsconfig.json': `
  {
    "include": ["src", "types"],
    "exclude": ["./dist/**/*" ],
    "compilerOptions": {
      "module": "commonjs",
      "lib": ["esnext", "DOM"],
      "target": "ES5",
      "importHelpers": true,
      "declaration": true,
      "sourceMap": true,
      "outDir": "./dist/types",
      "rootDir": "./src",
      "strict": true,
      "emitDeclarationOnly": true,
      "noImplicitReturns": true,
      "noFallthroughCasesInSwitch": true,
      "noUnusedLocals": true,
      "noUnusedParameters": true,
      "moduleResolution": "node",
      "jsx": "react",
      "esModuleInterop": true,
      "skipLibCheck": true,
      "forceConsistentCasingInFileNames": true
    }
  }
  `,
}
