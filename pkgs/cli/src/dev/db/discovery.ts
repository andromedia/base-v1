export interface IDiscoveryOptions {
  // paginateSQL
  offset?: number
  skip?: number
  limit?: number

  // buildQueryTables
  schema?: string
  owner?: string
  all?: boolean

  // buildQueryViews
  views?: string
}

export interface IField {
  column: string
  dbtype: string
  type: string
  nullable: boolean
  default?: any
  length?: number
  time_precision?: number
}
export interface IIndex {
  table: string
  name: string
  type: string
  primary: boolean
  unique: boolean
  keys?: string[]
  order?: string[]
}

export type IFieldType =
  | 'string'
  | 'date'
  | 'boolean'
  | 'binary'
  | 'number'
  | 'geopoint'
  | 'json'

export abstract class Discovery {
  abstract paginateSQL(
    sql: string,
    orderBy: string,
    options?: IDiscoveryOptions
  ): string
  abstract buildQueryTables(options: IDiscoveryOptions): string
  abstract buildQueryViews(options: IDiscoveryOptions): string
  abstract buildQueryColumns(owner: string, table: string): string
  abstract buildQueryPrimaryKeys(owner: string, table: string): string
  abstract buildQueryForeignKeys(owner: string, table: string): string
  abstract buildQueryExportedForeignKeys(owner: string, table: string): string
  abstract buildPropertyType(
    colDef: { dataType: string },
    options?: IDiscoveryOptions
  ): IFieldType

  abstract discoverModelIndexes(table: string): Promise<Record<string, IIndex>>
  abstract setDefaultOptions(options: IDiscoveryOptions): void
  abstract setNullableProperty(options: IDiscoveryOptions): void
  abstract getDefaultSchema(): string
  abstract getArgs(
    table: string,
    options: IDiscoveryOptions
  ): Promise<{
    owner: string
    schema: string
    table: string
    options: IDiscoveryOptions
  }>
  abstract showFields(table: string): Promise<IField[]>
  abstract showIndexes(table: string): Promise<IIndex[]>

  async getTableStatus(table: string) {
    const raws: [Promise<IField[]>, Promise<IIndex[]>] = [
      this.showFields(table),
      this.showIndexes(table),
    ]
    const result = await Promise.all(raws)
    return {
      fields: result[0],
      indexes: result[1],
    }
  }

  abstract discoverModelFields(
    table: string,
    options: IDiscoveryOptions
  ): Promise<Record<string, IField>[]>

  abstract executeSQL(sql: string, params?: any): Promise<any[]>
}


