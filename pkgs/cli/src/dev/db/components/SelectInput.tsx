import React from 'react'
import InkSelectInput from 'ink-select-input'

export default function SelectInput({
  onSubmit,
  onBlur,
  onChange,
  onFocus,
  ...props
}: any) {
  React.useEffect(() => {
    if (onFocus) {
      onFocus()
    }
    if (onBlur) {
      return onBlur
    }
  }, [onFocus, onBlur])
  return (
    <InkSelectInput
      {...props}
      onSelect={({ value }) => {
        onChange(value)
        onSubmit()
      }}
    />
  )
}
