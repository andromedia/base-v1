import React from 'react'
import { Box, Text } from 'ink'

export default function Error({ children }: any) {
  return (
    <Box>
      <Text color="red">{children}</Text>
    </Box>
  )
}
