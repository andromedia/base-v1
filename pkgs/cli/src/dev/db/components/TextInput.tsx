import React from 'react'
import { UncontrolledTextInput } from 'ink-text-input'

export default function TextInput({ onBlur, onFocus, ...props }: any) {
  React.useEffect(() => {
    if (onFocus) {
      onFocus()
    }
    if (onBlur) {
      return onBlur
    }
  }, [onFocus, onBlur])
  return <UncontrolledTextInput {...props} />
}
