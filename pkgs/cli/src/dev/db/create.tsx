import { mkdirp, pathExists, writeFile } from 'fs-extra'
import { Box, Text, useInput } from 'ink'
import { action, runInAction } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import { join } from 'path'
import React from 'react'
import { dirs } from '../../libs/dirs'
import SelectInput from './components/SelectInput'
import TextInput from './components/TextInput'

export const CreateConn = observer(({ done }: any) => {
  const state = useLocalObservable(() => ({
    active: 0,
    forms: {
      type: 'postgresql',
      name: 'main',
      host: 'db.plansys.co',
      port: '5432',
      dbname: 'pelindo',
      user: 'postgres',
      password: 'andromedia123oke',
    },
    log: '',
    submit: false,
    fields: [
      {
        name: 'type',
        label: 'Type',
        Input: SelectInput,
        inputConfig: {
          items: [
            { label: 'PostgreSQL', value: 'postgresql' },
            { label: 'Oracle', value: 'oracledb' },
          ],
        },
        validate: () => {
          runInAction(() => {
            const first = state.fields[0]

            if (state.forms.type === 'oracledb') {
              state.fields = [
                first,
                {
                  name: 'name',
                  label: 'Connection Name',
                  placeholder: 'main',
                  Input: TextInput,
                },
                {
                  name: 'host',
                  label: 'Host',
                  Input: TextInput,
                },
                {
                  name: 'port',
                  label: 'Port',
                  placeholder: '1521',
                  Input: TextInput,
                },
                {
                  name: 'service_name',
                  label: 'Service Name',
                  Input: TextInput,
                },
                {
                  name: 'user',
                  label: 'Username',
                  Input: TextInput,
                },
                {
                  name: 'password',
                  label: 'Password',
                  Input: TextInput,
                },
              ]
            } else {
              state.fields = [
                first,
                {
                  name: 'name',
                  label: 'Connection Name',
                  placeholder: 'main',
                  Input: TextInput,
                },
                {
                  name: 'host',
                  label: 'Host',
                  Input: TextInput,
                },
                {
                  name: 'port',
                  label: 'Port',
                  placeholder: '5432',
                  Input: TextInput,
                },
                {
                  name: 'dbname',
                  label: 'Database',
                  Input: TextInput,
                },
                {
                  name: 'user',
                  label: 'Username',
                  Input: TextInput,
                },
                {
                  name: 'password',
                  label: 'Password',
                  Input: TextInput,
                },
              ]
            }
          })
        },
      },
    ] as any,
  }))

  useInput((_input, key) => {
    if (key.tab) {
      runInAction(() => {
        if (state.active + 1 >= state.fields.length) state.active = 0
        else state.active = state.active + 1
      })
    }
  })

  if (state.submit) {
    return (
      <Box flexDirection="column">
        <Text>{state.log}</Text>
      </Box>
    )
  }

  return (
    <Box flexDirection="column">
      <Box flexDirection="column" marginLeft={2}>
        <Box marginY={1}>
          <Text>Create DB Connection • </Text>
          <Text color="gray">Key: </Text>
          <Text color="magentaBright" bold={true}>
            [tab] [enter] {state.active}
          </Text>
        </Box>

        {state.fields.map(
          (
            { name, label, Input, inputConfig, validate, placeholder }: any,
            index: number
          ) => {
            const form = state.forms as any
            if (typeof (state.forms as any)[name] === 'undefined') {
              form[name] = ''
            }

            return (
              <Box flexDirection="column" key={index}>
                <Box>
                  <Text bold={state.active === index}>{label}: </Text>
                  {state.active === index ? (
                    <Input
                      {...inputConfig}
                      value={form[name]}
                      onChange={action((value: any) => {
                        form[name] = value
                      })}
                      placeholder={placeholder}
                      onSubmit={async (value: any) => {
                        if (typeof validate === 'function') {
                          validate()
                        }

                        runInAction(() => {
                          if (value) {
                            form[name] = value
                          }
                          state.active += 1
                          if (state.active === state.fields.length) {
                            state.submit = true
                          }
                          state.log = ''
                        })

                        if (state.submit) {
                          await create(
                            form,
                            action((text: string) => {
                              state.log += text
                            }),
                            done
                          )
                        }
                      }}
                    />
                  ) : (
                    <Text>{form[name]}</Text>
                  )}
                </Box>
              </Box>
            )
          }
        )}
      </Box>
    </Box>
  )
})

const create = async (
  opt: any,
  log: (text: string) => void,
  done: () => void
) => {
  const dir = join(dirs.db, opt.name)

  if (await pathExists(dir)) {
    log(`Connection ${opt.name} already exists`)
  }
  log(`Creating ${opt.name}`)

  await mkdirp(dir)

  if (opt.type === 'oracledb') {
    await writeFile(
      join(dirs.db, opt.name, 'config.js'),
      `
module.exports = {
    client: 'oracledb',
    connection: {
        user: "${opt.user}",
        password: "${opt.password}",  
        connectString: "${opt.host}:${opt.port || '1521'}/${opt.service_name}"
    }
}
    `
    )
  } else if (opt.type === 'postgresql') {
    await writeFile(
      join(dirs.db, opt.name, 'config.js'),
      `
module.exports = {
    client: 'postgresql',
    connection: {
        host: '${opt.host}',
        port: '${opt.port}',
        database: '${opt.dbname}',
        user: "${opt.user}",
        password: "${opt.password}", 
    },
    pool: {
        min: 2,
        max: 10
    },
};
    `
    )
  }

  await writeFile(
    join(dirs.db, opt.name, 'package.json'),
    `{
    "name": "db-${opt.name}",
    "version": "0.1.1",
    "license": "UNLICENSED",
    "main": "dist/index.js",
    "types": "src/index.ts",
    "scripts": {
        "postinstall": "cd ../.. && yarn patch-package"
    },
    "dependencies": {
        "knex": "^0.21.13",
        "objection": "^2.2.3",
        "oracledb": "^5.1.0",
        "pg": "^8.5.1"
    },
    "devDependencies": {}
}
`
  )

  await writeFile(
    join(dirs.db, opt.name, 'tsconfig.json'),
    `{
    "include": ["src", "types"],
    "exclude": ["./dist/**/*" ],
    "compilerOptions": {
        "module": "commonjs",
        "lib": ["esnext", "DOM"],
        "target": "ES5",
        "importHelpers": true,
        "declaration": true,
        "sourceMap": true,
        "outDir": "./dist/types",
        "rootDir": "./src",
        "strict": true,
        "emitDeclarationOnly": true,
        "noImplicitReturns": true,
        "noFallthroughCasesInSwitch": true,
        "noUnusedLocals": true,
        "noUnusedParameters": true,
        "moduleResolution": "node",
        "jsx": "react",
        "esModuleInterop": true,
        "skipLibCheck": true,
        "forceConsistentCasingInFileNames": true
    }
}
`
  )

  await mkdirp(join(dirs.db, opt.name, 'src'))
  await writeFile(
    join(dirs.db, opt.name, 'src', 'index.ts'),
    `import Knex from 'knex'
  import { Model } from 'objection'
  export const dbconfig = require('../config.js')
  export const knex = Knex(dbconfig)
  Model.knex(knex)`
  )

  log('\nRunning Yarn...')
  done()
}
