import chokidar from 'chokidar'
import { copy, mkdirp, pathExists, remove } from 'fs-extra'
import { action, runInAction } from 'mobx'
import { join } from 'path'
import { build } from '../builder/build'
import { dirs } from '../libs/dirs'
import { dev } from './dev'

const list = [
  {
    name: 'libs',
    dir: dirs.libs,
    onAfterBuild: async () => {
      // updateLibsWeb(
      //   action(() => {
      //     if (dev.build.done) {
      //       dev.app[dev.active].log += `[  libs  ] Package Libs updated\n`
      //     }
      //   })
      // )
    },
  },
  {
    name: 'server',
    dir: dirs.platform,
    out: join(dirs.build, 'platform', 'index.js'),
  },
]

export const watchAndBuild = async () => {
  // const apps = Object.values(dev.app)

  startBuildTimer()

  // watch ui components and copy it to
  // web and mobile when there are changes
  const copyui = async (e: any) => {
    await copy(e, join(dirs.ui, e.substr(dirs.webui.length + 1)), {
      overwrite: true,
    })
  }

  if (pathExists(dirs.webui)) {
    await remove(dirs.webui)
  }

  await mkdirp(dirs.webui)
  await copy(dirs.ui, dirs.webui)

  const uiwatch = chokidar.watch(dirs.webui, {
    ignored: /(^|[\/\\])\../, // ignore dotfiles
    persistent: true,
    alwaysStat: false,
    ignoreInitial: true,
  })

  uiwatch.on('change', copyui)
  uiwatch.on('add', copyui)
  uiwatch.on('unlink', async (e) => {
    await remove(join(dirs.ui, e.substr(dirs.webui.length + 1)))
  })

  for (let e of list) {
    const { name, out, dir, onAfterBuild } = e
    const app = dev.app.server

    const watcher = chokidar.watch(join(dir, 'src'), {
      ignored: /(^|[\/\\])\../, // ignore dotfiles
      persistent: true,
      alwaysStat: false,
      ignoreInitial: true,
    })
    const rebuild = async () => {
      stopBuildTimer()
      startBuildTimer()
      runInAction(() => {
        dev.build.current = name
      })
      if (app) {
        runInAction(() => {
          app.log = `Restarting ${name}...`
        })

        try {
          await build(dir, {
            outfile: out,
          })
        } catch (e) {
          runInAction(() => {
            dev.build.current = ''
            app.log = e.toString()
          })
          stopBuildTimer()
          return
        }
        runInAction(() => {
          app.log = ``
        })

        app.start()
      }
      if (typeof onAfterBuild === 'function') {
        onAfterBuild()
      }
      stopBuildTimer()
      runInAction(() => {
        dev.build.current = ''
      })
    }
    watcher.on('change', rebuild)
    watcher.on('add', rebuild)
    watcher.on('unlink', rebuild)

    // if (!(await isBuiltAlready(out || dir, !out))) {
    await build(dir, {
      outfile: out,
    })
    // }
  }

  stopBuildTimer()
  runInAction(() => {
    if (!dev.build.done) dev.build.done = true
    dev.build.current = ''
  })

  for (let name of dev.start) {
    const app = dev.app[name]
    app.start()
  }
}

export const startBuildTimer = () => {
  stopBuildTimer()
  dev.build.ival = setInterval(
    action(() => {
      dev.build.ellapsed += 0.1
    }),
    100
  )
}

export const stopBuildTimer = () => {
  runInAction(() => {
    dev.build.ellapsed = 0
    if (dev.build.ival) {
      clearInterval(dev.build.ival)
    }
    dev.build.ival = 0
  })
}

// const isBuiltAlready = async (dir: string, useDist: boolean) => {
//   let checks = [dir]
//   if (useDist) {
//     checks = [join(dir, 'dist'), join(dir, 'dist', 'index.js')]
//   }

//   for (let d of checks) {
//     if (!(await pathExists(d))) {
//       return false
//     }
//   }

//   return true
// }
