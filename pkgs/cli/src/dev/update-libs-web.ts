import fs from 'fs-extra'
import { debounce } from 'lodash-es'
import { join } from 'path'
import { dirs } from '../libs/dirs'

export const updateLibsWeb = debounce(
  async (callback?: () => void) => {
    const cachepath = join(
      dirs.web,
      'node_modules',
      '.cache',
      'snowpack',
      'development',
      'libs.js'
    )
    const newfile = await fs.readFile(
      join(dirs.libs, 'dist', 'index.js'),
      'utf-8'
    )

    const file = `
import { g as getDefaultExportFromCjs, c as createCommonjsModule } from './common/_commonjsHelpers-913f9c4a.js';

var dist = createCommonjsModule(function (module, exports) {
${newfile}
});

var index = /*@__PURE__*/getDefaultExportFromCjs(dist);

export default index;

`.replace(/process\.env\.NODE_ENV/gi, '"development"')

    await fs.writeFile(cachepath, file)

    if (callback) {
      callback()
    }
  },
  1000,
  {
    trailing: true,
  }
)
