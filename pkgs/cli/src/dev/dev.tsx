import { ExecaChildProcess } from 'execa'
import { Box, Text, useInput, useStdout } from 'ink'
import SelectInput from 'ink-select-input'
import Spinner from 'ink-spinner'
import { camelCase, capitalize, upperCase, upperFirst } from 'lodash-es'
import { action, configure, observable, runInAction } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import { Main } from '../components/Main'
import { cap } from '../libs/cap'
import { init } from '../libs/init'
import { waitUntil } from '../libs/waitUntil'
import { db, DbComponent } from './db/db'
import { updateBase } from './git/update-base'
import { push } from './git/push'
import { runPackage } from './run-package'
import { watchAndBuild } from './watch-and-build'

import TextInput from 'ink-text-input'
import { PORT } from '..'
configure({
  useProxies: 'never',
})

interface IApp {
  child: ExecaChildProcess | null
  log: string
  url?: string | null
  start: () => void
}

export const startDev = () => {
  init(<Dev />)
}

export const dev = observable({
  build: {
    current: '',
    ellapsed: 0,
    done: false,
    ival: 0 as any,
  },
  rootMenu: false,
  menu: false,
  active: 'server',
  start: ['web', 'server', 'db', 'mobile'],
  pane: 'root',
  app: {
    server: {
      child: null,
      log: '',
      url: '',
      start: () => devServer.server(),
    },
    web: {
      child: null,
      log: '',
      url: '',
      start: () => devServer.web(),
    },
    mobile: {
      child: null,
      log: 'Mobile Ready',
      start: () => {},
    },
    db: {
      child: null,
      log: '',
      url: null,
      start: () => devServer.db(),
    },
  } as Record<string, IApp>,
  up: 0,
  get log() {
    return this.app[this.active].log
  },
})
export const Dev = observer(() => {
  const { stdout } = useStdout()
  useInput(keyboardShortcut)
  useEffect(() => {
    watchAndBuild()
  }, [])

  const size = {
    w: stdout?.columns || 80,
    h: stdout?.rows || 24,
  }

  const activeApp = dev.app[dev.active]

  return (
    <Main
      cleanup={async () => {
        for (let [_, e] of Object.entries(dev.app)) {
          if (e && e.child) {
            e.child.kill('SIGINT', {
              forceKillAfterTimeout: 1000,
            })
          }
        }
      }}
    >
      <Box height={1}>
        <Logo />
        {dev.build.done ? (
          Object.keys(dev.app).map((e, idx) => {
            return (
              <Box key={idx}>
                {idx === 0 ? (
                  <Text color={'blue'} bold={true}>
                    {' '}
                    {activeApp.url !== undefined ? '┬' : '→'}{' '}
                  </Text>
                ) : (
                  <Text> • </Text>
                )}
                <Text
                  bold={true}
                  color={
                    e === dev.active && dev.pane === 'root' && !dev.rootMenu
                      ? 'yellow'
                      : 'white'
                  }
                  underline={e === dev.active && !dev.rootMenu}
                >
                  {upperCase(e)}
                  {e === dev.active && dev.up > 0 ? ' ' + dev.up + '↑' : ''}
                </Text>
              </Box>
            )
          })
        ) : (
          <Text>
            <Text color={'blue'} bold={true}>
              {' '}
              {dev.app.web ? '┬' : '→'}{' '}
            </Text>
            Building
          </Text>
        )}

        {dev.build.current !== '' ? (
          <>
            <Text> • </Text>
            <Text color={'blue'} bold={true}>
              <Spinner /> {upperFirst(camelCase(dev.build.current))}{' '}
              {(Math.round(dev.build.ellapsed * 10) / 10).toFixed(1)}s
            </Text>
          </>
        ) : (
          <>
            <Text> • </Text>
            <Text color="grey">
              Key:{' '}
              <Text color={'magentaBright'} bold={true} wrap={'end'}>
                {dev.pane === 'db' ? '↑ [esc]' : '← → [enter]'}
              </Text>
            </Text>
          </>
        )}
      </Box>

      {typeof activeApp.url === 'string' && (
        <Box marginBottom={1}>
          <>
            <RootMenuKey />
            <Text color={'blue'} bold={true}>
              └{' '}
            </Text>
            {!activeApp.url ? (
              <Text color="cyan">
                <Spinner type={'aesthetic' as any} />
              </Text>
            ) : (
              <Text color="cyan" bold={true}>
                {activeApp.url}
              </Text>
            )}
          </>
        </Box>
      )}

      {activeApp.url === undefined && <Box height={1} />}

      {dev.rootMenu ? (
        <RootMenu h={size.h} />
      ) : (
        <>
          {dev.menu ? (
            <Box flexGrow={1} flexDirection="column">
              <Text>
                Press <Text bold={true}>[esc]</Text> to back,{' '}
                <Text bold={true}>[enter]</Text> to select:
              </Text>
              <Box marginLeft={2} marginTop={1}>
                <SelectInput
                  onSelect={action((e: { value: string; label: string }) => {
                    if (e.value.indexOf('restart-') === 0) {
                      const key = e.value.substr('restart-'.length)
                      if (key) {
                        restart(key)
                      }
                    } else if (e.value.indexOf('log-') === 0) {
                      const key = e.value.substr('log-'.length)
                      if (key) {
                        dev.app[key].log = ''
                      }
                    } else if (e.value === 'cache-web') {
                      restart('web', 'reload')
                    }
                  })}
                  items={getMenu()}
                />
              </Box>
            </Box>
          ) : (
            <Box flexGrow={1}>
              {dev.active === 'db' ? (
                <DbComponent />
              ) : (
                <Text>{cap(dev.log, size.h - 3, dev.up)}</Text>
              )}
            </Box>
          )}
        </>
      )}
    </Main>
  )
})

export const RootMenuKey = () => {
  return (
    <>
      <Text color={'grey'}>{'     '}</Text>
      <Text color={'grey'}>{'╴'}</Text>
      <Text color={'blueBright'}>esc</Text>
      <Text color={'grey'}>{'╶'}</Text>
      <Text color={'yellow'} bold={true}>
        {' '}
      </Text>
    </>
  )
}

const Logo = () => {
  return (
    <>
      <Text color={'greenBright'} bold={true} underline={dev.rootMenu}>
        DEV
      </Text>
      <Text color={'green'} underline={dev.rootMenu}>
        Plansys
      </Text>
    </>
  )
}

const RootMenu = observer(({ h }: any) => {
  const state = useLocalObservable(() => ({
    mode: '',
    log: '',
    inputTitle: '',
    inputValue: '',
    inputAction: (_: string) => {},
  }))

  if (state.mode === 'input') {
    return (
      <Box flexDirection="column">
        <Text>{state.inputTitle}</Text>
        <TextInput
          value={state.inputValue}
          onChange={action((e: any) => {
            state.inputValue = e
          })}
          onSubmit={(e: any) => {
            state.inputAction(e)
          }}
        />
      </Box>
    )
  }

  if (state.mode === 'log') {
    return <Text>{cap(state.log, h - 3, 0)}</Text>
  }

  return (
    <Box
      width={30}
      marginBottom={1}
      marginLeft={11}
      flexGrow={1}
      paddingX={1}
      borderColor={'green'}
      borderStyle="round"
    >
      <SelectInput
        onSelect={async (e) => {
          switch (e.value) {
            case 'quit':
              process.exit()
              break
            case 'pull-base':
              runInAction(() => {
                state.mode = 'log'
              })
              await updateBase(
                action((log) => {
                  state.log = log
                })
              )
              runInAction(() => {
                state.mode = ''
              })
              break
            case 'push-base':
              runInAction(() => {
                state.mode = 'input'
                state.inputValue = 'Fix'
                state.inputTitle = 'Commit Message:'
                state.inputAction = async (e: string) => {
                  runInAction(() => {
                    state.mode = 'log'
                  })
                  await push(
                    e,
                    action((log) => {
                      state.log = log
                    })
                  )
                  runInAction(() => {
                    state.mode = ''
                  })
                }
              })
              break
          }
        }}
        items={[
          {
            value: 'pull-base',
            label: 'Update Base',
          },
          {
            value: 'connect',
            label: 'Connect Remote Server',
          },
          {
            value: 'quit',
            label: 'Quit',
          },
        ]}
      />
    </Box>
  )
})

export const restart = async (key: string, argv?: any) => {
  const app = dev.app[key]
  if (!!app) {
    app.child?.kill('SIGINT', {
      forceKillAfterTimeout: 1000,
    })

    await waitUntil(
      action(() => {
        return app.child?.killed
      })
    )

    runInAction(() => {
      app.log = ''
      app.child = null
      if (dev.active !== key) {
        dev.active = key
      }
    })
    devServer[key](argv)
  }
}

const getMenu = () => {
  const menus = []
  if (dev.active) {
    menus.push({
      label: capitalize(dev.active) + ' - Clear Log',
      value: 'log-' + dev.active,
    })
    menus.push({
      label: capitalize(dev.active) + ' - Restart',
      value: 'restart-' + dev.active,
    })

    if (dev.active === 'web') {
      menus.push({
        label: 'Web - Reload Cache',
        value: 'cache-web',
      })
    }
  }

  if (menus.length === 0) {
    return [{ label: 'Loading...', value: '' }]
  }
  return menus
}

const devServer: Record<string, (argv?: any) => void> = {
  web: async (reload?: string) => {
    let webOutput = true
    const args: string[] = []
    let shouldReload = !!reload

    runInAction(() => {
      if (shouldReload) {
        args.push('--reload')
        webOutput = true
      }
      dev.app.web.child = runPackage(
        'web',
        action(
          (
            output: string,
            _child: ExecaChildProcess,
            mode: 'error' | 'data'
          ) => {
            if (mode === 'error') {
              dev.app.web.log = output
              return
            }

            if (output.indexOf('http://') > 0 && output.indexOf('•') > 0) {
              dev.app.server.url = output
                .trim()
                .split('\n')[0]
                .replace(/:(\d+)/gi, ':' + PORT)
              dev.app.web.url = dev.app.server.url
            }
            if (webOutput) {
              dev.app.web.log += output
            }
            const url = dev.app.web.url
            if (url && url.indexOf('http') >= 0 && !webOutput) {
              webOutput = true
              if (dev.app.web.log.trim() === '') {
                dev.app.web.log += `[snowpack] Web Ready\n`
              }
            }
          }
        ),
        args
      )
    })
  },
  server: async () => {
    if (dev.app.server.child) {
      dev.app.server.child.kill()
      await waitUntil(() => dev.app.server.child?.killed)
    }
    dev.app.server.child = runPackage(
      'platform',
      action((output: string) => {
        dev.app.server.log += output
      })
    )
  },
  db: async () => {
    await db.init()
    await db.buildEntry()
    // await db.buildAll()
  },
}

const keyboardShortcut = action((input: string, key: any) => {
  if (dev.pane !== 'root') {
    return
  }
  const keys = Object.keys(dev.app)
  const idx = keys.indexOf(dev.active)
  if (key.rightArrow) {
    dev.up = 0

    if (dev.rootMenu) {
      dev.rootMenu = false
      dev.active = keys[0]
      return
    }

    if (idx + 1 > keys.length - 1) {
      dev.rootMenu = true
    } else {
      dev.active = keys[idx + 1] as any
    }
  } else if (key.leftArrow) {
    dev.up = 0

    if (dev.rootMenu) {
      dev.rootMenu = false
      dev.active = keys[keys.length - 1]
      return
    }

    if (idx - 1 < 0) {
      dev.rootMenu = true
    } else {
      dev.active = keys[idx - 1] as any
    }
  }

  if (!dev.rootMenu) {
    if (!dev.menu) {
      if (key.pageUp) {
        dev.up += 100
      } else if (key.pageDown) {
        dev.up = 0
      } else if (key.upArrow) {
        dev.up += 1
      } else if (key.downArrow) {
        if (dev.active === 'db' && dev.up === 0) {
          dev.pane = 'db'
          return
        }

        dev.up -= 1
        if (dev.up < 0) dev.up = 0
      }
      if (key.return || input === ' ') {
        if (dev.active === 'db') {
          dev.pane = 'db'
        } else {
          dev.menu = true
        }
      }

      if (key.escape) {
        dev.rootMenu = !dev.rootMenu
      }
    } else {
      if (key.escape || key.return) {
        dev.menu = false
      }
    }
  }
})
