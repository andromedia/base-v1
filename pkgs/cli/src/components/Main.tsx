import { Box, useStdin, useStdout } from 'ink'
import { runInAction } from 'mobx'
import { observer, useLocalObservable } from 'mobx-react-lite'
import React, { useEffect } from 'react'
import terminalSize from 'term-size'
import { currentInk } from '../libs/init'

export const Main = observer(
  ({ children, cleanup }: { children: any; cleanup: () => void }) => {
    const { setRawMode, isRawModeSupported } = useStdin()
    const { stdout } = useStdout()
    const meta = useLocalObservable(() => ({
      size: { w: stdout?.columns, h: stdout?.rows },
    }))

    useEffect(() => {
      if (isRawModeSupported) {
        setRawMode(true)
      }

      if (cleanup) {
        ;[
          `exit`,
          `SIGINT`,
          `SIGUSR1`,
          `SIGUSR2`,
          `uncaughtException`,
          `SIGTERM`,
        ].forEach((eventType) => {
          process.on(eventType, cleanup.bind(null, eventType))
        })
      }

      stdout?.on('resize', () => {
        runInAction(() => {
          const size = terminalSize()
          meta.size.w = size.columns || 80
          meta.size.h = size.rows || 24
        })
      })

      return () => {
        if (isRawModeSupported) {
          setRawMode(false)
          if (currentInk) {
            currentInk.unmount()
            process.exit()
          }
        }
      }
    }, [stdout])

    return (
      <Box
        flexDirection={'column'}
        width={meta.size.w}
        height={meta.size.h}
        alignItems={'stretch'}
      >
        {children}
      </Box>
    )
  }
)
